//
//  DiceType.swift
//  MTGCounters
//
//  Created by Conor King on 11/18/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

enum DiceType {
    case coin
    case six
    case sixTwice
    case eight
    case twenty
}
