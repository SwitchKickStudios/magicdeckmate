//
//  SettingsViewController.swift
//  MTGCounters
//
//  Created by Conor King on 11/14/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class SettingsViewController: MDMViewController, CloseModalDelegate, BackModalDelegate {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    
    @IBOutlet weak var playersButton: UIButton!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var startingLifeButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var playersBorder: UIView!
    @IBOutlet weak var colorBorder: UIView!
    @IBOutlet weak var startingLifeBorder: UIView!
    @IBOutlet weak var helpBorder: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.customNavigationSetup()
        self.sizingSetup()
        self.UISetup()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playersButton(_ sender: Any) {
        self.delegate?.openView(selection: "players", animate: false)
    }
    @IBAction func colorButton(_ sender: Any) {
        self.delegate?.openView(selection: "color", animate: false)
    }
    @IBAction func startingLifeButton(_ sender: Any) {
        self.delegate?.openView(selection: "startingLife", animate: false)
    }
    @IBAction func helpButton(_ sender: Any) {
        self.delegate?.openView(selection: "support", animate: false)
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        self.delegate?.closeMenu()
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "main", animate: false)
    }

}

// MARK: STYLE SETUP
extension SettingsViewController {
    
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Settings"
        customTopBar.view.frame = self.topBufferView.bounds
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        customNavBar.showBackButton = true
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
    }
    
    func sizingSetup() {
        self.playersButton.set(fontSize: getCenterMenuFontSize())
        self.colorButton.set(fontSize: getCenterMenuFontSize())
        self.startingLifeButton.set(fontSize: getCenterMenuFontSize())
        self.helpButton.set(fontSize: getCenterMenuFontSize())
    }
    
    func UISetup() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color3 = colorData.color3
        self.color4 = colorData.color4
        
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        
        // Separators
        self.playersBorder.backgroundColor = self.color3
        self.colorBorder.backgroundColor = self.color3
        self.startingLifeBorder.backgroundColor = self.color3
        self.helpBorder.backgroundColor = self.color3
        
        // Buttons
        styleManager.format(button: self.playersButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.playersButton, type: .menuItem)
        styleManager.format(button: self.colorButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.colorButton, type: .menuItem)
        styleManager.format(button: self.startingLifeButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.startingLifeButton, type: .menuItem)
        styleManager.format(button: self.helpButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.helpButton, type: .menuItem)

    }
    
    // Returns a font size based on device class
    func getCenterMenuFontSize() -> CGFloat {
        let device = UIDevice.current
        let game = GameData.sharedInstance
        if device.model == "iPad" {
            if game.numberOfPlayers == 2 {
                return 23
            } else {
                return 25
            }
        } else {
            if game.numberOfPlayers == 2 {
                return 18
            } else {
                return 20
            }
        }
    }
}












