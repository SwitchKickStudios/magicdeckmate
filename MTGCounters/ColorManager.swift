//
//  ColorManager.swift
//  MTGCounters
//
//  Created by William King on 5/13/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation
import UIKit
import DynamicColor

class ColorManager {
        
    // Decide what color highlight based
    func getHighlightColor(for scheme: ColorSchemes?, color: UIColor) -> UIColor {
        if scheme == .black {
            return color.tinted(amount: 0.25)
        } else {
            return color.shaded(amount: 0.35)
        }
    }
    
    // Return a color - used for a pre-selected item
    func getSelectionColor(for scheme: ColorSchemes?, color: UIColor) -> UIColor {
        if scheme == .black {
            return color.tinted(amount: 0.15)
        } else {
            return color.shaded(amount: 0.25)
        }
    }
    
    // Returns a dictionary of assets from a given scheme, where the keys are the UI Elements and the values are the asset names
    func getAssetsForColor(scheme: ColorSchemes) -> ColorData {
        let secondaryColor = getFontColorFor(scheme: scheme)
        var colorData: ColorData?
        switch scheme {
        case .black:
            colorData = ColorData(backgroundLarge: "BlackBackground",
                                      menuIcon: #imageLiteral(resourceName: "BlackMenuIcon"),
                                      primaryColor: UIColor(hex: "000000"),
                                      secondaryColor: secondaryColor,
                                      color3: UIColor(hex: "313131"),
                                      color4: UIColor(hex: "151515"),
                                      color5: UIColor(hex: "0F0F0F")
            )
        case .blue:
            colorData = ColorData(backgroundLarge: "BlueBackground",
                                      menuIcon: #imageLiteral(resourceName: "BlueMenuIcon"),
                                      primaryColor: UIColor(hex: "385972"),
                                      secondaryColor: secondaryColor,
                                      color3: UIColor(hex: "18222A"),
                                      color4: UIColor(hex: "456278"),
                                      color5: UIColor(hex: "517087")
            )
        case .green:
            colorData = ColorData(backgroundLarge: "GreenBackground",
                                      menuIcon: #imageLiteral(resourceName: "GreenMenuIcon"),
                                      primaryColor: UIColor(hex: "84AE6B"),
                                      secondaryColor: secondaryColor,
                                      color3: UIColor(hex: "52763C"),
                                      color4: UIColor(hex: "8BAF77"),
                                      color5: UIColor(hex: "93B97E")
            )
        case .red:
            colorData = ColorData(backgroundLarge: "RedBackground",
                                      menuIcon: #imageLiteral(resourceName: "RedMenuIcon"),
                                      primaryColor: UIColor(hex: "663333"),
                                      secondaryColor: secondaryColor,
                                      color3: UIColor(hex: "471818"),
                                      color4: UIColor(hex: "6E3F3F"),
                                      color5: UIColor(hex: "774444")
            )
        case .white:
            colorData = ColorData(backgroundLarge: "WhiteBackground",
                                      menuIcon: #imageLiteral(resourceName: "WhiteMenuIcon"),
                                      primaryColor: UIColor(hex: "ECEBC8"),
                                      secondaryColor: secondaryColor,
                                      color3: UIColor(hex: "B7B585"),
                                      color4: UIColor(hex: "F1F1D6"),
                                      color5: UIColor(hex: "EEEED7")
            )
        default:
            colorData = ColorData(backgroundLarge: "ColorlessBackground",
                                      menuIcon: #imageLiteral(resourceName: "ColorlessMenuIcon"),
                                      primaryColor: UIColor(hex: "CACACA"),
                                      secondaryColor: secondaryColor,
                                      color3: UIColor(hex: "737373"),
                                      color4: UIColor(hex: "BFBFBF"),
                                      color5: UIColor(hex: "D9D9D9")
            )
        }
        return colorData!
    }
    
    // Returns a UIColor to be used as a font color from a given ColorScheme
    func getFontColorFor(scheme: ColorSchemes) -> UIColor {
        switch scheme {
        case .colorless, .green, .white:
            return UIColor.black
        case .black, .blue, .red:
            return UIColor.white
        }
    }
    
    // Set status base color for given scheme
    func setStatusBarColorFor(scheme: ColorSchemes) {
        var style: UIStatusBarStyle = .default
        switch scheme {
        case .black, .blue, .red:
            style = .lightContent
        default:
            style = .default
        }
        UIApplication.shared.statusBarStyle = style
    }
    
    func getStatusBarColorFor(scheme: ColorSchemes) -> UIStatusBarStyle {
        var style: UIStatusBarStyle = .default
        switch scheme {
        case .black, .blue, .red:
            style = .lightContent
        default:
            style = .default
        }
        return style
    }
    
    func getNavigationBarStyleFor(scheme: ColorSchemes) -> UIBarStyle {
        var style: UIBarStyle = .default
        switch scheme {
        case .black, .blue, .red:
            style = .black
        default:
            style = .default
        }
        return style
    }
    
}

// UIColor extension that creates a UIColor from a hex
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}



