 //
//  TwoPlayerBattlefieldViewController.swift
//  MTGCounters
//
//  Created by Conor King on 8/24/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class TwoPlayerBattlefieldViewController: MDMViewController, TwoPlayerUserDelegate, PlayerSelectDelegate, ColorSelectDelegate {

    // Inputs
    var players = [PlayerData(maxPlayers: 2),PlayerData(maxPlayers: 2)]
    var newCard: CardData?
    var newCardPlayerNumber = 1
    var startingOrientation = UIDevice.current.orientation
    
    let game = GameData.sharedInstance
    let buttons = Buttons()
    
    // UI Outlets
    @IBOutlet weak var player1BattlefieldView: UIView!
    @IBOutlet weak var player2BattlefieldView: UIView!
    @IBOutlet weak var player1Background: UIView!
    @IBOutlet weak var player2Background: UIView!
    
    @IBOutlet var playerAdjacentConstraint_Portrait: NSLayoutConstraint!
    @IBOutlet var player2Left_Portrait: NSLayoutConstraint!
    @IBOutlet var player1Right_Portrait: NSLayoutConstraint!
    @IBOutlet var playerBackgroundAdjacentConstraint_Portrait: NSLayoutConstraint!
    @IBOutlet var player2BackgroundLeft_Portrait: NSLayoutConstraint!
    @IBOutlet var player1BackgroundRight_Portrait: NSLayoutConstraint!
    
    @IBOutlet var player1Top_Landscape: NSLayoutConstraint!
    @IBOutlet var playerAdjacentConstraint_Landscape: NSLayoutConstraint!
    @IBOutlet var player1BackgroundTop_Landscape: NSLayoutConstraint!
    @IBOutlet var playerBackgroundAdjacentConstraint_Landscape: NSLayoutConstraint!
    
    var menuNavController: UINavigationController?
    var player1CenterMenuButton: UIButton?
    var player2CenterMenuButton: UIButton?
    
    // Parameters
    var showAlert: CustomAlertViewController? // Used to show the alert on viewWillAppear for a balanced transition
    var snap: UIView?
    var player1SavedMenu: String?
    var player2SavedMenu: String?
    var isReversed: Bool = false
    var currentMenuString: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        findEmptySlotForNewCard()
        self.arrangeSubViews(orientation: OrientationService().convertToDeviceOrientationFrom(interfaceOrientation: UIApplication.shared.statusBarOrientation), onLoad: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for subView in self.view.subviews {
            if subView == snap {
                subView.removeFromSuperview()
            }
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.view.transform = CGAffineTransform(rotationAngle: 0)
        if showAlert != nil {
            showAlert?.show()
            showAlert = nil
        }
        self.resetColorScheme()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.arrangeSubViews(orientation: UIDevice.current.orientation, onLoad: false)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resetColorScheme() {
        let gameSchemes = GameData.sharedInstance.colorSchemes
        if isReversed {
            self.player1Background.backgroundColor = ColorManager().getAssetsForColor(scheme: gameSchemes[1]).primaryColor
            self.player2Background.backgroundColor = ColorManager().getAssetsForColor(scheme: gameSchemes[0]).primaryColor
        } else {
            self.player1Background.backgroundColor = ColorManager().getAssetsForColor(scheme: gameSchemes[0]).primaryColor
            self.player2Background.backgroundColor = ColorManager().getAssetsForColor(scheme: gameSchemes[1]).primaryColor
        }

    }

}

// MARK : BATTLEFIELD CONFIGURATION
extension TwoPlayerBattlefieldViewController {
    // Find empty slot in PlayerData for the correct player, if fails, present alert
    func findEmptySlotForNewCard() {
        if newCard != nil {
            if players[newCardPlayerNumber-1].findEmptySlot(for: newCard!) == false {
                
                // Create an alert before calling the delegate method do perform the proper reset
                let alert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertViewController
                alert.player = newCardPlayerNumber
                alert.setAlertText(title: "", message: "This player has no more room on the battlefield.", b1: "Okay", b2: "")
                
                alert.button1Action = { (alertVC) in
                }
                alert.button2ShouldHide = true
                self.showAlert = alert
                
            }
            newCard = nil

        }
    }
    
    // Removes the subviews from each battlefield guide view
    func clearBattlefield() {
        for subView in player1BattlefieldView.subviews {
            subView.removeFromSuperview()
        }
        for subView in player2BattlefieldView.subviews {
            subView.removeFromSuperview()
        }
    }
}

// MARK : BATTLEFIELD ACTIONS
extension TwoPlayerBattlefieldViewController {
    
    // SWITCH BATTLEFIELD
    // This is the action taken when changing global Player Number
    // PlayerSelectDelegate function
    func changeBattlefieldTo(players: Int) {
        switch players {
        case 1:
            
            // ONE PLAYER
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            let onePlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "OnePlayerBattlefieldVC") as! OnePlayerBattlefieldViewController
            self.navigationController?.setViewControllers([onePlayerVC], animated: true)
            
        case 2:
            
            // TWO PLAYERS
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            let twoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "TwoPlayerBattlefieldVC") as! TwoPlayerBattlefieldViewController
            self.navigationController?.setViewControllers([twoPlayerVC], animated: true)
            
        default:
            return
        }
    }
    
    // Update the battlefield's array of players
    // TwoPlayerUserDelegate function
    func updatePlayer(data: PlayerData, number: Int) {
        self.players[number-1] = data
    }
    
}

// MARK: SUBVIEWS
extension TwoPlayerBattlefieldViewController {
    
    // Create instances of TwoPlayerUsersVCs on the battlefield
    func loadPlayers(reverse: Bool) {
        //  User 1
        let player1VC = storyboard?.instantiateViewController(withIdentifier: "TwoPlayerUserVC") as! TwoPlayerUserViewController
        self.addChildViewController(player1VC)
        player1VC.view.frame = player1BattlefieldView.bounds
        player1VC.delegate = self
        player1VC.colorDelegate = self
        player1VC.player = self.players[0]
        player1VC.playerNumber = 1
        player1VC.menuToOpen = self.player1SavedMenu
        self.player1CenterMenuButton = player1VC.centerMenuButton
        if reverse == true {
            player2BattlefieldView.addSubview(player1VC.view)
        } else {
            player1BattlefieldView.addSubview(player1VC.view)
        }
        player1VC.didMove(toParentViewController: self)
        
        //  User 2
        let player2VC = storyboard?.instantiateViewController(withIdentifier: "TwoPlayerUserVC") as! TwoPlayerUserViewController
        self.addChildViewController(player2VC)
        player2VC.view.frame = player2BattlefieldView.bounds
        player2VC.delegate = self
        player2VC.colorDelegate = self
        player2VC.player = self.players[1]
        player2VC.playerNumber = 2
        player2VC.menuToOpen = self.player2SavedMenu
        self.player2CenterMenuButton = player2VC.centerMenuButton
        if reverse == true {
            player1BattlefieldView.addSubview(player2VC.view)
        } else {
            player2BattlefieldView.addSubview(player2VC.view)
        }
        player2VC.didMove(toParentViewController: self)
        
    }
    
    // Depending on device orientation, arrange and reorient subviews
    func arrangeSubViews(orientation: UIDeviceOrientation, onLoad: Bool) {
        self.clearBattlefield()
        if orientation.isLandscape {
            // LANDSCAPE
            player2Left_Portrait.isActive = false
            player2BackgroundLeft_Portrait.isActive = false
            player1Right_Portrait.isActive = false
            player1BackgroundRight_Portrait.isActive = false
            playerAdjacentConstraint_Portrait.isActive = false
            playerBackgroundAdjacentConstraint_Portrait.isActive = false
            player1Top_Landscape.isActive = true
            player1BackgroundTop_Landscape.isActive = true
            playerAdjacentConstraint_Landscape.isActive = true
            playerBackgroundAdjacentConstraint_Landscape.isActive = true
            
            player2BattlefieldView.transform = CGAffineTransform(rotationAngle: 0)
            player2Background.transform = CGAffineTransform(rotationAngle: 0)

        } else {
            // PORTRAIT
            player1Top_Landscape.isActive = false
            player1BackgroundTop_Landscape.isActive = false
            playerAdjacentConstraint_Landscape.isActive = false
            playerBackgroundAdjacentConstraint_Landscape.isActive = false
            player2Left_Portrait.isActive = true
            player2BackgroundLeft_Portrait.isActive = true
            player1Right_Portrait.isActive = true
            player1BackgroundRight_Portrait.isActive = true
            playerAdjacentConstraint_Portrait.isActive = true
            playerBackgroundAdjacentConstraint_Portrait.isActive = true

            // Flip User 2
            player2BattlefieldView.transform = CGAffineTransform(rotationAngle: .pi)
            player2Background.transform = CGAffineTransform(rotationAngle: .pi)
        }
        if onLoad == true {
            if orientation == .landscapeLeft || orientation == .portrait {
                self.loadPlayers(reverse: false)
                self.isReversed = false
            } else {
                self.loadPlayers(reverse: true)
                self.isReversed = true
            }
        } else {
            if orientation == .landscapeLeft {
                self.loadPlayers(reverse: true)
                self.isReversed = true
            } else {
                self.loadPlayers(reverse: false)
                self.isReversed = false
            }
        }
        self.resetColorScheme()
        self.view.layoutIfNeeded()
    }
    
    // TwoPlayerUserDelegate function
    // Receives the name of a menu then pushes it in a navigation controller
    func openTo(menu: String, player: Int, originVC: TwoPlayerUserViewController) {
        switch menu {
            case "tokens":
                // Tokens
                let tokensVC = storyboard?.instantiateViewController(withIdentifier: "TokenTypeTableVC") as! TokenTypeSelectionTableViewController
                tokensVC.players = self.players
                tokensVC.playerNumber = player
                tokensVC.delegate = originVC
                tokensVC.tokensDictionary = DataFormatService().getTokenDictionary()
                HeroWrapper.coverDown()
                if player == 2 && UIApplication.shared.statusBarOrientation.isPortrait {
                    self.snap = self.view.snapshotView(afterScreenUpdates: false)
                    self.view.addSubview(self.snap!)
                    self.snap?.transform = CGAffineTransform(rotationAngle: .pi)
                }
                self.navigationController?.pushViewController(tokensVC, animated: true)
            
            case "counters":
                // Counters
                let countersVC = storyboard?.instantiateViewController(withIdentifier: "CounterCategoryTableVC") as! CounterCategoryTableViewController
                countersVC.players = self.players
                countersVC.countersDictionary = DataFormatService().getCounterDictionary()
                countersVC.playerNumber = player
                countersVC.delegate = originVC
                HeroWrapper.coverDown()
                if player == 2 && UIApplication.shared.statusBarOrientation.isPortrait {
                    self.snap = self.view.snapshotView(afterScreenUpdates: false)
                    self.view.addSubview(self.snap!)
                    self.snap?.transform = CGAffineTransform(rotationAngle: .pi)
                }
                navigationController?.pushViewController(countersVC, animated: true)
            
            case "decks":
                // Deck
                let decksVC = storyboard?.instantiateViewController(withIdentifier: "DecksVC") as! DecksViewController
                decksVC.players = self.players
                decksVC.playerNumber = player
                decksVC.delegate = originVC
                HeroWrapper.coverDown()
                if player == 2 && UIApplication.shared.statusBarOrientation.isPortrait {
                    self.snap = self.view.snapshotView(afterScreenUpdates: false)
                    self.view.addSubview(self.snap!)
                    self.snap?.transform = CGAffineTransform(rotationAngle: .pi)
                }
                navigationController?.pushViewController(decksVC, animated: true)
            
            case "tips":
                // Tips and Donations
                let tipsVC = storyboard?.instantiateViewController(withIdentifier: "TipsTableVC") as! TipsTableViewController
                tipsVC.delegate = originVC
                tipsVC.origin = self.currentMenuString ?? "main"

                if ReachabilityTest.isConnectedToNetwork() {
                    HeroWrapper.coverDown()
                    navigationController?.pushViewController(tipsVC, animated: true)
                } else {
                    let alertView = UIAlertController(title: "Network Error", message: "It appears there an issue with your connection.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                }

            default:
                debugPrint("Cannot determine which menu selection to open ...")
        }
    }
    
    // TwoPlayerUserDelegate Function
    // Determine which player, then save that player's opened subView
    func saveMenu(menu: String?, player: Int) {
        if menu != "tips" {
            if player == 2 {
                self.player2SavedMenu = menu
            } else {
                self.player1SavedMenu = menu
            }
        }
    }
    
    // TwoPlayerUserDelegate Function
    // Save a variable to determine which view to open back up from tipsVC
    func setCurrentMenuString(string: String) {
        if string != "tips" {
            self.currentMenuString = string
        }
    }


}




