//
//  CenterMenuViewController.swift
//  MTGCounters
//
//  Created by William King on 5/13/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

// Delegate to drive the menu navigation
protocol MenuNavigationDelegate {
    func openView(selection: String, animate: Bool)
    func turnOffGuideViews()
    func closeMenu()
}

class CenterMenuViewController: MDMViewController, CloseModalDelegate {

    var delegate: MenuNavigationDelegate? = nil
    var rootVC: UIViewController? = nil
    var playerNumber = 1
    
    @IBOutlet weak var tokensButton: UIButton!
    @IBOutlet weak var countersButton: UIButton!
    @IBOutlet weak var decksButton: UIButton!
    @IBOutlet weak var diceButton: UIButton!
    @IBOutlet weak var resetBattlefieldButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var tokensBottomBorder: UIView!
    @IBOutlet weak var countersBottomBorder: UIView!
    @IBOutlet weak var decksBottomBorder: UIView!
    @IBOutlet weak var diceBottomBorder: UIView!
    @IBOutlet weak var resetBattlefieldBottomBorder: UIView!
    @IBOutlet weak var settingsBottomBorder: UIView!
    @IBOutlet weak var middleBorder: UIView!
    
    @IBOutlet weak var bottomBufferViewWidthConstraint: NSLayoutConstraint!
    
    var didLoad = false // Used to determine if the UI styling was loaded
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sizingSetup()
        self.customNavigationSetup()
        self.UISetup()
        didLoad = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        if didLoad == false { // Prevent UI from being set up redundantly
            self.UISetup()
        }
        didLoad = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func tokensButton(_ sender: Any) {
        self.delegate?.openView(selection: "tokens", animate: false)
    }
    
    @IBAction func countersButton(_ sender: Any) {
        self.delegate?.openView(selection: "counters", animate: false)
    }
    
    @IBAction func decksButton(_ sender: Any) {
        self.delegate?.openView(selection: "decks", animate: false)
    }
    
    @IBAction func diceButton(_ sender: Any) {
        self.delegate?.openView(selection: "dice", animate: false)
    }
    
    @IBAction func resetBattlefieldButton(_ sender: Any) {
        self.delegate?.openView(selection: "resets", animate: false)
    }
    
    @IBAction func settingsButton(_ sender: Any) {
        self.delegate?.openView(selection: "settings", animate: false)
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        self.delegate?.closeMenu()
    }
}

// MARK: STYLE SETUP
extension CenterMenuViewController {
    
    func sizingSetup() {
        self.tokensButton.set(fontSize: getCenterMenuFontSize())
        self.countersButton.set(fontSize: getCenterMenuFontSize())
        self.decksButton.set(fontSize: getCenterMenuFontSize())
        self.diceButton.set(fontSize: getCenterMenuFontSize())
        self.resetBattlefieldButton.set(fontSize: getCenterMenuFontSize())
        self.settingsButton.set(fontSize: getCenterMenuFontSize())
    }
    
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Menu"
        customTopBar.view.frame = self.topBufferView.bounds
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        customNavBar.showBackButton = false
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        self.bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
    }
    
    func UISetup() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
        self.color4 = colorData.color4
        
        // Separators
        self.tokensBottomBorder.backgroundColor = self.color3
        self.countersBottomBorder.backgroundColor = self.color3
        self.decksBottomBorder.backgroundColor = self.color3
        self.diceBottomBorder.backgroundColor = self.color3
        self.resetBattlefieldBottomBorder.backgroundColor = self.color3
        self.settingsBottomBorder.backgroundColor = self.color3

        self.middleBorder.backgroundColor = self.color3
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4

        // Buttons
        styleManager.format(button: self.tokensButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.tokensButton, type: .menuItem)
        styleManager.format(button: self.countersButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.countersButton, type: .menuItem)
        styleManager.format(button: self.decksButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.decksButton, type: .menuItem)
        styleManager.format(button: self.diceButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.diceButton, type: .menuItem)
        styleManager.format(button: self.resetBattlefieldButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.resetBattlefieldButton, type: .menuItem)
        styleManager.format(button: self.settingsButton, type: .menuItem, iconImageView: nil)
        self.formatButtonOnAction(button: self.settingsButton, type: .menuItem)
        
    }
    
    // Returns a font size based on device class
    func getCenterMenuFontSize() -> CGFloat {
        let device = UIDevice.current
        let game = GameData.sharedInstance
        if device.model == "iPad" {
            if game.numberOfPlayers == 2 {
                return 23
            } else {
                return 25
            }
        } else {
            if game.numberOfPlayers == 2 {
                return 18
            } else {
                return 20
            }
        }
    }
    
}




