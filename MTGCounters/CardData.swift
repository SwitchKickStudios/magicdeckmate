//
//  CardData.swift
//  MTGCounters
//
//  Created by William King on 4/29/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

struct CardData {
    
    // Long description of card properties
    var description: String?
    
    // String identifier of the card that corresponds to the image asset name
    var name: String?
    
    // "token" or "counter"
    var type: CardType?
    
    // category of the card, the header the card will fall under
    var category: String?
    
    // quantity associated with the card
    var quantity: Int = 0
    
    // colors of the card
    var color: [String] = ["colorless"]
    
    // base stats of the card
    var stats: String = ""
    

    init(description: String
        , name: String
        , type: CardType
        , category: String
        , color: [String]
        , stats: String) {
        self.description = description
        self.name = name
        self.type = type
        self.category = category
        self.color = color
        self.stats = stats
    }
    
}
