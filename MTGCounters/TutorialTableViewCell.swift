//
//  TutorialTableViewCell.swift
//  
//
//  Created by Conor King on 5/15/18.
//

import UIKit

class TutorialTableViewCell: UITableViewCell {

    @IBOutlet weak var bullet: UILabel!
    @IBOutlet weak var tutorialTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
