//
//  DecksTableViewCell.swift
//  MTGCounters
//
//  Created by Conor King on 8/5/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class DecksTableViewCell: UITableViewCell {

    @IBOutlet weak var deckLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var deleteCellButton: UIButton!
    @IBOutlet weak var editCellButton: UIButton!
    
    @IBOutlet weak var editButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var editButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteButtonTrailingConstraint: NSLayoutConstraint!
    
    var editCellAction: ((DecksTableViewCell) -> Void)?
    var deleteCellAction: ((DecksTableViewCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func deleteCellButton(_ sender: Any) {
        deleteCellAction?(self)
    }
    @IBAction func editCellButton(_ sender: Any) {
        editCellAction?(self)
    }
    
}
