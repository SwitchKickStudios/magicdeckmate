//
//  UIButtonExtension.swift
//  MTGCounters
//
//  Created by William King on 6/13/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func set(fontSize: CGFloat) {
        if let titleLabel = titleLabel {
            titleLabel.font = UIFont(name: titleLabel.font.fontName, size: fontSize)
        }
    }
}
