//
//  CustomModal.swift
//  MTGCounters
//
//  Created by Conor King on 9/25/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol CustomModal {
    
}

extension CustomModal where Self:UIViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(animationComplete: @escaping () -> ()) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        },
           completion: {(finished: Bool) in
            if (finished) {
                self.view.removeFromSuperview()
                animationComplete()
            }
        })
        
    }
}
