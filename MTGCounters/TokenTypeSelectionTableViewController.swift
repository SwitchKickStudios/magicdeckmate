//
//  TokenTypeSelectionTableViewController.swift
//  MTGCounters
//
//  Created by William King on 4/30/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit
import Hero

class TokenTypeSelectionTableViewController: MDMTableViewController {
    
    let colorManager = ColorManager()
    
    var tokensDictionary = Dictionary<String,[CardData]>()
    var tokenTypes = Array<String>()
    var tokenTypesDictionary = Dictionary<String,[String]>() // for right side index
    var tokenAlphabeticalSections = Array<String>() // for right side index
    var players = Array<PlayerData>()
    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var deckMode: Bool = false
    var deckName: String = ""

    var selectedRow = 1
    
    // Parameters
    let viewTitle = "Tokens"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.formatTokenTypes()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.orientNavigationController(orientation: OrientationService().convertToDeviceOrientationFrom(interfaceOrientation: UIApplication.shared.statusBarOrientation))
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshTable()
        self.UISetup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController) {
            if playerNumber == 2 && UIApplication.shared.statusBarOrientation.isPortrait {
                // Make a snapshot of the tableview, place it over the view, then flip it
                // When the navigation controller is turned right side up, the snapshot will be upside down
                let snap = self.view.snapshotView(afterScreenUpdates: false)
                self.view.addSubview(snap!)
                snap?.transform = CGAffineTransform(rotationAngle: .pi)
                HeroWrapper.unCoverDown()
                self.delegate?.openView(selection: "main", animate: false)
            } else {
                HeroWrapper.unCoverUp()
                self.delegate?.openView(selection: "main", animate: false)
            }
           
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.orientNavigationController(orientation: UIDevice.current.orientation)
    }
    
    override var prefersStatusBarHidden: Bool {
        if self.playerNumber == 2 {
            return true
        } else {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        self.navigationController?.navigationBar.barStyle = self.colorManager.getNavigationBarStyleFor(scheme: self.colorScheme)
        return self.colorManager.getStatusBarColorFor(scheme: self.colorScheme)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func orientNavigationController(orientation: UIDeviceOrientation) {
        if self.playerNumber == 2 && orientation.isPortrait {
            self.navigationController?.view.transform = CGAffineTransform(rotationAngle: .pi)
        } else {
            self.navigationController?.view.transform = CGAffineTransform(rotationAngle: 0)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return tokenAlphabeticalSections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let indexKey = tokenAlphabeticalSections[section]
        if let indexValues = tokenTypesDictionary[indexKey] {
            return indexValues.count
        }
        return 0
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return tokenAlphabeticalSections
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TokenTypeCell", for: indexPath) as! TokenTypeTableViewCell

        let indexKey = tokenAlphabeticalSections[indexPath.section]
        if let indexValues = tokenTypesDictionary[indexKey] {
            cell.tokenTypeLabel.text = indexValues[indexPath.row]
        }
        cell.contentView.backgroundColor = self.primaryColor
        cell.backgroundColor = self.primaryColor
        cell.tokenTypeLabel.textColor = self.secondaryColor
        cell.separator.backgroundColor = self.color3

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Change the text color of the color label to signify it is highlighted
        let cell = tableView.cellForRow(at: indexPath) as! TokenTypeTableViewCell
        cell.tokenTypeLabel.textColor = UIColor.lightGray
        cell.contentView.backgroundColor = colorManager.getHighlightColor(for: GameData.sharedInstance.colorSchemes[self.playerNumber-1], color: primaryColor!)
        
        // Decide whether to segue to Recent Tokens Table VC or Token Variation Table VC
        let selectedType = cell.tokenTypeLabel.text!
        if selectedType == "Recent Tokens" {
            
            let recentTokensVC = storyboard?.instantiateViewController(withIdentifier: "RecentTokensTableVC") as! RecentTokensTableViewController
            recentTokensVC.tokenCategory = selectedType
            recentTokensVC.players = players
            recentTokensVC.playerNumber = self.playerNumber
            recentTokensVC.deckMode = self.deckMode
            recentTokensVC.deckName = self.deckName
            navigationController?.pushViewController(recentTokensVC, animated: true)
            
        } else {
            
            let tokensVC = storyboard?.instantiateViewController(withIdentifier: "TokenVariationTableVC") as! TokenVariationSelectionTableViewController
            tokensVC.tokensDictionary = self.tokensDictionary
            // Define and pass the variations within each token type as an array
            if let tokenVariation = self.tokensDictionary[selectedType] {
                tokensVC.tokenVariations = tokenVariation
            }
            tokensVC.tokenCategory = selectedType
            tokensVC.players = players
            tokensVC.playerNumber = self.playerNumber
            tokensVC.deckMode = self.deckMode
            tokensVC.deckName = self.deckName
            navigationController?.pushViewController(tokensVC, animated: true)
            
        }
    }
    
    func formatTokenTypes() {
        tokenTypes = tokensDictionary.keys.sorted()
        
        for tokenType in tokenTypes {
            let indexKey = String(tokenType.prefix(1))
            if var indexValues = tokenTypesDictionary[indexKey] {
                indexValues.append(tokenType)
                tokenTypesDictionary[indexKey] = indexValues
            } else {
                tokenTypesDictionary[indexKey] = [tokenType]
            }
        }
        tokenAlphabeticalSections = [String](tokenTypesDictionary.keys)
        tokenAlphabeticalSections = tokenAlphabeticalSections.sorted()
        tokenAlphabeticalSections.insert("Top", at: 0)
        tokenTypesDictionary["Top"] = ["Recent Tokens"]
        
        tokenTypes.insert("Recent Tokens", at: 0)
    }
    
    func refreshTable() {
        DispatchQueue.main.async  {
            self.tableView.reloadData()
            return
        }
    }

}

// MARK: STYLE SETUP
extension TokenTypeSelectionTableViewController {
    
    func UISetup() {
        self.title = self.viewTitle
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = colorManager.getAssetsForColor(scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
        
        // Set Colors
        self.formatNavigationBar()
        self.tableView.backgroundColor = primaryColor
        self.tableView.sectionIndexColor = secondaryColor
        self.tableView.sectionIndexBackgroundColor = primaryColor
        self.tableView.sectionIndexTrackingBackgroundColor = colorManager.getHighlightColor(for: colorScheme, color: primaryColor!)
        self.tableView.backgroundView?.backgroundColor = primaryColor
        
        if self.colorScheme == .colorless || self.colorScheme == .green || self.colorScheme == .white {
            self.tableView.indicatorStyle = .black
        } else {
            self.tableView.indicatorStyle = .white
        }
    }
}







