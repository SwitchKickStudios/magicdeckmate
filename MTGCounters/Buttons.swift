//
//  Buttons.swift
//  MTGCounters
//
//  Created by William King on 5/15/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation
import UIKit


// SERVICE CLASS THAT HAS FUNCTIONS FOR BUTTONS
class Buttons {
    
    // Parameters
    let hamburgerLineHeight = 2
    let hamburgerLineWidth = 25    
    
    func circleButton(diameter: CGFloat, color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)
        let context = UIGraphicsGetCurrentContext()!
        context.saveGState()
        
        let rect = CGRect(x: 0, y: 0, width: diameter, height: diameter)
        context.setFillColor(color.cgColor)
        context.fillEllipse(in: rect)
        
        context.restoreGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return img
    }
    
    // Creates the Hamburger Menu Image
    func defaultBlackMenu() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: hamburgerLineWidth, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: hamburgerLineWidth, height: hamburgerLineHeight)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: hamburgerLineWidth, height: hamburgerLineHeight)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: hamburgerLineWidth, height: hamburgerLineHeight)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    func defaultWhiteMenu() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: hamburgerLineWidth, height: hamburgerLineHeight)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: hamburgerLineWidth, height: hamburgerLineHeight)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: hamburgerLineWidth, height: hamburgerLineHeight)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }

}

