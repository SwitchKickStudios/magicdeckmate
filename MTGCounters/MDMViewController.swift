//
//  MDMViewController.swift
//  MTGCounters
//
//  Created by Conor King on 3/25/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class MDMViewController: UIViewController {

    // VARIABLES
    var primaryColor: UIColor?
    var secondaryColor: UIColor?
    var color3: UIColor?
    var color4: UIColor?
    var color5: UIColor?
    var colorScheme: ColorSchemes = .colorless
    
    // PARAMETERS
    let deleteButtonColor = UIColor.red
    var donateButtonBorderColor: CGColor = UIColor(hex: "0DFE16").cgColor
    let editButtonColor = UIColor.white
    
    // STYLING FUNCTIONS -----------------
    
    // Assign actions to button based on button type
    func formatButtonOnAction(button: UIButton, type: ButtonTypes) {
        switch type {
            
        case .standardButton, .smallButton:
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.maskShadow(sender:)), for: .touchDown)
            button.addTarget(self, action: #selector(self.unHighlightButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
            
        case .circlarMenuButton:
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.maskShadow(sender:)), for: .touchDown)
            
        case .circularMenuButtonSmallShadow:
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.maskShadow(sender:)), for: .touchDown)

        case .menuItem:
            button.addTarget(self, action: #selector(self.unHighlightButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
            
        case .editButton: // Comes into the view as color scheme but will get toggled to self.editButtonColor when pressed
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.maskShadow(sender:)), for: .touchDown)
            button.addTarget(self, action: #selector(self.unHighlightEditButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightEditButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
            
        case .editButton2: // Comes into the view as self.editButtonColor
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.maskShadow(sender:)), for: .touchDown)
            button.addTarget(self, action: #selector(self.unHighlightEditButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightEditButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightEditButtonColor(sender:)), for: .touchDown)
            
        case .deleteButton: // Comes into the view as color scheme but will get toggled to self.deleteButtonColor when pressed
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.maskShadow(sender:)), for: .touchDown)
            button.addTarget(self, action: #selector(self.unHighlightDeleteButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightDeleteButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
            
        case .deleteButton2: // Comes into the view as self.deleteButtonColor
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unMaskShadow(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.maskShadow(sender:)), for: .touchDown)
            button.addTarget(self, action: #selector(self.unHighlightDeleteButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightDeleteButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightDeleteButtonColor(sender:)), for: .touchDown)
            
        }
    }
    
    // Changes a delete button depending on whether it should be 'on' or 'off'
    func toggleDeleteButton(button: UIButton, on: Bool) {
        if on {
            button.setTitleColor(UIColor.white, for: .normal)
            button.addTarget(self, action: #selector(self.unHighlightButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightDeleteButtonColor(sender:)), for: .touchDown)
        } else {
            button.setTitleColor(self.secondaryColor, for: .normal)
            button.addTarget(self, action: #selector(self.unHighlightDeleteButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightDeleteButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
        }
        self.view.layoutIfNeeded()
    }
    
    // Changes an edit button depending on whether it should be 'on' or 'off'
    func toggleEditButton(button: UIButton, on: Bool) {
        if on {
            button.setTitleColor(UIColor.black, for: .normal)
            button.addTarget(self, action: #selector(self.unHighlightButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightEditButtonColor(sender:)), for: .touchDown)
        } else {
            button.setTitleColor(self.secondaryColor, for: .normal)
            button.addTarget(self, action: #selector(self.unHighlightEditButtonColor(sender:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(self.unHighlightEditButtonColor(sender:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
        }
        self.view.layoutIfNeeded()
    }
    
    // -- ACTIONS FOR SCHEME CONFORMING BUTTONS --
    // Enable shadows
    @objc func unMaskShadow(sender: UIButton) {
        sender.layer.masksToBounds = false
    }
    // Disable shadows
    @objc func maskShadow(sender: UIButton) {
        sender.layer.masksToBounds = true
    }
    // Change color
    @objc func highlightButtonColor(sender: UIButton) {
        sender.backgroundColor? = ColorManager().getHighlightColor(for: self.colorScheme, color: primaryColor!)
    }
    // Change color back
    @objc func unHighlightButtonColor(sender: UIButton) {
        sender.backgroundColor? = primaryColor!
    }
    
    // -- ACTIONS FOR NON-SCHEME BUTTONS --
    // Change color of delete button
    @objc func highlightDeleteButtonColor(sender: UIButton) {
        sender.backgroundColor? = ColorManager().getHighlightColor(for: nil, color: deleteButtonColor)
    }
    // Change color back of cell button
    @objc func unHighlightDeleteButtonColor(sender: UIButton) {
        sender.backgroundColor? = self.deleteButtonColor
    }
    @objc func highlightEditButtonColor(sender: UIButton) {
        sender.backgroundColor? = ColorManager().getHighlightColor(for: nil, color: editButtonColor)
    }
    // Change color back of cell button
    @objc func unHighlightEditButtonColor(sender: UIButton) {
        sender.backgroundColor? = self.editButtonColor
    }
}
