//
//  DecksViewController.swift
//  MTGCounters
//
//  Created by Conor King on 8/5/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class DecksViewController: MDMViewController, UITableViewDataSource, UITableViewDelegate {

    let colorManager = ColorManager()
    
    @IBOutlet weak var decksTableView: UITableView!
    @IBOutlet weak var bottomBarBorder: UIView!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var bottomBarBorderWidthConstraint: NSLayoutConstraint!
    
    // Parameters
    var viewTitle = "Decks"

    var delegate: MenuNavigationDelegate? = nil
    var players = Array<PlayerData>()
    var playerNumber = 1
    var savedDecks = [String:Any]()
    var savedDeckNames = [String]()
    var colorData: ColorData?
    
    var mode: DecksTableMode = .none
    var previousMode: DecksTableMode = .none // Used for finding out which buttons to hide
    var newDeckName = ""
    var inAnimationTransition: Bool = false // Used for detecting if the resfresh is based on the delete button
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        decksTableView.delegate = self
        decksTableView.dataSource = self
        
        if playerNumber != 2 {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(self.addButton(sender:)))
        }
        self.createDecksResources()
        self.sizingSetup()
        self.orientNavigationController(orientation: OrientationService().convertToDeviceOrientationFrom(interfaceOrientation: UIApplication.shared.statusBarOrientation))
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.inAnimationTransition = false
        self.mode = .none
        self.createDecksResources()
        self.refreshTable()
        self.UISetup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController) {
            if (self.isMovingFromParentViewController) {
                if playerNumber == 2 && UIApplication.shared.statusBarOrientation.isPortrait {
                    // Make a snapshot of the tableview, place it over the view, then flip it
                    // When the navigation controller is turned right side up, the snapshot will be upside down
                    let snap = self.view.snapshotView(afterScreenUpdates: false)
                    self.view.addSubview(snap!)
                    snap?.transform = CGAffineTransform(rotationAngle: .pi)
                    HeroWrapper.unCoverDown()
                    self.delegate?.openView(selection: "main", animate: false)
                } else {
                    HeroWrapper.unCoverUp()
                    self.delegate?.openView(selection: "main", animate: false)
                }
                
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        self.navigationController?.navigationBar.barStyle = self.colorManager.getNavigationBarStyleFor(scheme: self.colorScheme)
        return self.colorManager.getStatusBarColorFor(scheme: self.colorScheme)
    }
    
    override var prefersStatusBarHidden: Bool {
        if self.playerNumber == 2 {
            return true
        } else {
            return false
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.orientNavigationController(orientation: UIDevice.current.orientation)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func orientNavigationController(orientation: UIDeviceOrientation) {
        if self.playerNumber == 2 && orientation.isPortrait {
            self.navigationController?.view.transform = CGAffineTransform(rotationAngle: .pi)
        } else {
            self.navigationController?.view.transform = CGAffineTransform(rotationAngle: 0)
        }
    }

    @IBAction func deleteButton(_ sender: Any) {
        self.previousMode = self.mode
        if self.mode == .delete {
            self.mode = .none
            self.toggleDeleteButton(button: self.deleteButton, on: false)
        } else {
            self.mode = .delete
            self.toggleDeleteButton(button: self.deleteButton, on: true)
            self.toggleEditButton(button: self.editButton, on: false)
            self.editButton.backgroundColor = primaryColor
        }
        self.inAnimationTransition = true
        self.refreshTable()
        
    }
    
    @IBAction func editButton(_ sender: Any) {
        self.previousMode = self.mode
        if self.mode == .edit {
            self.previousMode = .edit
            self.mode = .none
            self.toggleEditButton(button: self.editButton, on: false)
        } else {
            self.mode = .edit
            self.toggleEditButton(button: self.editButton, on: true)
            self.toggleDeleteButton(button: self.deleteButton, on: false)
            self.deleteButton.backgroundColor = primaryColor
        }
        self.inAnimationTransition = true
        self.refreshTable()
    }

    
    
    // ADD A DECK
    @objc func addButton(sender: Any) {
        displayNamingAlert()
    }

    // Delete button actions for individual deck cells
    func deleteDeck(cell: DecksTableViewCell) {
        let deckName = cell.deckLabel.text
        self.savedDecks.removeValue(forKey: deckName!)
        UserDefaults.standard.set(self.savedDecks, forKey: "savedDecks")
        self.savedDeckNames = savedDecks.keys.sorted()
        self.refreshTable()
    }
    
    // Edit button actions for individual deck cells
    func editDeck(cell: DecksTableViewCell) {
        let listVC = storyboard?.instantiateViewController(withIdentifier: "DeckListVC") as! DeckListViewController
        listVC.editMode = self.mode == .edit ? true : false
        listVC.deckName = cell.deckLabel.text
        listVC.players = self.players
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    
    // Get and set this page's resources
    func createDecksResources() {
        self.savedDecks = UserDefaults.standard.dictionary(forKey: "savedDecks") ?? [String:Any]()
        self.savedDeckNames = savedDecks.keys.sorted()
    }
    
    // Display dialog to name and confirm creation of a new deck.
    func displayNamingAlert() {
        let alert = UIAlertController(title: "New Deck", message: "Enter a name for this deck:", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Deck Name"
        }
        let OKAction = UIAlertAction(title: "Create", style: .default) { (action:UIAlertAction!) in
            let nameField = alert.textFields![0].text!
            let savedDecks = UserDefaults.standard.dictionary(forKey: "savedDecks") ?? [String:Any]()
            if savedDecks[nameField] != nil {
                self.displayNamingError()
            } else {
                self.createNewDeck(name: nameField)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        }
        alert.addAction(OKAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // Display dialog to alert user that the deck name already exists
    func displayNamingError() {
        let alert = UIAlertController(title: "Please choose another name.", message: "A deck with that name already exists", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            self.displayNamingAlert()
        }
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // Create a new deck and segue to edit
    func createNewDeck(name: String) {
        let newCards = [[String:Any]]()
        self.savedDecks[name] = newCards
        UserDefaults.standard.set(self.savedDecks, forKey: "savedDecks")
        
        let listVC = storyboard?.instantiateViewController(withIdentifier: "DeckListVC") as! DeckListViewController
        listVC.editMode = true
        listVC.deckName = name
        let deckCards = savedDecks[name] as? [CardData]
        listVC.deckCards = deckCards ?? [CardData]()
        
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    
    // Show the alert dialog when deleting a deck
    func displayDeleteAlert(cell: DecksTableViewCell) {
        let deckName: String = cell.deckLabel.text!
        let alert = UIAlertController(title: "Deleting Deck", message: "Are you sure you want to delete \(deckName)", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            self.deleteDeck(cell: cell)
        }
        let noAction = UIAlertAction(title: "No", style: .cancel)  { (action:UIAlertAction!) in
        }
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // Method to refresh the list of decks
    func refreshTable() {
        DispatchQueue.main.async  {
            self.decksTableView.reloadData()
        }
    }

}


// MARK: - Table view data source
extension DecksViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedDeckNames.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let deckCell = tableView.dequeueReusableCell(withIdentifier: "DecksCell", for: indexPath) as! DecksTableViewCell
        if savedDeckNames.count > 0 {
            deckCell.contentView.backgroundColor = self.primaryColor
            deckCell.deckLabel.textColor = self.secondaryColor
            deckCell.separator.backgroundColor = self.color3
            deckCell.deckLabel.text = self.savedDeckNames[indexPath.row]
            self.configureCellButtons(deckCell: deckCell)
        } else {
            deckCell.isHidden = true
            deckCell.isUserInteractionEnabled = false
        }
        
        if let lastVisibleIndexPath = decksTableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                self.inAnimationTransition = false
            }
        }
        
        return deckCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Change the text color of the color label to signify it is highlighted
        let cell = tableView.cellForRow(at: indexPath) as! DecksTableViewCell
        cell.deckLabel.textColor = UIColor.lightGray
        cell.contentView.backgroundColor = self.colorManager.getHighlightColor(for: GameData.sharedInstance.colorSchemes[self.playerNumber-1], color: primaryColor!)
        
        // Segue to DeckList
        let listVC = storyboard?.instantiateViewController(withIdentifier: "DeckListVC") as! DeckListViewController
        listVC.editMode = false
        listVC.deckName = cell.deckLabel.text
        listVC.players = players
        listVC.playerNumber = self.playerNumber
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    
    
    // Determine if cell buttons are needed and set them
    func configureCellButtons(deckCell: DecksTableViewCell) {
        let styleManager = StyleManager(colorData: self.colorData!, scheme: self.colorScheme)
        
        switch self.mode {
        case .delete:
            deckCell.deleteCellButton.isUserInteractionEnabled = true
            deckCell.editCellButton.isUserInteractionEnabled = false
            
            styleManager.format(button: deckCell.deleteCellButton, type: .deleteButton2, iconImageView: nil)
            self.formatButtonOnAction(button: deckCell.deleteCellButton, type: .deleteButton2)
            
            // If loading cell because of the delete button, then animate the cell's delete button constraint
            // If not, display the delete button immediately
            if self.inAnimationTransition == true {
                deckCell.deleteButtonTrailingConstraint.constant = -75
                if self.previousMode == .edit {
                    deckCell.editButtonTrailingConstraint.constant = 0
                }
                deckCell.contentView.layoutIfNeeded()
                deckCell.deleteButtonTrailingConstraint.constant = 0
                if self.previousMode == .edit {
                    deckCell.editButtonTrailingConstraint.constant = -75
                }
                UIView.animate(withDuration: 0.5, animations: {
                    deckCell.contentView.layoutIfNeeded()
                })
            } else {
                deckCell.contentView.layoutIfNeeded()
                deckCell.editButtonTrailingConstraint.constant = -75
                deckCell.deleteButtonTrailingConstraint.constant = 0
                deckCell.contentView.layoutIfNeeded()
            }
            deckCell.deleteCellAction = { (cell) in
                self.displayDeleteAlert(cell: cell)
            }
            
        case .edit:
            deckCell.deleteCellButton.isUserInteractionEnabled = false
            deckCell.editCellButton.isUserInteractionEnabled = true
            
            styleManager.format(button: deckCell.editCellButton, type: .editButton2, iconImageView: nil)
            self.formatButtonOnAction(button: deckCell.editCellButton, type: .editButton2)
            
            // If loading cell because of the edit button, then animate the cell's delete button constraint
            // If not, display the edit button immediately
            if self.inAnimationTransition == true {
                deckCell.editButtonTrailingConstraint.constant = -75
                if self.previousMode == .delete {
                    deckCell.deleteButtonTrailingConstraint.constant = 0
                }
                deckCell.contentView.layoutIfNeeded()
                deckCell.editButtonTrailingConstraint.constant = 0
                if self.previousMode == .delete {
                    deckCell.deleteButtonTrailingConstraint.constant = -75
                }
                UIView.animate(withDuration: 0.5, animations: {
                    deckCell.contentView.layoutIfNeeded()
                })
            } else {
                deckCell.contentView.layoutIfNeeded()
                deckCell.editButtonTrailingConstraint.constant = 0
                deckCell.deleteButtonTrailingConstraint.constant = -75
                deckCell.contentView.layoutIfNeeded()
            }
            deckCell.editCellAction = { (cell) in
                self.editDeck(cell: cell)
            }
            
        default:
            if self.inAnimationTransition {
                if self.previousMode == .delete {
                    deckCell.deleteButtonTrailingConstraint.constant = 0
                    deckCell.editButtonTrailingConstraint.constant = -75
                } else if self.previousMode == .edit {
                    deckCell.editButtonTrailingConstraint.constant = 0
                    deckCell.deleteButtonTrailingConstraint.constant = -75
                }
                
                deckCell.contentView.layoutIfNeeded()
                deckCell.deleteButtonTrailingConstraint.constant = -75
                deckCell.editButtonTrailingConstraint.constant = -75
                UIView.animate(withDuration: 0.5, animations: {
                    deckCell.contentView.layoutIfNeeded()
                })
            } else {
                deckCell.contentView.layoutIfNeeded()
                deckCell.deleteButtonTrailingConstraint.constant = -75
                deckCell.editButtonTrailingConstraint.constant = -75
                deckCell.contentView.layoutIfNeeded()
            }
            deckCell.deleteCellButton.isUserInteractionEnabled = false
            deckCell.editCellButton.isUserInteractionEnabled = false
            
        }
    }
    
}
// MARK: STYLE SETUP
extension DecksViewController {

    func sizingSetup() {
        self.editButton.set(fontSize: getButtonFontSize())
        self.deleteButton.set(fontSize: getButtonFontSize())
    }
    
    func UISetup() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.title = self.viewTitle
        
        if self.playerNumber == 2 {
            self.bottomBar.isHidden = true
            self.bottomBarBorder.isHidden = true
        }

        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        self.colorData = self.colorManager.getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: self.colorData!, scheme: self.colorScheme)
        self.setNeedsStatusBarAppearanceUpdate()
        self.secondaryColor = colorData?.secondaryColor
        self.primaryColor = colorData?.primaryColor
        self.color3 = colorData?.color3
        self.color4 = colorData?.color4
        
        // Set Colors
        self.view.backgroundColor = self.primaryColor
        self.navigationController?.navigationBar.barTintColor = self.primaryColor
        self.navigationController?.navigationBar.tintColor = self.secondaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor : self.secondaryColor! ]
        self.decksTableView.backgroundColor = self.primaryColor
        self.bottomBar.backgroundColor = self.color4
        styleManager.format(borderView: self.bottomBarBorder, widthConstraint: self.bottomBarBorderWidthConstraint)
        
        // Buttons
        styleManager.format(button: self.editButton, type: .editButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.editButton, type: .editButton)
        styleManager.format(button: self.deleteButton, type: .deleteButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.deleteButton, type: .deleteButton)
        
    }
    
    // Returns a font size based on device class
    func getButtonFontSize() -> CGFloat {
        if let currentTraits = self.navigationController?.traitCollection {
            if currentTraits.isIpad {
                return 20
            } else if currentTraits.isIphoneLandscape {
                return 18
            } else if currentTraits.isIphonePortrait {
                return 18
            }
        }
        return 20
    }
    
    
}



