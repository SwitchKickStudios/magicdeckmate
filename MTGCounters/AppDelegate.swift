//
//  AppDelegate.swift
//  MTGCounters
//
//  Created by William King on 9/20/16.
//  Copyright © 2016 ConorKing. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        loadPlistsIntoDefaults()
        
        let reviewHelper = StoreReviewHelper()
        reviewHelper.incrementAppOpenedCount()
        reviewHelper.checkAndAskForReview()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate {
    
    // Method to load the tokens and counters data from plists
    // for startup
    func loadPlistsIntoDefaults() {
        if let path = Bundle.main.path(forResource: "Tokens", ofType: "plist") {
            if let plistDictionary = NSDictionary(contentsOfFile: path) as? [String:[Any]] {
                UserDefaults.standard.set(plistDictionary, forKey: "TokensDictionary")
            } else {
                debugPrint("ERROR LOADING TOKENS")
            }
        }
        if let path = Bundle.main.path(forResource: "Counters", ofType: "plist") {
            if let plistDictionary = NSDictionary(contentsOfFile: path) as? [String:[String]] {
                UserDefaults.standard.set(plistDictionary, forKey: "CountersDictionary")
            } else {
                debugPrint("ERROR LOADING COUNTERS")
            }
        }
    }
    
}



















