//
//  RecentCounterTableViewCell.swift
//  MTGCounters
//
//  Created by William King on 5/27/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class RecentCounterTableViewCell: UITableViewCell {

    @IBOutlet weak var recentCounterLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
