//
//  UIApplicationExtension.swift
//  MTGCounters
//
//  Created by Conor King on 6/2/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}
