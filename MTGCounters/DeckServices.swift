//
//  DeckServices.swift
//  MTGCounters
//
//  Created by Conor King on 8/9/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class DecksServices {
    
    
    
    // SUBMIT A CARD TO USER DEFAULTS
    
    func submitTo(deck: String, card: CardData, errorView: UIViewController, completion: @escaping () -> ()) {
        var savedDecks = UserDefaults.standard.dictionary(forKey: "savedDecks")
        var cards = savedDecks?[deck] as! [[String:Any]]
        if cards.count >= GameData.sharedInstance.maxCards2[1]! {
            // Alert for too many cards in deck already
            let alert = UIAlertController(title: "Deck Full", message: "This deck already has the maximum amount of cards.", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                completion()
            }
            alert.addAction(OKAction)
            errorView.present(alert, animated: true, completion: nil)
        } else {
            // Create new card and add it to the deck, then save
            let newCard = self.convertToDictionary(cardData: card)
            cards.append(newCard)
            savedDecks?[deck] = cards
            UserDefaults.standard.set(savedDecks, forKey: "savedDecks")
            completion()
        }
    }
    
    
    
    // CONVERT FROM CARDDATA TO A DICTIONARY
    
    func convertToDictionary(cardData: CardData) -> [String:Any] {
        // Create new card and add it to the deck, then save
        let cardType = cardData.type == .token ? "token" : "counter"
        let newCard: [String:Any] = [
            "description":cardData.description!,
            "name":cardData.name!,
            "category":cardData.category!,
            "color":cardData.color,
            "stats":cardData.stats,
            "type":cardType
        ]
        return newCard
    }
    
    
    
    // RETRIEVE AN ARRAY OF CARD DATA FOR A GIVEN DECKNAME
    
    func getCardsFor(deck: String) -> [CardData] {
        let savedDecks = UserDefaults.standard.dictionary(forKey: "savedDecks")
        let cards = savedDecks?[deck] as! [[String:Any]]
        var cardsArray = [CardData]()
        for card in cards {
            let cardType: CardType = card["type"] as! String == "token" ? .token : .counter
            let newCardData: CardData = CardData(description: card["description"] as! String
                , name: card["name"] as! String
                , type: cardType
                , category: card["category"] as! String
                , color: card["color"] as! [String]
                , stats: card["stats"] as! String)
            cardsArray.append(newCardData)
        }
        return cardsArray
    }
}
