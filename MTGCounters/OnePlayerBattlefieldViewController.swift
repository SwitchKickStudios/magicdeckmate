//
//  OnePlayerBattlefieldViewController.swift
//  MTGCounters
//
//  Created by William King on 4/29/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit
import Hero
import DynamicColor

class OnePlayerBattlefieldViewController: PlayerBattlefieldViewController, MenuNavigationDelegate, ResetDelegate, TokenDelegate, CounterDelegate, BattlefieldUpdatePlayerDelegate, ColorSelectDelegate, LifeSelectDelegate, PlayerSelectDelegate {

    // Inputs
    var player1 = PlayerData(maxPlayers: 1)
    var newCard: CardData?
    
    let game = GameData.sharedInstance
    let colorManager = ColorManager()
    
    // UI Outlets
    @IBOutlet weak var battlefieldLevelView: UIView!
    @IBOutlet weak var touchOutsideMenuDetectionLevelView: UIView!
    @IBOutlet weak var menuLevelView: UIView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var headBackground: UIImageView!
    @IBOutlet weak var bottomBarBackground: UIImageView!
    @IBOutlet weak var lifeCounterBackgroundView: UIView!
    @IBOutlet weak var battlefieldGuideView: UIView!
    @IBOutlet weak var staticBattlefieldGuideView: UIView!
    @IBOutlet weak var centerMenuButton: UIButton!
    @IBOutlet weak var centerMenuIcon: UIImageView!
    @IBOutlet weak var playerLifeLabel: UILabel!
    @IBOutlet weak var decreaseLifeButton: UIButton!
    @IBOutlet weak var increaseLifeButton: UIButton!
    @IBOutlet weak var headSectionBorder: UIView!
    @IBOutlet weak var bottomSectionBorder: UIView!
    
    // Parameters
    var areaOutsideMenuAlpha: CGFloat = 0.65
    
    @IBOutlet weak var headSectionBorderWidth: NSLayoutConstraint!
    @IBOutlet weak var bottomSectionBorderWidth: NSLayoutConstraint!
    @IBOutlet weak var headSectionTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var battlefieldGuideViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var battlefieldGuideViewBottomConstraint: NSLayoutConstraint!
    
    var showAlert: CustomAlertViewController? // Used to show the alert on viewWillAppear for a balanced transition
    var currentMenu: UIViewController? // Store the last menu opened in case we need it for switching storyboard elements
    var isTransitioning = false // Flag to determine whether the device is in the middle of an orientation transition
    var battlefieldCollectionVC: BattlefieldCollectionViewController? = nil
    var currentMenuString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.findEmptySlotForNewCard()
        self.resetColorScheme()
        self.configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.resetColorScheme()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if showAlert != nil {
            showAlert?.show()
            showAlert = nil
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.determineShowTutorial()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return colorManager.getStatusBarColorFor(scheme: self.colorScheme)
    }
    
    // Wait for the view to finish changing orientation
    // Then adjust the layout constraints
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            self.adjustBattlefieldVerticalConstraints()
            self.battlefieldCollectionVC?.collectionViewLayout.invalidateLayout()
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            self.adjustMenuOrientation()
        })
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func centerMenuButton(_ sender: UIButton) {
        openView(selection: "main", animate: true)
    }

    @IBAction func decreaseLifeButton(_ sender: Any) {
        player1.life -= 1
        playerLifeLabel.text = String(player1.life)
    }
    
    @IBAction func increaseLifeCounter(_ sender: Any) {
        player1.life += 1
        playerLifeLabel.text = String(player1.life)
    }
    
}


// MARK: BATTLEFIELD CONFIGURATION
extension OnePlayerBattlefieldViewController {
    // Find empty slot in PlayerData, if fails, present alert
    func findEmptySlotForNewCard() {
        if newCard != nil {
            if player1.findEmptySlot(for: newCard!) == false  {
                
                // Create an alert before calling the delegate method do perform the proper reset
                let alert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertViewController
                alert.player = 1
                alert.setAlertText(title: "", message: "No more room on the battlefield.", b1: "Okay", b2: "")

                alert.button1Action = { (alertVC) in
                }
                alert.button2ShouldHide = true
                self.showAlert = alert
                
            }
            newCard = nil
        }
    }
    
}

// MARK : BATTLEFIELD ACTIONS
extension OnePlayerBattlefieldViewController {
    
    
    // UPDATE SLOT INFO
    // TokenDelegate FUNCTION
    func updateToken(id: Int, orientation: SlotState, quantity: Int) {
        let index = self.player1.cardIDs.index(of: id)!
        self.player1.cards[index].quantity = quantity
        self.player1.battledfieldState[index] = orientation
        if orientation == .empty {
            self.player1.cardIDs[index] = 0
        }
        self.battlefieldCollectionVC?.player = player1
    }
    // CounterDelegate FUNCTION
    func updateCounter(id: Int, orientation: SlotState, counterValue: Int) {
        let index = self.player1.cardIDs.index(of: id)!
        self.player1.cards[index].quantity = counterValue
        self.player1.battledfieldState[index] = orientation
        if orientation == .empty {
            self.player1.cardIDs[index] = 0
        }
        self.battlefieldCollectionVC?.player = player1
    }
    // BOTH CounterDelegate function and TokenDelegate function
    func getPlayerData() -> PlayerData {
        return self.player1
    }
    
    // BattlefieldUpdatePlayerDelegate FUNCTION
    // Update self.player and TwoPlayerBattlefieldVC.player
    func updatePlayer(data: PlayerData) {
        self.player1 = data
        self.adjustBattlefieldVerticalConstraints()
    }
    
    // RESET BATTLEFIELD - ResetDelegate Method
    // Switch method that resets the battlefield according to ResetType
    func resetBattlefield(playerNumber: Int, type: ResetType) {
        switch type {
            
        // RESET LIFE
        case .resetLife:
                
                // RESET LIFE: set player1 life to GameData singleton starting life
                self.resetLifeTotal()
        
        // SET CARD VALUES TO ZERO
        case .setCardsToZero:
                
                // SET CARDS TO ZERO: Loop over the each cards in player1 cards, settings the quantity to 0 and the respective battlefield status to untapped
                for slot in 1 ... self.player1.cards.count {
                    if self.player1.battledfieldState[slot-1] != .empty {
                        self.player1.cards[slot-1].quantity = 0
                        self.player1.battledfieldState[slot-1] = .untapped
                    }
                }
                self.initializeBattlefieldCollectionView()
        
        // CLEAR CARDS
        case .resetCards:
                
                // CLEAR CARDS: Loop over player1 cards, replacing with empty CardData
                // Loop over player1 battlefield state, replacing with .empty
                self.player1.removeAllCards()
                self.initializeBattlefieldCollectionView()

            
        // RESET ALL
        case .resetAll:
                
                // RESET ALL: set player1 life to GameData singleton starting life
                // Loop over player1 cards, replacing with empty CardData
                // Loop over player1 battlefield state, replacing with .empty
                self.resetLifeTotal()
                self.player1.removeAllCards()
                self.initializeBattlefieldCollectionView()
            
        }
    }
    
    // Delegate method to reset life from Life Selection
    func resetLifeTotal() {
        // RESET LIFE: set player1 life to GameData singleton starting life
        self.player1.life = self.game.startingLife
        self.playerLifeLabel.text = String(self.player1.life)
    }
    
    // SWITCH BATTLEFIELD
    // This is the action taken when changing global Player Number
    // PlayerSelectDelegate function
    func changeBattlefieldTo(players: Int) {
        switch players {
        case 1:
            
            // ONE PLAYER
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            let onePlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "OnePlayerBattlefieldVC") as! OnePlayerBattlefieldViewController
            self.navigationController?.setViewControllers([onePlayerVC], animated: true)
            
        case 2:
            
            // TWO PLAYERS
            self.presentedViewController?.dismiss(animated: true, completion: nil)
            let twoPlayerVC = self.storyboard?.instantiateViewController(withIdentifier: "TwoPlayerBattlefieldVC") as! TwoPlayerBattlefieldViewController
            self.navigationController?.setViewControllers([twoPlayerVC], animated: true)

        default:
            return
        }
    }
    
}

// MARK: SUBVIEWS
extension OnePlayerBattlefieldViewController {
    
    // MenuNavigationDelegate function - Handles the navigation from this battlefield
    func openView(selection: String, animate: Bool) {
        self.currentMenu = nil
        
        var playersArray = Array<PlayerData>()
        playersArray.append(player1)
        
        switch selection {
            
        case "main":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            var menuVC = self.storyboard?.instantiateViewController(withIdentifier: "CenterMenuVC1") as! CenterMenuViewController
            if UIApplication.shared.statusBarOrientation.isLandscape  {
                menuVC = self.storyboard?.instantiateViewController(withIdentifier: "CenterMenuVC2") as! CenterMenuViewController
            }
            menuVC.delegate = self
            menuVC.playerNumber = 1
            self.currentMenu = menuVC
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: menuVC, animate: animate)
            
        case "tokens":
            let tokensVC = storyboard?.instantiateViewController(withIdentifier: "TokenTypeTableVC") as! TokenTypeSelectionTableViewController
            tokensVC.players = playersArray
            tokensVC.delegate = self
            tokensVC.tokensDictionary = DataFormatService().getTokenDictionary()
            pushViewControllerForMenu(newViewController: tokensVC)
            
        case "counters":
            let countersVC = storyboard?.instantiateViewController(withIdentifier: "CounterCategoryTableVC") as! CounterCategoryTableViewController
            countersVC.players = playersArray
            countersVC.countersDictionary = DataFormatService().getCounterDictionary()
            countersVC.delegate = self
            pushViewControllerForMenu(newViewController: countersVC)
            
        case "decks":
            let decksVC = storyboard?.instantiateViewController(withIdentifier: "DecksVC") as! DecksViewController
            decksVC.players = playersArray
            decksVC.delegate = self
            pushViewControllerForMenu(newViewController: decksVC)
            
        case "tips":
            let tipsVC = storyboard?.instantiateViewController(withIdentifier: "TipsTableVC") as! TipsTableViewController
            tipsVC.delegate = self
            tipsVC.origin = self.currentMenuString
            if ReachabilityTest.isConnectedToNetwork() {
                pushViewControllerForMenu(newViewController: tipsVC)
            } else {
                let alertView = UIAlertController(title: "Network Error", message: "It appears there an issue with your connection.", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            }
        
        case "dice":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let diceVC = storyboard?.instantiateViewController(withIdentifier: "DiceVC") as! DiceViewController
            diceVC.playerNumber = 1
            diceVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: diceVC, animate: animate)
            
        case "resets":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let resetVC = storyboard?.instantiateViewController(withIdentifier: "ResetVC") as! ResetViewController
            resetVC.delegate = self
            resetVC.resetDelegate = self
            resetVC.playerNumber = 1
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: resetVC, animate: animate)

        case "settings":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let settingsVC = storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsViewController
            settingsVC.playerNumber = 1
            settingsVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: settingsVC, animate: animate)
            
        case "players":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let playerSelectVC = storyboard?.instantiateViewController(withIdentifier: "PlayerSelectVC") as! PlayerSelectViewController
            playerSelectVC.delegate = self
            playerSelectVC.playerDelegate = self
            playerSelectVC.playerNumber = 1
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: playerSelectVC, animate: animate)
            
        case "color":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            var colorVC = storyboard?.instantiateViewController(withIdentifier: "ColorSelectVC1") as! ColorSelectViewController
            if UIApplication.shared.statusBarOrientation.isLandscape  {
                colorVC = self.storyboard?.instantiateViewController(withIdentifier: "ColorSelectVC2") as! ColorSelectViewController
            }
            colorVC.delegate = self
            colorVC.colorDelegate = self
            colorVC.playerNumber = 1
            self.currentMenu = colorVC
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: colorVC, animate: animate)
        
        case "startingLife":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let lifeVC = storyboard?.instantiateViewController(withIdentifier: "LifeSelectVC") as! LifeSelectViewController
            lifeVC.delegate = self
            lifeVC.lifeDelegate = self
            lifeVC.playerNumber = 1
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: lifeVC, animate: animate)
            
        case "welcomeMessage":
            removeLastSubView(from: self.menuLevelView)
            let supportVC = storyboard?.instantiateViewController(withIdentifier: "WelcomeMessageVC") as! WelcomeMessageViewController
            supportVC.delegate = self
            supportVC.tutorialMode = self.initialOpen
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: supportVC, animate: animate)
            
        case "support":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let supportVC = storyboard?.instantiateViewController(withIdentifier: "SupportTutorialVC") as! SupportTutorialViewController
            supportVC.delegate = self
            supportVC.tutorialMode = self.initialOpen
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: supportVC, animate: animate)
            self.initialOpen = false // Ensure we don't accidentally show the tutorial for thevery first opening of the app when we aren't suppose to.
            
        case "tokenControls":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let tokenTutorialVC = storyboard?.instantiateViewController(withIdentifier: "TokensTutorialVC") as! TokensTutorialViewController
            tokenTutorialVC.delegate = self
            tokenTutorialVC.tutorialMode = self.initialOpen
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: tokenTutorialVC, animate: animate)
            
        case "tokenControlsExample":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let tokenTutorialExampleVC = storyboard?.instantiateViewController(withIdentifier: "TokensTutorialExampleVC") as! TokensTutorialExampleViewController
            tokenTutorialExampleVC.delegate = self
            tokenTutorialExampleVC.tutorialMode = self.initialOpen
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: tokenTutorialExampleVC, animate: animate)
            
        case "counterControls":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let countersTutorialVC = storyboard?.instantiateViewController(withIdentifier: "CountersTutorialVC") as! CountersTutorialViewController
            countersTutorialVC.delegate = self
            countersTutorialVC.tutorialMode = self.initialOpen
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: countersTutorialVC, animate: animate)
            
        case "counterControlsExample":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let countersTutorialExampleVC = storyboard?.instantiateViewController(withIdentifier: "CountersTutorialExampleVC") as! CountersTutorialExampleViewController
            countersTutorialExampleVC.delegate = self
            countersTutorialExampleVC.tutorialMode = self.initialOpen
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: countersTutorialExampleVC, animate: animate)
            
        case "menuNavigationControls":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let menuNavigationControlsVC = storyboard?.instantiateViewController(withIdentifier: "MenuNavigationTutorialVC") as! MenuNavigationTutorialViewController
            menuNavigationControlsVC.delegate = self
            menuNavigationControlsVC.tutorialMode = self.initialOpen
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: menuNavigationControlsVC, animate: animate)

        default:
            print("MENU ERROR - selection not recognized")
        }
        
        self.currentMenuString = selection
    }
    
    // MenuNavigationDelegate function - Turns off the guideViews associated with mnu navigation
    // Used in the case where a subview is closed from an elements not in this ViewController
    func turnOffGuideViews() {
        self.touchOutsideMenuDetectionLevelView.isUserInteractionEnabled = false
        self.touchOutsideMenuDetectionLevelView.isHidden = true
        self.menuLevelView.isUserInteractionEnabled = false
    }
    
    // MenuNavigationDelegate function - Close the menu
    // Menu Closing Animation
    func closeMenu() {
        let snap = self.menuLevelView.snapshotView(afterScreenUpdates: false)
        snap?.center = self.menuLevelView.center
        snap?.isUserInteractionEnabled = false
        self.view.addSubview(snap!)
        self.touchOutsideMenuDetectionLevelView.isUserInteractionEnabled = false
        self.removeLastSubView(from: self.menuLevelView)
        let animator = UIViewPropertyAnimator(duration: 0.4, curve: .linear)
        animator.addAnimations {
            snap?.alpha = 0.0
            self.touchOutsideMenuDetectionLevelView.alpha = 0.0
        }
        animator.startAnimation()
        animator.addCompletion({ (position) in
            if position == .end {
                snap?.removeFromSuperview()
                self.menuLevelView.isUserInteractionEnabled = false
                self.touchOutsideMenuDetectionLevelView.isHidden = true
            }
        })
    }
    
    // Turn the touch outside menu zone on
    // Add viewcontroller to given zone aka guideView
    func addViewControllerAsSubView(guideView: UIView, newViewController: UIViewController, animate: Bool) {
        self.touchOutsideMenuDetectionLevelView.isUserInteractionEnabled = true
        
        // Set up local style manager
        let colorScheme = self.game.colorSchemes[0]
        let colorData = colorManager.getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: colorScheme)
        styleManager.format(view: newViewController.view)
        newViewController.view.frame = guideView.bounds
        newViewController.view.clipsToBounds = true
        self.addChildViewController(newViewController)
        guideView.addSubview(newViewController.view)
        newViewController.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        if animate == true {
            let animator = UIViewPropertyAnimator(duration: 0.4, curve: .linear)
            let animator2 = UIViewPropertyAnimator(duration: 0.2, curve: .linear)
            self.touchOutsideMenuDetectionLevelView.alpha = 0.0
            self.touchOutsideMenuDetectionLevelView.isHidden = false
            guideView.alpha = 0.0
            animator.addAnimations {
                self.touchOutsideMenuDetectionLevelView.alpha = self.areaOutsideMenuAlpha
            }
            animator2.addAnimations {
                guideView.alpha = 1.0
            }
            animator.startAnimation()
            animator2.startAnimation()
        } else {
            self.touchOutsideMenuDetectionLevelView.alpha = self.areaOutsideMenuAlpha
            self.touchOutsideMenuDetectionLevelView.isHidden = false
        }
    }
    
    // Prepare and push a viewController for the menu navigation
    func pushViewControllerForMenu(newViewController: UIViewController) {
        HeroWrapper.coverDown()
        self.touchOutsideMenuDetectionLevelView.isUserInteractionEnabled = false
        self.menuLevelView.isUserInteractionEnabled = false
        navigationController?.pushViewController(newViewController, animated: true)
    }
    
    // Switch storyboard elements based on current orientation
    func adjustMenuOrientation() {
        let menu = self.menuLevelView.subviews.last
        if menu != nil && menu == self.currentMenu?.view {
            if self.currentMenu is CenterMenuViewController {
                self.openView(selection: "main", animate: false)
            } else if self.currentMenu is ColorSelectViewController {
                self.openView(selection: "color", animate: false)
            }
        }
    }
    
    // Create the battlefield as collection view and instantiate it in the guideView
    func initializeBattlefieldCollectionView() {
        self.removeLastSubView(from: self.battlefieldGuideView)
        let collectionView = storyboard?.instantiateViewController(withIdentifier: "BattlefieldCollectionVC") as! BattlefieldCollectionViewController
        player1.sortCardsForBattlefield()
        collectionView.player = self.player1
        collectionView.tokenDelegate = self
        collectionView.counterDelegate = self
        collectionView.updateDelegate = self
        self.battlefieldCollectionVC = collectionView
        self.addChildViewController(collectionView)
        collectionView.view.frame = battlefieldGuideView.bounds
        battlefieldGuideView.addSubview(collectionView.view)
        collectionView.collectionView?.backgroundColor = UIColor.clear
        collectionView.didMove(toParentViewController: self)
    }
    
    // If its the first time opening the app, show the tutorial
    func determineShowTutorial() {
        let showTutorial = UserDefaults.standard.bool(forKey: "hasShownInitialTutorial")
        if showTutorial == false {
            self.menuLevelView.isUserInteractionEnabled = true
            self.initialOpen = true
            self.openView(selection: "welcomeMessage", animate: false)
            UserDefaults.standard.set(true, forKey: "hasShownInitialTutorial")
        }
    }

    
    func removeLastSubView(from: UIView) {
        if from.subviews.last != nil {
            from.subviews.last?.removeFromSuperview()
        }
    }
    
}

// MARK: STYLING
extension OnePlayerBattlefieldViewController {
    
    // Modify the UI on load
    func configureUI() {
        setNeedsStatusBarAppearanceUpdate()
        self.headSectionTopConstraint.constant = 20
        self.touchOutsideMenuDetectionLevelView.isHidden = true
        self.playerLifeLabel.text = String(self.player1.life)
    }
    
    // Changes the color scheme to global variable for this player's color
    func resetColorScheme() {
        // Acquire colors
        self.colorScheme = self.game.colorSchemes[0]
        let colorData = colorManager.getAssetsForColor(scheme: self.colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: colorScheme)
        self.setNeedsStatusBarAppearanceUpdate()
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        styleManager.format(borderView: self.headSectionBorder, widthConstraint: self.headSectionBorderWidth)
        styleManager.format(borderView: self.bottomSectionBorder, widthConstraint: self.bottomSectionBorderWidth)

        // UI Elements
        self.background.image = UIImage(named: colorData.backgroundLarge!)
        self.bottomBarBackground.image = UIImage(named: colorData.backgroundLarge!)
        self.playerLifeLabel.textColor = colorData.secondaryColor
        
        self.touchOutsideMenuDetectionLevelView.backgroundColor = UIColor.black
        self.touchOutsideMenuDetectionLevelView.alpha = self.areaOutsideMenuAlpha
        
        styleManager.format(view: self.lifeCounterBackgroundView)
        styleManager.format(imageView: self.headBackground)
        
        // Buttons
        styleManager.format(button: self.increaseLifeButton, type: .standardButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.increaseLifeButton, type: .standardButton)
        styleManager.format(button: self.decreaseLifeButton, type: .standardButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.decreaseLifeButton, type: .standardButton)
        styleManager.format(button: self.centerMenuButton, type: .circlarMenuButton, iconImageView: self.centerMenuIcon)
        self.formatButtonOnAction(button: self.centerMenuButton, type: .circlarMenuButton)
        
        // Move stuff around
        self.adjustBattlefieldVerticalConstraints()
        
        // Remove and reload cards with proper colors
        self.initializeBattlefieldCollectionView()
    }
    
    // Adjust constraints to make sure cards and their labels are in proportion
    func adjustBattlefieldVerticalConstraints() {
        let orientation = UIApplication.shared.statusBarOrientation
        let device = UIDevice.current
        let currentBattlefieldHeight = self.staticBattlefieldGuideView.layer.bounds.height
        
        // iPhone Landscape
        if orientation.isLandscape && device.userInterfaceIdiom == .phone {
            if self.player1.getCardCount() >= self.game.growAtCards2[1]! {
                self.battlefieldGuideViewTopConstraint.constant = currentBattlefieldHeight * 0.22
                self.battlefieldGuideViewBottomConstraint.constant = currentBattlefieldHeight * 0.22
            } else if self.player1.getCardCount() >= self.game.growAtCards1[1]! {
                self.battlefieldGuideViewTopConstraint.constant = currentBattlefieldHeight * 0.14
                self.battlefieldGuideViewBottomConstraint.constant = currentBattlefieldHeight * 0.14
            } else {
                self.battlefieldGuideViewTopConstraint.constant = 8
                self.battlefieldGuideViewBottomConstraint.constant = 8
            }
        // iPhone Portrait
        } else if device.userInterfaceIdiom == .phone && self.player1.getCardCount() >= self.game.growAtCards1[1]! && self.player1.getCardCount() < self.game.growAtCards2[1]! {
            self.battlefieldGuideViewTopConstraint.constant = currentBattlefieldHeight * 0.07
            self.battlefieldGuideViewBottomConstraint.constant = currentBattlefieldHeight * 0.07
        // iPad
        } else {
            self.battlefieldGuideViewTopConstraint.constant = 8
            self.battlefieldGuideViewBottomConstraint.constant = 8
        }
        self.view.layoutIfNeeded()
    }
    
}


















