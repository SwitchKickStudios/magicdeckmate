//
//  LifeSelectViewController.swift
//  MTGCounters
//
//  Created by Conor King on 11/17/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol LifeSelectDelegate {
    func resetLifeTotal()
}

class LifeSelectViewController: MDMViewController, CloseModalDelegate, BackModalDelegate {

    var game = GameData.sharedInstance
    var colorManager = ColorManager()
    var lifeDelegate: LifeSelectDelegate? = nil
    var delegate: MenuNavigationDelegate? = nil
    var playerNumber = 1
    
    @IBOutlet weak var twentyButton: UIButton!
    @IBOutlet weak var thirtyButton: UIButton!
    @IBOutlet weak var fortyButton: UIButton!
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var twentyBorder: UIView!
    @IBOutlet weak var thirtyBorder: UIView!
    @IBOutlet weak var fortyBorder: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.sizingSetup()
        self.UISetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func twentyButton(_ sender: Any) {
        setLife(toLife: 20)
    }
    @IBAction func thirtyButton(_ sender: Any) {
        setLife(toLife: 30)
    }
    @IBAction func fortyButton(_ sender: Any) {
        setLife(toLife: 40)
    }
    
    func setLife(toLife: Int) {
        self.game.startingLife = toLife
        self.lifeDelegate?.resetLifeTotal()
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        self.delegate?.closeMenu()
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "settings", animate: false)
    }
    
}

// MARK: STYLE SETUP
extension LifeSelectViewController {
    
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Starting Life Total"
        customTopBar.view.frame = self.topBufferView.bounds
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        customNavBar.showBackButton = true
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func sizingSetup() {
        self.twentyButton.set(fontSize: getCenterMenuFontSize())
        self.thirtyButton.set(fontSize: getCenterMenuFontSize())
        self.fortyButton.set(fontSize: getCenterMenuFontSize())
    }
    
    func UISetup() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = self.game.colorSchemes[self.playerNumber-1]
        let colorData = self.colorManager.getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color3 = colorData.color3
        self.color4 = colorData.color4
        
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4

        // Separators
        self.twentyBorder.backgroundColor = self.color3
        self.thirtyBorder.backgroundColor = self.color3
        self.fortyBorder.backgroundColor = self.color3
        
        // Buttons
        styleManager.format(button: self.twentyButton, type: .menuItem, iconImageView: nil)
        styleManager.format(button: self.thirtyButton, type: .menuItem, iconImageView: nil)
        styleManager.format(button: self.fortyButton, type: .menuItem, iconImageView: nil)

        self.twentyButton.addTarget(self, action: #selector(self.unHighlightLifeButton(sender:)), for: .touchUpInside)
        self.twentyButton.addTarget(self, action: #selector(self.unHighlightLifeButton(sender:)), for: .touchUpOutside)
        self.twentyButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)

        self.thirtyButton.addTarget(self, action: #selector(self.unHighlightLifeButton(sender:)), for: .touchUpInside)
        self.thirtyButton.addTarget(self, action: #selector(self.unHighlightLifeButton(sender:)), for: .touchUpOutside)
        self.thirtyButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)

        self.fortyButton.addTarget(self, action: #selector(self.unHighlightLifeButton(sender:)), for: .touchUpInside)
        self.fortyButton.addTarget(self, action: #selector(self.unHighlightLifeButton(sender:)), for: .touchUpOutside)
        self.fortyButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
        
        self.setSelectedButton()
    }
    
    // Pre-selected Life
    func setSelectedButton() {
        switch self.game.startingLife {
        case 30:
            self.thirtyButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        case 40:
            self.fortyButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        default:
            self.twentyButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        }
    }
    
    // Returns a font size based on device class
    func getCenterMenuFontSize() -> CGFloat {
        let device = UIDevice.current
        if device.model == "iPad" {
            if self.game.numberOfPlayers == 2 {
                return 40
            } else {
                return 44
            }
        } else {
            if self.game.numberOfPlayers == 2 {
                return 28
            } else {
                return 34
            }
        }
    }

    // Change color back
    @objc func unHighlightLifeButton(sender: UIButton) {
        self.twentyButton.backgroundColor = primaryColor!
        self.thirtyButton.backgroundColor = primaryColor!
        self.fortyButton.backgroundColor = primaryColor!
        self.setSelectedButton()
    }
    
}







