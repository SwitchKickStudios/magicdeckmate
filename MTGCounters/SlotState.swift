//
//  SlotStates.swift
//  MTGCounters
//
//  Created by William King on 4/30/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

enum SlotState {
    case empty
    case untapped
    case tappedLeft
    case tappedRight
}
