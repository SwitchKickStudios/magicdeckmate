//
//  GameData.swift
//  MTGCounters
//
//  Created by William King on 4/29/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

final class GameData {
    
    static let sharedInstance = GameData()
    
    var startingLife = Int()
    var numberOfPlayers = Int()
    var colorSchemes: [ColorSchemes]
    
    private init() {
        self.startingLife = 20
        self.numberOfPlayers = 1
        self.colorSchemes = [.colorless,.colorless,.colorless,.colorless]
    }
    
    let maxCards1: [Int:Int] = [ // as in 'Max number of players' : 'Max number of cards allowed'
        1:6,
        2:3,
    ]
    let maxCards2: [Int:Int] = [
        1:9,
        2:6,
    ]
    let growAtCards1: [Int:Int] = [ // as in 'Max number of players' : 'Number of cards minimum required to use bigger battlefield'
        1:5,
        2:3,
    ]
    let growAtCards2: [Int:Int] = [
        1:7,
        2:4,
    ]
    let shrinkAtCards1: [Int:Int] = [ // as in 'Max number of players' : 'Number of cards minimum required to decrease the size of the battlefield'
        1:4,
        2:2
    ]
    let shrinkAtCards2: [Int:Int] = [
        1:6,
        2:3
    ]
    
}
