//
//  PlayerSelectViewController.swift
//  MTGCounters
//
//  Created by Conor King on 11/14/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol PlayerSelectDelegate {
    func changeBattlefieldTo(players: Int)
}

class PlayerSelectViewController: MDMViewController, CloseModalDelegate, BackModalDelegate {

    var game = GameData.sharedInstance
    let colorManager = ColorManager()
    
    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var playerDelegate: PlayerSelectDelegate? = nil
    
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var oneBorder: UIView!
    @IBOutlet weak var twoBorder: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.customNavigationSetup()
        self.sizingSetup()
        self.UISetup()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func oneButton(_ sender: Any) {
        self.beginSwitchingBattlefields(players: 1)
        
    }
    @IBAction func twoButton(_ sender: Any) {
        self.beginSwitchingBattlefields(players: 2)
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        self.delegate?.closeMenu()
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "settings", animate: false)
    }
    
    func beginSwitchingBattlefields(players: Int) {
        // Create an alert before calling the delegate method do perform the proper reset
        let alert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertViewController
        alert.player = self.playerNumber
        alert.setAlertText(title: "Change to \(players) players?", message: "This will completely reset the battlefield.", b1: "No", b2: "Yes")
        
        alert.button1Action = { (alertVC) in
            self.setSelectedButton()
        }
        alert.button2Action = { (alertVC) in
            self.game.numberOfPlayers = players
            self.playerDelegate?.changeBattlefieldTo(players: players)
        }
        alert.show()
    }
    
}

// MARK: STYLE SETUP
extension PlayerSelectViewController {
    
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Number of Players"
        customTopBar.view.frame = self.topBufferView.bounds
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        customNavBar.showBackButton = true
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func sizingSetup() {
        self.oneButton.set(fontSize: getCenterMenuFontSize())
        self.twoButton.set(fontSize: getCenterMenuFontSize())
    }
    
    func UISetup() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = self.game.colorSchemes[self.playerNumber-1]
        let colorData = self.colorManager.getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color3 = colorData.color3
        self.color4 = colorData.color4
        
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        
        // Separators
        self.oneBorder.backgroundColor = self.color3
        self.twoBorder.backgroundColor = self.color3

        // Buttons
        self.oneButton.setTitleColor(self.secondaryColor, for: .normal)
        self.oneButton.addTarget(self, action: #selector(self.highlightPlayerButtonColor(sender:)), for: .touchDown)
        
        self.twoButton.setTitleColor(self.secondaryColor, for: .normal)
        self.twoButton.addTarget(self, action: #selector(self.highlightPlayerButtonColor(sender:)), for: .touchDown)
        
        self.setSelectedButton()
    }
    
    // Pre-select players count and disable button for current player count
    func setSelectedButton() {
        self.oneButton.isUserInteractionEnabled = true
        self.twoButton.isUserInteractionEnabled = true
        if game.numberOfPlayers == 1 {
            self.selectOne()
            self.oneButton.isUserInteractionEnabled = false
        } else {
            self.selectTwo()
            self.twoButton.isUserInteractionEnabled = false
        }
    }
    func selectOne() {
        self.oneButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: primaryColor!)
        self.twoButton.backgroundColor = self.primaryColor!
    }
    func selectTwo() {
        self.twoButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: primaryColor!)
        self.oneButton.backgroundColor = self.primaryColor!
    }
    
    // Change color
    @objc func highlightPlayerButtonColor(sender: UIButton) {
        self.oneButton.backgroundColor = self.primaryColor!
        self.twoButton.backgroundColor = self.primaryColor!
        sender.backgroundColor? = self.colorManager.getHighlightColor(for: self.game.colorSchemes[self.playerNumber-1], color: primaryColor!)
    }
    
    // Returns a font size based on device class
    func getCenterMenuFontSize() -> CGFloat {
        let device = UIDevice.current
        if device.model == "iPad" {
            if self.game.numberOfPlayers == 2 {
                return 60
            } else {
                return 72
            }
        } else {
            if self.game.numberOfPlayers == 2 {
                return 54
            } else {
                return 64
            }
        }
    }

}

