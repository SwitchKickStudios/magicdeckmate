//
//  ModalNavigationViewController.swift
//  MTGCounters
//
//  Created by Conor King on 11/11/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol CloseModalDelegate {
    func closeButton()
}
protocol BackModalDelegate {
    func backButton()
}
protocol NextModalDelegate {
    func nextButton()
}

class ModalNavigationViewController: MDMViewController {

    // INPUTS
    var playerNumber = 1
    let game = GameData.sharedInstance
    var showBackButton = true
    var showCloseButton = true
    var showNextButton = false
    var backButtonText = "Back"
    var nextButtonText = "Next"
    var closeButtonText = "Close"
    var backDelegate: BackModalDelegate? = nil
    var nextDelegate: NextModalDelegate? = nil
    var closeDelegate: CloseModalDelegate? = nil
    var showTopBorder: Bool = false
    var backgroundHeightConstant: CGFloat = 52
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var topBorder: UIView!
    @IBOutlet weak var topBorderWidthConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UISetup()
        setBackButton()
        setNextButton()
        setCloseButton()
        setupButtonText()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.backDelegate?.backButton()
    }
    @IBAction func nextButton(_ sender: Any) {
        self.nextDelegate?.nextButton()
    }
    @IBAction func closeButton(_ sender: Any) {
        self.closeDelegate?.closeButton()
    }
    
    func setBackButton() {
        if self.showBackButton == true {
            backButton.isHidden = false
            backButton.isUserInteractionEnabled = true
        } else {
            backButton.isHidden = true
            backButton.isUserInteractionEnabled = false
        }
    }
    
    func setNextButton() {
        if self.showNextButton == true {
            nextButton.isHidden = false
            nextButton.isUserInteractionEnabled = true
        } else {
            nextButton.isHidden = true
            nextButton.isUserInteractionEnabled = false
        }
    }
    
    func setCloseButton() {
        if self.showCloseButton == true {
            closeButton.isHidden = false
            closeButton.isUserInteractionEnabled = true
        } else {
            closeButton.isHidden = true
            closeButton.isUserInteractionEnabled = false
        }
    }
    
    func setupButtonText() {
        self.backButton.setTitle(self.backButtonText, for: .normal)
        self.nextButton.setTitle(self.nextButtonText, for: .normal)
        self.closeButton.setTitle(self.closeButtonText, for: .normal)
    }
    
}

// MARK: STYLE SETUP
extension ModalNavigationViewController {
    func UISetup() {
        // Acquire colors
        self.colorScheme = self.game.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
        
        self.background.backgroundColor = UIColor.clear
        self.topBorder.isHidden = !self.showTopBorder
        styleManager.format(borderView: self.topBorder, widthConstraint: self.topBorderWidthConstraint)
        
        // BUTTONS
        styleManager.format(button: self.closeButton, type: .smallButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.closeButton, type: .smallButton)
        styleManager.format(button: self.backButton, type: .smallButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.backButton, type: .smallButton)
        styleManager.format(button: self.nextButton, type: .smallButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.nextButton, type: .smallButton)

    }

}
