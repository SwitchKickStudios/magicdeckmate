//
//  CountersTutorialViewController.swift
//  MTGCounters
//
//  Created by Conor King on 5/13/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class CountersTutorialViewController: MDMViewController, CloseModalDelegate, BackModalDelegate, NextModalDelegate, UITableViewDelegate, UITableViewDataSource {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var tutorialMode = false // Indicates whether we are displaying for inital opening of the app or not.
    
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let countersTutorialText = [
        "Swipe up over a counter to increase its value."
        ,"Swipe down over a counter to decrease its value."
        ,"Press and hold a counter for 1.5 seconds to remove it from the battlefield."
        ,"Press \"Next\" below to try it out!"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 10, 0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.UISetup()
        self.refreshTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.flashScrollIndicators()
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        if self.tutorialMode {
            self.delegate?.openView(selection: "counterControlsExample", animate: false)
        } else {
            self.delegate?.closeMenu()
        }    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "tokenControlsExample", animate: false)
    }
    
    // NextModalDelegate function
    func nextButton() {
        self.delegate?.openView(selection: "counterControlsExample", animate: false)
    }
    
    
    // TABLEVIEW FUNCTIONS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countersTutorialText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CounterTutorialCell", for: indexPath) as! TutorialTableViewCell
        cell.bullet.text = "•"
        cell.tutorialTextLabel.text = self.countersTutorialText[indexPath.row]
        
        cell.contentView.backgroundColor = self.primaryColor
        cell.bullet.textColor = self.secondaryColor
        cell.tutorialTextLabel.textColor = self.secondaryColor
        
        return cell
    }
    
    // Method to refresh the list of decks
    func refreshTable() {
        DispatchQueue.main.async  {
            self.tableView.reloadData()
        }
    }
    
}

// MARK: STYLE SETUP
extension CountersTutorialViewController {
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Counter Controls"
        customTopBar.view.frame = self.topBufferView.bounds
        customTopBar.shouldShowTipButton = !tutorialMode
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        
        if self.tutorialMode {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = false
            customNavBar.showCloseButton = true
            customNavBar.closeButtonText = "Next"
        } else {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = true
            customNavBar.showCloseButton = true
        }
        
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.nextDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func UISetup() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color4 = colorData.color4
        
        if self.colorScheme == .colorless || self.colorScheme == .green || self.colorScheme == .white {
            self.tableView.indicatorStyle = .black
        } else {
            self.tableView.indicatorStyle = .white
        }
        
        self.background.backgroundColor = primaryColor
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        
    }
    
}
