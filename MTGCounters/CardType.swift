//
//  CardTypes.swift
//  MTGCounters
//
//  Created by William King on 4/30/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

enum CardType {
    case token
    case counter
}
