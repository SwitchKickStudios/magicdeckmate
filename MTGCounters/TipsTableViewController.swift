//
//  TipsTableViewController.swift
//  MTGCounters
//
//  Created by Conor King on 6/28/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class TipsTableViewController: MDMTableViewController {

    let colorManager = ColorManager()
    let iapService = InAppPurchaseService.shared
    
    var origin = "main"
    var delegate: MenuNavigationDelegate? = nil
    var playerNumber = 1
    
    let viewTitle = "Tip the Developer"
    var shouldShowDescriptionCell: Bool = true
    
    let tipsArray: [[String:Any]] = [
        [
            "text":"$0.99",
            "tipCell":true
        ],
        [
            "text":"$4.99",
            "tipCell":true
        ],
        [
            "text":"$14.99",
            "tipCell":true
        ],
        [
            "text":"$99.99",
            "tipCell":true
        ],
        [
            "text":"No one likes paywalls, which is why all of the content of this app is available for free upfront. \n\nFor the righteous ones that like Magic DeckMate enough to do so, any contributions are GREATLY appreciated! All proceeds go directly to the developer. \n\n\nFor more information, please visit:",
            "tipCell":false
        ],
        [
            "text":"",
            "tipCell":false
        ]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.UISetup()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.setNeedsStatusBarAppearanceUpdate()
        
        iapService.fetchAvailableProducts()
        iapService.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController) {
            HeroWrapper.unCoverUp()
            self.delegate?.openView(selection: self.origin, animate: false)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        self.navigationController?.navigationBar.barStyle = self.colorManager.getNavigationBarStyleFor(scheme: self.colorScheme)
        return self.colorManager.getStatusBarColorFor(scheme: self.colorScheme)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tipsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        if self.tipsArray[index]["tipCell"] as! Bool == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TipsCell", for: indexPath) as! TipsTableViewCell
            cell.tipLabel.text = self.tipsArray[index]["text"] as? String
            cell.backgroundColor = self.color5
            cell.contentView.backgroundColor = self.color5
            if index == 0 {
                cell.topBorder.backgroundColor = self.color3
            } else {
                cell.topBorder.backgroundColor = UIColor.clear
            }
            cell.separator.backgroundColor = self.color3
            cell.tipLabel.textColor = self.secondaryColor
            return cell
        } else if self.shouldShowDescriptionCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TipsDescriptionCell", for: indexPath) as! TipsDescriptionTableViewCell
            cell.descriptionLabel.text = self.tipsArray[index]["text"] as? String
            cell.contentView.backgroundColor = self.primaryColor
            cell.backgroundColor = self.primaryColor
            cell.descriptionLabel.textColor = self.secondaryColor
            self.shouldShowDescriptionCell = false
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TipsLinksCell", for: indexPath) as! TipsLinksTableViewCell
            cell.contentView.backgroundColor = self.primaryColor
            cell.backgroundColor = self.primaryColor
            let buttons = Buttons()
            
            let menuCornerRadius: CGFloat = 13
            let menuBorderWidth: CGFloat = 1.5
            
            // WEBSITE BUTTON
            let SKSColor = UIColor(hex: "283f60")
            cell.websiteButton.layer.borderWidth = menuBorderWidth
            cell.websiteButton.layer.borderColor = SKSColor.cgColor
            cell.websiteButton.layer.cornerRadius = menuCornerRadius
            cell.websiteButton.backgroundColor = UIColor.white
            cell.websiteButton.setTitleColor(SKSColor, for: .normal)
            
            // INSTAGRAM BUTTON
            cell.instagramButton.setBackgroundImage(buttons.circleButton(diameter: cell.instagramButton.layer.bounds.width, color: UIColor.white), for: .normal)
            cell.instagramButton.setBackgroundImage(buttons.circleButton(diameter: cell.instagramButton.layer.bounds.width, color: ColorManager().getHighlightColor(for: .colorless, color: UIColor.white)), for: .highlighted)
            cell.instagramButton.backgroundColor = UIColor.white
            cell.instagramButton.layer.cornerRadius = cell.instagramButton.layer.bounds.width/2
            cell.instagramButton.layer.borderColor = UIColor.black.cgColor
            cell.instagramButton.layer.borderWidth = menuBorderWidth
            cell.instagramButton.setImage(#imageLiteral(resourceName: "InstagramIcon"), for: .normal)
            
            // TWITTER BUTTON
            let twitterBlue = UIColor(hex: "4DABE4")
            cell.twitterButton.setBackgroundImage(buttons.circleButton(diameter: cell.twitterButton.layer.bounds.width, color: twitterBlue), for: .normal)
            cell.twitterButton.setBackgroundImage(buttons.circleButton(diameter: cell.twitterButton.layer.bounds.width, color: ColorManager().getHighlightColor(for: .colorless, color: twitterBlue)), for: .highlighted)
            cell.twitterButton.backgroundColor = twitterBlue
            cell.twitterButton.layer.cornerRadius = cell.twitterButton.layer.bounds.width/2
            cell.twitterButton.layer.borderColor = UIColor.white.cgColor
            cell.twitterButton.layer.borderWidth = menuBorderWidth
            cell.twitterButton.setImage(#imageLiteral(resourceName: "TwitterIcon"), for: .normal)

            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        if let cell = tableView.cellForRow(at: indexPath) as? TipsTableViewCell {
            cell.contentView.backgroundColor = colorManager.getHighlightColor(for: GameData.sharedInstance.colorSchemes[self.playerNumber-1], color: primaryColor!)
            
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                cell.contentView.backgroundColor = self.color5
            })
            
            if !ReachabilityTest.isConnectedToNetwork() {
                let alertView = UIAlertController(title: "Network Error", message: "It appears there an issue with your connection.", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    self.iapService.fetchAvailableProducts()
                })
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            }
            
            if iapService.hasPendingTransactions() < 1 {
                print(iapService.hasPendingTransactions())
                iapService.purchaseMyProduct(index: index)
            }
        }

    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    // Method to refresh the list of decks
    func refreshTable() {
        DispatchQueue.main.async  {
            self.tableView.reloadData()
        }
    }

}

// MARK: STYLE SETUP
extension TipsTableViewController {
    
    func UISetup() {
        self.title = self.viewTitle
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = self.colorManager.getAssetsForColor(scheme: colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
        self.color5 = colorData.color5
        
        // Set Colors
        self.formatNavigationBar()
        self.tableView.backgroundColor = primaryColor
        
        if self.colorScheme == .colorless || self.colorScheme == .green || self.colorScheme == .white {
            self.tableView.indicatorStyle = .black
        } else {
            self.tableView.indicatorStyle = .white
        }
    }
}
