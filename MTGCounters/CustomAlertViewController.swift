//
//  CustomAlertViewController.swift
//  MTGCounters
//
//  Created by Conor King on 9/25/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class CustomAlertViewController: MDMViewController, CustomModal {
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    var player = 1
    
    var alertTitle = ""
    var alertMessage = ""
    var button1Title = ""
    var button2Title = ""
    var button1ShouldHide = false
    var button2ShouldHide = false
    
    var button1Action: ((CustomAlertViewController) -> Void)?
    var button2Action: ((CustomAlertViewController) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        
        self.titleLabel.text = self.alertTitle
        self.descriptionLabel.text = self.alertMessage
        self.button1.setTitle(button1Title, for: .normal)
        self.button2.setTitle(button2Title, for: .normal)
        self.button1.isHidden = button1ShouldHide
        self.button2.isHidden = button2ShouldHide
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.orientFrame()
        self.UISetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func button1(_ sender: Any) {
        self.removeAnimate {
            self.button1Action!(self)
            self.dismiss(animated: false, completion: nil)
        }
    }
    @IBAction func button2(_ sender: Any) {
        self.removeAnimate {
            self.button2Action!(self)
            self.dismiss(animated: false, completion: nil)
        }
    }
    override var prefersStatusBarHidden: Bool {
        if GameData.sharedInstance.numberOfPlayers == 2 {
            return true
        } else {
            return false
        }
    }
    
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindowLevelAlert + 1
        win.makeKeyAndVisible()
        self.view.alpha = 0
        vc.present(self, animated: false, completion: {
                self.showAnimate()
        })
    }
    
    func setAlertText(title: String, message: String, b1: String, b2: String) {
        self.alertTitle = title
        self.alertMessage = message
        self.button1Title = b1
        self.button2Title = b2
    }
    
    func orientFrame() {
        if player == 2 && UIApplication.shared.statusBarOrientation.isPortrait {
            self.background.transform = CGAffineTransform(rotationAngle: .pi)
        }

    }

}

// MARK : STYLING

extension CustomAlertViewController {
    func UISetup() {
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[player-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color3 = colorData.color3
        
        self.titleLabel.textColor = self.secondaryColor
        self.descriptionLabel.textColor = self.secondaryColor
        
        self.background.backgroundColor = self.primaryColor
        self.background.layer.borderWidth = styleManager.menuBorderWidth
        self.background.layer.borderColor = self.color3?.cgColor
        self.background.layer.cornerRadius = styleManager.buttonCornerRadius
        
        // Buttons
        styleManager.format(button: self.button1, type: .standardButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.button1, type: .standardButton)
        styleManager.format(button: self.button2, type: .standardButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.button2, type: .standardButton)
        
    }
    
}
