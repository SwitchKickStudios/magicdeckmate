//
//  ModalNavigationTopViewController.swift
//  MTGCounters
//
//  Created by Conor King on 3/30/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class ModalNavigationTopViewController: MDMViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topBarBorder: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var tipButton: UIButton!
    
    @IBOutlet weak var topBarBorderWidthConstraint: NSLayoutConstraint!
    
    var delegate: MenuNavigationDelegate? = nil
    var titleText: String = ""
    var playerNumber = 1
    var shouldShowTipButton = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.UISetup()
        self.titleLabel.text = self.titleText
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if self.playerNumber == 1 && self.shouldShowTipButton {
            self.tipButton.isHidden = false
            self.formatButtonOnAction(button: self.tipButton, type: .circlarMenuButton)
            self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
            let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
            let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
            styleManager.format(button: self.tipButton, type: .circularMenuButtonSmallShadow, iconImageView: nil)
            self.tipButton.setImage(#imageLiteral(resourceName: "HeartIcon"), for: .normal)
            self.tipButton.tintColor = self.secondaryColor
        } else {
            self.tipButton.isHidden = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func tipButton(_ sender: Any) {
        self.delegate?.openView(selection: "tips", animate: false)
    }
    
}

// MARK: STYLE SETUP
extension ModalNavigationTopViewController {
    func UISetup() {
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
        
        self.background.backgroundColor = UIColor.clear
        self.titleLabel.textColor = self.secondaryColor
        styleManager.format(borderView: self.topBarBorder, widthConstraint: self.topBarBorderWidthConstraint)
        
    }
    
}
