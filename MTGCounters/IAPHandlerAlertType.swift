//
//  IAPHandlerAlertType.swift
//  MTGCounters
//
//  Created by Conor King on 7/6/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit
import StoreKit

enum IAPHandlerAlertType {
    case disabled
    case restored
    case purchased
    
    func message() -> String{
        switch self {
        case .disabled: return "Purchases are disabled in your device!"
        case .restored: return "You've successfully restored your purchase!"
        case .purchased: return "Transaction completed successfully. Thank so you such for your contribution!"
        }
    }
}
