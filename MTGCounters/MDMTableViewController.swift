//
//  MDMTableViewController.swift
//  MTGCounters
//
//  Created by Conor King on 3/25/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class MDMTableViewController: UITableViewController {

    // VARIABLES
    var primaryColor: UIColor?
    var secondaryColor: UIColor?
    var color3: UIColor?
    var color4: UIColor?
    var color5: UIColor?
    var colorScheme: ColorSchemes = .colorless

    func formatNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.barTintColor = self.primaryColor
        self.navigationController?.navigationBar.tintColor = self.secondaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor : self.secondaryColor! ]
    }
    
}
