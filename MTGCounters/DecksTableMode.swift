//
//  DecksTableMode.swift
//  MTGCounters
//
//  Created by Conor King on 8/5/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

enum DecksTableMode {
    case none
    case delete
    case edit
}
