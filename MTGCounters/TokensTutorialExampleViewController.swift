//
//  TokensTutorialExampleViewController.swift
//  MTGCounters
//
//  Created by Conor King on 5/13/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class TokensTutorialExampleViewController: MDMViewController, CloseModalDelegate, BackModalDelegate, NextModalDelegate {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var tutorialMode = false // Indicates whether we are displaying for inital opening of the app or not.
    
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var contentBackground: UIView!
    @IBOutlet weak var tokenTopBufferView: UIView!
    @IBOutlet weak var tokenBottomBufferView: UIView!
    @IBOutlet weak var tokenGuideView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var bufferSizeConstraint: NSLayoutConstraint!
    
    var tokensDictionary = [String:[CardData]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tokensDictionary = DataFormatService().getTokenDictionary()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.UISetup()
        self.setupToken()
    }
    
    // This is fired whenever a change in layout is detecte (I.E. orientation change or size change)
    // However, if the token has been killed, we do not want this to trigger an immediate reset
    override func viewWillLayoutSubviews() {
        let currentlyPopulated = tokenGuideView.subviews.count
        if currentlyPopulated > 0 {
            super.viewWillLayoutSubviews()
            self.removeSubviews()
            self.setupToken()
            self.view.layoutIfNeeded()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetButton(_ sender: Any) {
        self.removeSubviews()
        self.view.layoutIfNeeded()
        self.setupToken()
        self.view.layoutIfNeeded()
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        if self.tutorialMode {
            self.delegate?.openView(selection: "counterControls", animate: false)
        } else {
            self.delegate?.closeMenu()
        }
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "tokenControls", animate: false)
    }
    
    // NextModalDelegate function
    func nextButton() {
        self.delegate?.openView(selection: "counterControls", animate: false)
    }
    
    // Set up Example Token
    func setupToken() {
        let tokenVC = storyboard?.instantiateViewController(withIdentifier: "TokenCardVC") as! TokenCardViewController
        var exampleToken: CardData? = nil
        
        switch self.colorScheme {
        case .black:
            exampleToken = self.tokensDictionary["Assassin"]?[1]
        case .blue:
            exampleToken = self.tokensDictionary["Drake"]?[0]
        case .green:
            exampleToken = self.tokensDictionary["Elephant"]?[0]
        case .colorless:
            exampleToken = self.tokensDictionary["Servo"]?[0]
        case .red:
            exampleToken = self.tokensDictionary["Goblin"]?[0]
        case .white:
            exampleToken = self.tokensDictionary["Soldier"]?[2]
        }
        
        tokenVC.card = exampleToken
        tokenVC.labelColor = self.secondaryColor
        tokenVC.orientation = .untapped
        tokenVC.id = 1
        tokenVC.isExample = true

        self.addChildViewController(tokenVC)
        tokenVC.view.frame = self.tokenGuideView.bounds
        self.tokenGuideView.addSubview(tokenVC.view)
        tokenVC.didMove(toParentViewController: self)
    }
    
    // Used to remove the token
    func removeSubviews() {
        for subUIView in tokenGuideView.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
    }
    
    // Set up Buffer Views
    func setupBufferViewHeights() {
        let orientation = UIApplication.shared.statusBarOrientation
        let device = UIDevice.current
        if GameData.sharedInstance.numberOfPlayers == 2 || (orientation.isLandscape && device.userInterfaceIdiom == .phone) {
            self.bufferSizeConstraint.isActive = false
        }
    }

}

// MARK: STYLE SETUP
extension TokensTutorialExampleViewController {
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Token Controls"
        customTopBar.view.frame = self.topBufferView.bounds
        customTopBar.shouldShowTipButton = !tutorialMode
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        
        if self.tutorialMode {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = false
            customNavBar.showCloseButton = true
            customNavBar.closeButtonText = "Next"
        } else {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = true
            customNavBar.showCloseButton = true
        }
        
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.nextDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func UISetup() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color4 = colorData.color4
        
        if self.colorScheme == .blue || self.colorScheme == .black || self.colorScheme == .red {
            self.resetButton.setImage(#imageLiteral(resourceName: "ResetIconWhite"), for: .normal)
        } else {
            self.resetButton.setImage(#imageLiteral(resourceName: "ResetIconBlack"), for: .normal)
        }
        
        self.tokenTopBufferView.backgroundColor = UIColor.clear
        self.tokenBottomBufferView.backgroundColor = UIColor.clear
        self.tokenGuideView.backgroundColor = UIColor.clear
        
        self.background.backgroundColor = primaryColor
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        
        setupBufferViewHeights()
    }
    
}
