//
//  OrientationService.swift
//  MTGCounters
//
//  Created by Conor King on 12/4/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation
import UIKit

class OrientationService {
    
    func convertToDeviceOrientationFrom(interfaceOrientation: UIInterfaceOrientation) -> UIDeviceOrientation {
        switch interfaceOrientation {
        case .landscapeLeft:
            return .landscapeLeft
        case .landscapeRight:
            return .landscapeRight
        default:
            return .portrait
        }
    }

}
