//
//  ColorSchemes.swift
//  MTGCounters
//
//  Created by William King on 5/13/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

enum ColorSchemes {
    case colorless
    case black
    case blue
    case green
    case red
    case white
}
