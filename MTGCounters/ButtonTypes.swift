//
//  ButtonTypes.swift
//  MTGCounters
//
//  Created by Conor King on 3/25/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import Foundation

enum ButtonTypes {
    case standardButton
    case smallButton
    case circlarMenuButton
    case circularMenuButtonSmallShadow
    case menuItem
    case editButton
    case editButton2
    case deleteButton
    case deleteButton2
}
