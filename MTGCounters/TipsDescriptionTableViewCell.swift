//
//  TipsDescriptionTableViewCell.swift
//  MTGCounters
//
//  Created by Conor King on 7/1/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class TipsDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
