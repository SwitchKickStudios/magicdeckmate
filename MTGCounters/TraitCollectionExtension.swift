//
//  TraitCollectionExtension.swift
//  MTGCounters
//
//  Created by William King on 5/17/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation
import UIKit

extension UITraitCollection {
    
    var isIpad: Bool {
        return horizontalSizeClass == .regular && verticalSizeClass == .regular
    }
    
    var isIphoneLandscape: Bool {
        return verticalSizeClass == .compact
    }
    
    var isIphonePortrait: Bool {
        return horizontalSizeClass == .compact && verticalSizeClass == .regular
    }
    
    var isIphone: Bool {
        return isIphoneLandscape || isIphonePortrait
    }
}
