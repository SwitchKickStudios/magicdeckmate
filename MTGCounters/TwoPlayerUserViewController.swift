//
//  TwoPlayerUserViewController.swift
//  MTGCounters
//
//  Created by Conor King on 9/20/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol TwoPlayerUserDelegate {
    func openTo(menu: String, player: Int, originVC: TwoPlayerUserViewController)
    func updatePlayer(data: PlayerData, number: Int)
    func saveMenu(menu: String?, player: Int)
    func setCurrentMenuString(string: String)
}

class TwoPlayerUserViewController: PlayerBattlefieldViewController, MenuNavigationDelegate, TokenDelegate, CounterDelegate, BattlefieldUpdatePlayerDelegate, ResetDelegate, ColorSelectDelegate, LifeSelectDelegate {

    // Inputs
    var delegate: TwoPlayerUserDelegate? = nil
    var colorDelegate: ColorSelectDelegate? = nil
    var playerNumber = 1
    var player = PlayerData(maxPlayers: 2)
    
    let game = GameData.sharedInstance
    let colorManager = ColorManager()
    let buttons = Buttons()
    
    // UI Outlets
    @IBOutlet weak var menuLevelView: UIView!
    @IBOutlet weak var touchOutsideMenuDetectionLevelView: UIView!
    @IBOutlet weak var battlefieldGuideView: UIView!
    @IBOutlet weak var staticBattlefieldGuideView: UIView!
    @IBOutlet weak var battlefieldView: UIView!
    @IBOutlet weak var battlefieldBackground: UIImageView!
    @IBOutlet weak var headBackground: UIImageView!
    @IBOutlet weak var headSectionBorder: UIView!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var lifeBackgroundView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var lifeLabel: UILabel!
    @IBOutlet weak var centerMenuButton: UIButton!
    @IBOutlet weak var centerMenuIcon: UIImageView!
    
    // Parameters
    var areaOutsideMenuAlpha: CGFloat = 0.65
    
    @IBOutlet weak var headSectionBorderWidth: NSLayoutConstraint!
    @IBOutlet weak var minusButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headSectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var battlefieldGuideViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var battlefieldGuideViewBottomConstraint: NSLayoutConstraint!
    
    var menuToOpen: String?
    var battlefieldCollectionVC: BattlefieldCollectionViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.resetColorScheme()
        self.lifeLabel.text = String(self.player.life)
        self.openMenuIfTransition()

    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.adjustBattlefieldVerticalConstraints()
        self.adjustHeadPadding()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.adjustBattlefieldVerticalConstraints()
        self.adjustHeadPadding()
    }
    
    // Wait for the view to finish changing orientation
    // Then adjust the layout constraints
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
        }, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func plusButton(_ sender: Any) {
        player.life += 1
        lifeLabel.text = String(player.life)
        self.delegate?.updatePlayer(data: player, number: playerNumber)

    }
    @IBAction func minusButton(_ sender: Any) {
        player.life -= 1
        lifeLabel.text = String(player.life)
        self.delegate?.updatePlayer(data: player, number: playerNumber)

    }
    @IBAction func centerMenuButton(_ sender: Any) {
        openView(selection: "main", animate: true)
    }

}

// MARK: BATTLEFIELD CONFIGURATION
extension TwoPlayerUserViewController {
    
    // Delegate method to reset life from Life Selection
    func resetLifeTotal() {
        self.player.life = self.game.startingLife
        self.lifeLabel.text = String(player.life)
        self.delegate?.updatePlayer(data: player, number: playerNumber)
    }

}

// MARK: BATTLEFIELD ACTIONS
extension TwoPlayerUserViewController {
    
    // UPDATE SLOT INFO
    // TokenDelegate FUNCTION
    func updateToken(id: Int, orientation: SlotState, quantity: Int) {
        let index = self.player.cardIDs.index(of: id)!
        self.player.cards[index].quantity = quantity
        self.player.battledfieldState[index] = orientation
        if orientation == .empty {
            self.player.cardIDs[index] = 0
        }
        self.delegate?.updatePlayer(data: player, number: playerNumber)
        self.battlefieldCollectionVC?.player = player
    }
    // CounterDelegate FUNCTION
    func updateCounter(id: Int, orientation: SlotState, counterValue: Int) {
        let index = self.player.cardIDs.index(of: id)!
        self.player.cards[index].quantity = counterValue
        self.player.battledfieldState[index] = orientation
        if orientation == .empty {
            self.player.cardIDs[index] = 0
        }
        self.delegate?.updatePlayer(data: player, number: playerNumber)
        self.battlefieldCollectionVC?.player = player
    }
    // CounterDelegate function
    func getPlayerData() -> PlayerData {
        return self.player
    }
    
    // BattlefieldUpdatePlayerDelegate FUNCTION
    // Update self.player and TwoPlayerBattlefieldVC.player
    func updatePlayer(data: PlayerData) {
        self.player = data
        self.adjustBattlefieldVerticalConstraints()
        self.delegate?.updatePlayer(data: player, number: playerNumber)
    }
    
    // RESET BATTLEFIELD - ResetDelegate Method
    // Switch method that resets the battlefield according to ResetType
    func resetBattlefield(playerNumber: Int, type: ResetType) {
        switch type {
            
        // RESET LIFE
        case .resetLife:
            
            // RESET LIFE: set player life to GameData singleton starting life
            self.player.life = self.game.startingLife
            self.lifeLabel.text = String(player.life)
            
        // SET CARD VALUES TO ZERO
        case .setCardsToZero:
            
            // SET CARDS TO ZERO: Loop over the each cards in player1 cards, settings the quantity to 0 and the respective battlefield status to untapped
            for slot in 1 ... self.player.cards.count {
                if self.player.battledfieldState[slot-1] != .empty {
                    self.player.cards[slot-1].quantity = 0
                    self.player.battledfieldState[slot-1] = .untapped
                }
            }
            self.initializeBattlefieldCollectionView()
            
        // CLEAR CARDS
        case .resetCards:
            
            // CLEAR CARDS: Loop over player cards, replacing with empty CardData
            // Loop over players battlefield state, replacing with .empty
            self.player.removeAllCards()
            self.initializeBattlefieldCollectionView()
            
        // RESET ALL
        case .resetAll:
            
            // RESET ALL: set player life to GameData singleton starting life
            // Loop over player cards, replacing with empty CardData
            self.player.life = self.game.startingLife
            self.lifeLabel.text = String(player.life)
            self.player.removeAllCards()
            self.initializeBattlefieldCollectionView()
            
        }
        self.delegate?.updatePlayer(data: player, number: playerNumber)
    }
    
}

// MARK: SUBVIEWS
extension TwoPlayerUserViewController {
    
    // MenuNavigationDelegate function - Handles the navigation from this battlefield
    func openView(selection: String, animate: Bool) {
        self.delegate?.saveMenu(menu: selection, player: self.playerNumber)
        self.menuLevelView.isUserInteractionEnabled = true

        switch selection {
            
        case "main":
            removeLastSubView(from: self.menuLevelView)
            let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "CenterMenuVC2") as! CenterMenuViewController
            menuVC.delegate = self
            menuVC.playerNumber = self.playerNumber
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: menuVC, animate: animate)
            
        case "tokens":
            self.pushViewControllerForMenu(menuName: "tokens")

        case "counters":
            self.pushViewControllerForMenu(menuName: "counters")

        case "decks":
            self.pushViewControllerForMenu(menuName: "decks")
            
        case "tips":
            self.pushViewControllerForMenu(menuName: "tips")
            
        case "dice":
            removeLastSubView(from: self.menuLevelView)
            let diceVC = storyboard?.instantiateViewController(withIdentifier: "DiceVC") as! DiceViewController
            diceVC.playerNumber = self.playerNumber
            diceVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: diceVC, animate: animate)
            
        case "resets":
            removeLastSubView(from: self.menuLevelView)
            let resetVC = storyboard?.instantiateViewController(withIdentifier: "ResetVC") as! ResetViewController
            resetVC.resetDelegate = self
            resetVC.delegate = self
            resetVC.playerNumber = self.playerNumber
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: resetVC, animate: animate)
            
        case "settings":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let settingsVC = storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsViewController
            settingsVC.playerNumber = self.playerNumber
            settingsVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: settingsVC, animate: animate)
            
        case "players":
            removeLastSubView(from: self.menuLevelView)
            self.menuLevelView.isUserInteractionEnabled = true
            let playerSelectVC = storyboard?.instantiateViewController(withIdentifier: "PlayerSelectVC") as! PlayerSelectViewController
            playerSelectVC.playerDelegate = self.parent as? PlayerSelectDelegate
            playerSelectVC.delegate = self
            playerSelectVC.playerNumber = self.playerNumber
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: playerSelectVC, animate: animate)
            
        case "color":
            removeLastSubView(from: self.menuLevelView)
            let colorVC = storyboard?.instantiateViewController(withIdentifier: "ColorSelectVC2") as! ColorSelectViewController
            colorVC.delegate = self
            colorVC.colorDelegate = self
            colorVC.playerNumber = self.playerNumber
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: colorVC, animate: animate)
            
        case "startingLife":
            removeLastSubView(from: self.menuLevelView)
            let lifeVC = storyboard?.instantiateViewController(withIdentifier: "LifeSelectVC") as! LifeSelectViewController
            lifeVC.delegate = self
            lifeVC.lifeDelegate = self
            lifeVC.playerNumber = self.playerNumber
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: lifeVC, animate: animate)
            
        case "support":
            removeLastSubView(from: self.menuLevelView)
            let supportVC = storyboard?.instantiateViewController(withIdentifier: "SupportTutorialVC") as! SupportTutorialViewController
            supportVC.playerNumber = self.playerNumber
            supportVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: supportVC, animate: animate)
        
        case "tokenControls":
            removeLastSubView(from: self.menuLevelView)
            let tokenTutorialVC = storyboard?.instantiateViewController(withIdentifier: "TokensTutorialVC") as! TokensTutorialViewController
            tokenTutorialVC.playerNumber = self.playerNumber
            tokenTutorialVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: tokenTutorialVC, animate: animate)
            
        case "tokenControlsExample":
            removeLastSubView(from: self.menuLevelView)
            let tokenTutorialExampleVC = storyboard?.instantiateViewController(withIdentifier: "TokensTutorialExampleVC") as! TokensTutorialExampleViewController
            tokenTutorialExampleVC.playerNumber = self.playerNumber
            tokenTutorialExampleVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: tokenTutorialExampleVC, animate: animate)
            
        case "counterControls":
            removeLastSubView(from: self.menuLevelView)
            let countersTutorialVC = storyboard?.instantiateViewController(withIdentifier: "CountersTutorialVC") as! CountersTutorialViewController
            countersTutorialVC.playerNumber = self.playerNumber
            countersTutorialVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: countersTutorialVC, animate: animate)
            
        case "counterControlsExample":
            removeLastSubView(from: self.menuLevelView)
            let countersTutorialExampleVC = storyboard?.instantiateViewController(withIdentifier: "CountersTutorialExampleVC") as! CountersTutorialExampleViewController
            countersTutorialExampleVC.playerNumber = self.playerNumber
            countersTutorialExampleVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: countersTutorialExampleVC, animate: animate)
            
        case "menuNavigationControls":
            removeLastSubView(from: self.menuLevelView)
            let menuNavigationControlsVC = storyboard?.instantiateViewController(withIdentifier: "MenuNavigationTutorialVC") as! MenuNavigationTutorialViewController
            menuNavigationControlsVC.playerNumber = self.playerNumber
            menuNavigationControlsVC.delegate = self
            addViewControllerAsSubView(guideView: self.menuLevelView, newViewController: menuNavigationControlsVC, animate: animate)
            
        default:
            print("MENU ERROR - selection not recognized")
        }
        
        if self.playerNumber == 1 {
            self.delegate?.setCurrentMenuString(string: selection)
            self.menuToOpen = selection
        }
    }
    
    // MenuNavigationDelegate function - Turns off the guideViews associated with mnu navigation
    // Used in the case where a subview is closed from an elements not in this ViewController
    func turnOffGuideViews() {
        self.touchOutsideMenuDetectionLevelView.isUserInteractionEnabled = false
        self.menuLevelView.isUserInteractionEnabled = false
    }
    
    // MenuNavigationDelegate function - Close the menu
    // Menu Closing Animation
    func closeMenu() {
        let snap = self.menuLevelView.snapshotView(afterScreenUpdates: false)
        snap?.center = self.menuLevelView.center
        snap?.isUserInteractionEnabled = false
        self.view.addSubview(snap!)
        self.touchOutsideMenuDetectionLevelView.isUserInteractionEnabled = false
        self.removeLastSubView(from: self.menuLevelView)
        let animator = UIViewPropertyAnimator(duration: 0.4, curve: .linear)
        animator.addAnimations {
            snap?.alpha = 0.0
            self.touchOutsideMenuDetectionLevelView.alpha = 0.0
        }
        animator.startAnimation()
        animator.addCompletion({ (position) in
            if position == .end {
                snap?.removeFromSuperview()
                self.menuLevelView.isUserInteractionEnabled = false
                self.touchOutsideMenuDetectionLevelView.isHidden = true
                self.delegate?.saveMenu(menu: nil, player: self.playerNumber)
            }
        })
    }
    
    // Turn the touch outside menu zone on
    // Add viewcontroller to given zone aka guideView
    func addViewControllerAsSubView(guideView: UIView, newViewController: UIViewController, animate: Bool) {
        self.touchOutsideMenuDetectionLevelView.isUserInteractionEnabled = true
        
        // Set up local style manager
        let colorScheme = self.game.colorSchemes[self.playerNumber-1]
        let colorData = self.colorManager.getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: colorScheme)
        styleManager.format(view: newViewController.view)
        newViewController.view.frame = guideView.bounds
        newViewController.view.clipsToBounds = true
        self.addChildViewController(newViewController)
        guideView.addSubview(newViewController.view)
        newViewController.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        if animate == true {
            let animator = UIViewPropertyAnimator(duration: 0.4, curve: .linear)
            let animator2 = UIViewPropertyAnimator(duration: 0.2, curve: .linear)
            self.touchOutsideMenuDetectionLevelView.alpha = 0.0
            self.touchOutsideMenuDetectionLevelView.isHidden = false
            guideView.alpha = 0.0
            animator.addAnimations {
                self.touchOutsideMenuDetectionLevelView.alpha = self.areaOutsideMenuAlpha
            }
            animator2.addAnimations {
                guideView.alpha = 1.0
            }
            animator.startAnimation()
            animator2.startAnimation()
        } else {
            self.touchOutsideMenuDetectionLevelView.alpha = self.areaOutsideMenuAlpha
            self.touchOutsideMenuDetectionLevelView.isHidden = false
        }
    }
    
    // Prepare and push a viewController for the menu navigation
    func pushViewControllerForMenu(menuName: String) {
        self.touchOutsideMenuDetectionLevelView.isUserInteractionEnabled = false
        self.delegate?.openTo(menu: menuName, player: self.playerNumber, originVC: self)
    }
    
    
    // Used to open a menu when the user gets reloaded in transition
    func openMenuIfTransition() {
        if self.menuToOpen == "tokens" || self.menuToOpen == "counters" || self.menuToOpen == "decks" {
            self.menuToOpen = "main"
        }
        if self.menuToOpen != nil {
            self.removeLastSubView(from: self.menuLevelView)
            self.openView(selection: self.menuToOpen!, animate: false)
        }
    }
    
    // Create the battlefield as collection view and instantiate it in the guideView
    func initializeBattlefieldCollectionView() {
        self.removeLastSubView(from: self.battlefieldGuideView)
        let collectionView = storyboard?.instantiateViewController(withIdentifier: "BattlefieldCollectionVC") as! BattlefieldCollectionViewController
        player.sortCardsForBattlefield()
        collectionView.player = self.player
        collectionView.tokenDelegate = self
        collectionView.counterDelegate = self
        collectionView.updateDelegate = self
        collectionView.playerNumber = self.playerNumber
        self.battlefieldCollectionVC = collectionView
        self.addChildViewController(collectionView)
        collectionView.view.frame = battlefieldGuideView.bounds
        battlefieldGuideView.addSubview(collectionView.view)
        collectionView.collectionView?.backgroundColor = UIColor.clear
        collectionView.didMove(toParentViewController: self)
    }
    
    func removeLastSubView(from: UIView) {
        if from.subviews.last != nil {
            from.subviews.last?.removeFromSuperview()
        }
    }

}

// MARK: STYLING
extension TwoPlayerUserViewController {
    
    // Modify the UI on load
    func configureUI() {
        self.touchOutsideMenuDetectionLevelView.isHidden = true
    }
    
    // Changes the color scheme to global variable for this player's color
    func resetColorScheme() {
        // Acquire colors
        self.colorScheme = game.colorSchemes[playerNumber-1]
        let colorData = self.colorManager.getAssetsForColor(scheme: self.colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)

        if playerNumber == 2 {
            self.colorManager.setStatusBarColorFor(scheme: self.colorScheme)
        }
        secondaryColor = colorData.secondaryColor!
        primaryColor = colorData.primaryColor
        styleManager.format(borderView: self.headSectionBorder, widthConstraint: self.headSectionBorderWidth)
        
        // UI Elements
        self.battlefieldBackground.image = UIImage(named: colorData.backgroundLarge!)
        self.lifeLabel.textColor = self.secondaryColor
        
        self.touchOutsideMenuDetectionLevelView.backgroundColor = UIColor.black
        self.touchOutsideMenuDetectionLevelView.alpha = self.areaOutsideMenuAlpha
        
        styleManager.format(view: self.lifeBackgroundView)
        styleManager.format(imageView: self.headBackground)
        
        // Buttons
        styleManager.format(button: self.plusButton, type: .standardButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.plusButton, type: .standardButton)
        styleManager.format(button: self.minusButton, type: .standardButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.minusButton, type: .standardButton)
        styleManager.format(button: self.centerMenuButton, type: .circlarMenuButton, iconImageView: self.centerMenuIcon)
        self.formatButtonOnAction(button: self.centerMenuButton, type: .circlarMenuButton)
        
        // Move stuff around
        self.adjustHeadPadding()
        
        // Remove and reload cards with proper colors
        self.initializeBattlefieldCollectionView()
        
        // Reset the background colors of the parent view
        self.colorDelegate?.resetColorScheme()
    }
    
    // Adjust the constraint that control distance from head area to top of view
    func adjustHeadPadding() {
        let device = UIDevice.current
        self.minusButtonTopConstraint.constant = 8
        if device.userInterfaceIdiom == .phone {
            self.headSectionHeightConstraint.constant = 100
        } else {
            self.headSectionHeightConstraint.constant = 120
        }
        self.view.layoutIfNeeded()
    }
    
    // Adjust constraints to make sure cards and their labels are in proportion
    func adjustBattlefieldVerticalConstraints() {
        let orientation = UIApplication.shared.statusBarOrientation
        let device = UIDevice.current
        let currentBattlefieldHeight = self.staticBattlefieldGuideView.layer.bounds.height
        
        // iPad Landscape
        if orientation.isLandscape && device.userInterfaceIdiom == .pad {
            if self.player.getCardCount() >= self.game.growAtCards2[2]! {
                self.battlefieldGuideViewTopConstraint.constant = currentBattlefieldHeight * 0.08
                self.battlefieldGuideViewBottomConstraint.constant = currentBattlefieldHeight * 0.08
            } else if self.player.getCardCount() >= self.game.growAtCards1[2]! {
                self.battlefieldGuideViewTopConstraint.constant = currentBattlefieldHeight * 0.29
                self.battlefieldGuideViewBottomConstraint.constant = currentBattlefieldHeight * 0.29
            } else {
                self.battlefieldGuideViewTopConstraint.constant = currentBattlefieldHeight * 0.2
                self.battlefieldGuideViewBottomConstraint.constant = currentBattlefieldHeight * 0.2
            }
        // iPad Portrait
        } else if device.userInterfaceIdiom == .pad {
            self.battlefieldGuideViewTopConstraint.constant = 8
            self.battlefieldGuideViewBottomConstraint.constant = 8
        // iPhone Portrait
        } else if orientation.isLandscape && device.userInterfaceIdiom == .phone {
            if self.player.getCardCount() >= self.game.growAtCards1[2]! && self.player.getCardCount() < self.game.growAtCards2[2]! {
                self.battlefieldGuideViewTopConstraint.constant = currentBattlefieldHeight * 0.18
                self.battlefieldGuideViewBottomConstraint.constant = currentBattlefieldHeight * 0.18
            } else {
                self.battlefieldGuideViewTopConstraint.constant = 8
                self.battlefieldGuideViewBottomConstraint.constant = 8
            }
        }
        self.view.layoutIfNeeded()
    }
    
}






