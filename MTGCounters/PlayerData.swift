//
//  PlayerData.swift
//  MTGCounters
//
//  Created by William King on 4/29/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

struct PlayerData {
    
    var cards = Array<CardData>()
    var life = Int()
    var battledfieldState = Array<SlotState>()
    var cardIDs = Array<Int>() // Used to determine which cards to update from collection view. Values of 0 indicate an unpopulated slot
    
    init(maxPlayers: Int) {
        
        // Set starting life of player
        self.life = GameData.sharedInstance.startingLife
        
        // Populate state of the battlefield as with the max number of cards based on maxPlayers
        for _ in 0...(GameData.sharedInstance.maxCards2[maxPlayers]! - 1) {
            battledfieldState.append(.empty)
            cards.append(CardData(description: "", name: "", type: .token, category: "", color: ["colorless"], stats: ""))
            cardIDs.append(0)
        }
    }
    
    // Find an empty slot in battlefieldstate array, then populate corresponding slot in cards array
    // and mark corresponding battlefieldstate as untapped
    // If successful return true, else false
    mutating func findEmptySlot(for newCard: CardData) -> Bool {
        var slotIndex = 0
        for slotStatus in battledfieldState {
            if slotStatus == .empty {
                cards[slotIndex] = newCard
                battledfieldState[slotIndex] = .untapped
                self.assignCardID(slotIndex: slotIndex)
                return true
            }
            slotIndex += 1
        }
        return false
    }

    // Find an available cardID
    // Build an array of IDs still available, then assign it to the cardID index of the given slot index
    mutating func assignCardID(slotIndex: Int) {
        let game = GameData.sharedInstance
        
        var totalIDs = Array<Int>()
        for ID in 1...game.maxCards2[game.numberOfPlayers]! {
            totalIDs.append(ID)
        }
        var availableIDs = Set(totalIDs).subtracting(self.cardIDs).sorted()
        if !availableIDs.isEmpty {
            cardIDs[slotIndex] = availableIDs[0]
        }
    }
    
    // Sort the cards, moving excess empty cards to the highest indexes of the array
    // Used to help scale battlefields properly
    mutating func sortCardsForBattlefield() {
        
        // Get a count of not empty cards
        var cardCount = 0
        for state in battledfieldState {
            if state != .empty {
                cardCount += 1
            }
        }
        
        // If card count is at a point where cards should rearrange, sort empty cards to the highest indexes
        let game = GameData.sharedInstance
        let cardCountToTransition1 = game.shrinkAtCards1[game.numberOfPlayers]
        let cardCountToTransition2 = game.shrinkAtCards2[game.numberOfPlayers]
        
        if cardCount == cardCountToTransition1 ||
            cardCount == cardCountToTransition2 {
            var index = 0
            var emptyCount = 0
            for state in battledfieldState {
                if state == .empty {
                    battledfieldState.remove(at: index)
                    cards.remove(at: index)
                    cardIDs.remove(at: index)
                    emptyCount += 1
                } else {
                    index += 1
                }
            }
            if emptyCount >= 1 {
                for _ in 0...(emptyCount-1) {
                    battledfieldState.append(.empty)
                    cards.append(CardData(description: "", name: "", type: .token, category: "", color: ["colorless"], stats: ""))
                    cardIDs.append(0)
                }
            }
        }
        
    }
    
    // Loop over the cards and set them to blank cards and empty battlefield states
    mutating func removeAllCards() {
        for slot in 1 ... self.cards.count {
            cards[slot-1] = CardData(description: "", name: "", type: .token, category: "", color: ["colorless"], stats: "")
            battledfieldState[slot-1] = .empty
            cardIDs[slot-1] = 0
        }
    }

    // Utility function to count number slots being used
    func getCardCount() -> Int {
        var populatedSlots = 0
        for state in battledfieldState {
            if state != .empty {
                populatedSlots += 1
            }
        }
        return populatedSlots
    }
}
