//
//  SupportTutorialViewController.swift
//  MTGCounters
//
//  Created by Conor King on 5/13/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class SupportTutorialViewController: MDMViewController, CloseModalDelegate, BackModalDelegate, NextModalDelegate {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var tutorialMode = false // Indicates whether we are displaying for inital opening of the app or not.
    
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var introAndVersionLabel: UILabel!
    @IBOutlet weak var tutorialTextLabel: UILabel!
    @IBOutlet weak var websiteLinkButton: UIButton!
    @IBOutlet weak var instagramButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")!
        let versionString = String(describing: versionNumber)
        
        introAndVersionLabel.text = "Thank you for using\nMagic DeckMate Version \(versionString) \n\nFor information and support, please visit:"
        websiteLinkButton.setTitle("magicdeckmate.com", for: .normal)
        tutorialTextLabel.text = "\nTo see a detailed description of the controls, hit the \"Tutorial\" button below.\n"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.UISetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Function for website link button and social media buttons
    @IBAction func websiteLinkButton(_ sender: Any) {
        if let url = URL(string: "http://magicdeckmate.com") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func instagramButton(_ sender: Any) {
        if let url = URL(string: "https://www.instagram.com/switchkickstudios/") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func twitterButton(_ sender: Any) {
        if let url = URL(string: "https://twitter.com/SwitchKickApps") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        self.delegate?.closeMenu()
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "settings", animate: false)
    }
    
    // NextModalDelegate function
    func nextButton() {
        self.delegate?.openView(selection: "tokenControls", animate: false)
    }

}

// MARK: STYLE SETUP
extension SupportTutorialViewController {
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Help and Support"
        customTopBar.view.frame = self.topBufferView.bounds
        customTopBar.shouldShowTipButton = !tutorialMode
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        
        if self.tutorialMode {
            customNavBar.showBackButton = false
            customNavBar.showNextButton = false
        } else {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = true
        }
        
        customNavBar.showCloseButton = true
        customNavBar.nextButtonText = "Tutorial"
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.nextDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func UISetup() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color4 = colorData.color4
        
        let buttons = Buttons()
        
        // WEBSITE BUTTON
        let SKSColor = UIColor(hex: "283f60")
        let menuCornerRadius: CGFloat = 13
        let menuBorderWidth: CGFloat = 1.5
        self.websiteLinkButton.layer.borderWidth = menuBorderWidth
        self.websiteLinkButton.layer.borderColor = SKSColor.cgColor
        self.websiteLinkButton.layer.cornerRadius = menuCornerRadius
        self.websiteLinkButton.backgroundColor = UIColor.white
        self.websiteLinkButton.setTitleColor(SKSColor, for: .normal)
        
        // INSTAGRAM BUTTON
        self.instagramButton.setBackgroundImage(buttons.circleButton(diameter: self.instagramButton.layer.bounds.width, color: UIColor.white), for: .normal)
        self.instagramButton.setBackgroundImage(buttons.circleButton(diameter: self.instagramButton.layer.bounds.width, color: ColorManager().getHighlightColor(for: .colorless, color: UIColor.white)), for: .highlighted)
        self.instagramButton.backgroundColor = UIColor.white
        self.instagramButton.layer.cornerRadius = self.instagramButton.layer.bounds.width/2
        self.instagramButton.layer.borderColor = UIColor.black.cgColor
        self.instagramButton.layer.borderWidth = menuBorderWidth
        self.instagramButton.setImage(#imageLiteral(resourceName: "InstagramIcon"), for: .normal)
        
        // TWITTER BUTTON
        let twitterBlue = UIColor(hex: "4DABE4")
        self.twitterButton.setBackgroundImage(buttons.circleButton(diameter: self.twitterButton.layer.bounds.width, color: twitterBlue), for: .normal)
        self.twitterButton.setBackgroundImage(buttons.circleButton(diameter: self.twitterButton.layer.bounds.width, color: ColorManager().getHighlightColor(for: .colorless, color: twitterBlue)), for: .highlighted)
        self.twitterButton.backgroundColor = twitterBlue
        self.twitterButton.layer.cornerRadius = self.twitterButton.layer.bounds.width/2
        self.twitterButton.layer.borderColor = UIColor.white.cgColor
        self.twitterButton.layer.borderWidth = menuBorderWidth
        self.twitterButton.setImage(#imageLiteral(resourceName: "TwitterIcon"), for: .normal)
        
        if self.colorScheme == .colorless || self.colorScheme == .green || self.colorScheme == .white {
            self.scrollView.indicatorStyle = .black
        } else {
            self.scrollView.indicatorStyle = .white
        }
        
        self.background.backgroundColor = primaryColor
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        self.introAndVersionLabel.textColor = secondaryColor
        self.tutorialTextLabel.textColor = secondaryColor
        
    }
    
}
