//
//  MenuNavigationTutorialViewController.swift
//  MTGCounters
//
//  Created by Conor King on 5/13/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class MenuNavigationTutorialViewController: MDMViewController, CloseModalDelegate, BackModalDelegate, UITableViewDelegate, UITableViewDataSource {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var tutorialMode = false // Indicates whether we are displaying for inital opening of the app or not.
    
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let menuTutorialText = [
        "This is the menu button. The icon will change depending on what color scheme you set for the battlefield."
        ,"Menu Items"
        ,"Tokens: Deploy tokens on the battlefield."
        ,"Counters: Deploy counters on the battlefield."
        ,"Decks: Deploy and organize user-defined collections of tokens and counters."
        ,"Dice: Simulate various dice rolls and coin flips."
        ,"Battlefield Resets: Shortcuts to reset different aspects of the battlefield."
        ,"Settings: Set up number of players, color schemes, and starting life."
        ,"\nTo see this tutorial again or for help/support, go to Menu > Settings > Controls | Help.\n"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 10, 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.UISetup()
        self.refreshTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.flashScrollIndicators()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        if self.tutorialMode {
            self.delegate?.openView(selection: "support", animate: false)
        } else {
            self.delegate?.closeMenu()
        }
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "counterControlsExample", animate: false)
    }
    
    // TABLEVIEW FUNCTIONS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuTutorialText.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            
            // First row - Image of menu with arrow
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuExampleTutorialCell", for: indexPath) as! TutorialImageTableViewCell
            cell.contentView.backgroundColor = self.primaryColor
            return cell
            
        } else if indexPath.row == 2 {
            
            // Third row - Section title
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuHelpSectionTutorialCell", for: indexPath) as! TutorialSectionTableViewCell
            let textToUse = self.menuTutorialText[indexPath.row-1]
            cell.sectionLabel.text = textToUse
            
            cell.contentView.backgroundColor = self.primaryColor
            cell.sectionLabel.textColor = self.secondaryColor
            
            return cell

        } else {
            
            // Second and subsequent rows except for sections - Text cells
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuHelpTutorialCell", for: indexPath) as! TutorialTableViewCell
            
            let textToUse = self.menuTutorialText[indexPath.row-1]
            let nonBoldStart = getCharactersUpToDivider(string: textToUse)
            let nonBoldRange = NSMakeRange(nonBoldStart, textToUse.count-nonBoldStart)
            cell.tutorialTextLabel.attributedText = self.attributedString(from: textToUse, nonBoldRange: nonBoldRange, cell: cell)
            
            if indexPath.row == 1 || indexPath.row == 9 {
                cell.bullet.text = " "
            } else {
                cell.bullet.text = "•"
            }

            cell.contentView.backgroundColor = self.primaryColor
            cell.bullet.textColor = self.secondaryColor
            cell.tutorialTextLabel.textColor = self.secondaryColor
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return getImageSize()
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    // Set image size for tutorial
    func getImageSize() -> CGFloat {
        let device = UIDevice.current
        var size: CGFloat = 200
        if device.userInterfaceIdiom == .pad {
            size = 310
        }
        return size
    }
    
    // Make font bold or nonbold based on a range
    func attributedString(from string: String, nonBoldRange: NSRange?, cell: TutorialTableViewCell) -> NSAttributedString {
        let fontSize = cell.tutorialTextLabel.font.pointSize

        let attrs = [
            kCTFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize),
            kCTForegroundColorAttributeName: UIColor.black
        ]
        let nonBoldAttribute = [
            kCTFontAttributeName: UIFont.systemFont(ofSize: fontSize),
        ]
        
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs as [NSAttributedStringKey : Any])
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute as [NSAttributedStringKey : Any], range: range)
        }
        return attrStr
    }
    
    // If we have a dividing character, get character count up to and including the dividing character
    func getCharactersUpToDivider(string: String) -> Int {
        if string.contains(":") {
            var textSections = string.components(separatedBy: ":")
            return textSections[0].count + 1
        } else {
            return 0
        }
    }
    
    // Method to refresh the list of decks
    func refreshTable() {
        DispatchQueue.main.async  {
            self.tableView.reloadData()
        }
    }
    
}

// MARK: STYLE SETUP
extension MenuNavigationTutorialViewController {
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "The Menu"
        customTopBar.view.frame = self.topBufferView.bounds
        customTopBar.shouldShowTipButton = !tutorialMode
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController

        if self.tutorialMode {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = false
            customNavBar.showCloseButton = true
            customNavBar.closeButtonText = "Next"
        } else {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = false
            customNavBar.showCloseButton = true
        }
        
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func UISetup() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color4 = colorData.color4
        
        if self.colorScheme == .colorless || self.colorScheme == .green || self.colorScheme == .white {
            self.tableView.indicatorStyle = .black
        } else {
            self.tableView.indicatorStyle = .white
        }
        
        self.background.backgroundColor = primaryColor
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        
    }
    
}
