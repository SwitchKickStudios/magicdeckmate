//
//  TipsLinksTableViewCell.swift
//  MTGCounters
//
//  Created by Conor King on 7/11/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class TipsLinksTableViewCell: UITableViewCell {

    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var instagramButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func websiteButton(_ sender: Any) {
        if let url = URL(string: "http://magicdeckmate.com") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func instagramButton(_ sender: Any) {
        if let url = URL(string: "https://www.instagram.com/switchkickstudios/") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func twitterButton(_ sender: Any) {
        if let url = URL(string: "https://twitter.com/SwitchKickApps") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
