//
//  HeroWrapper.swift
//  MTGCounters
//
//  Created by William King on 4/30/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit
import Hero

// This class contains custom transitions used in this app, provided by the Hero framework
class HeroWrapper: NSObject {
    
    class func pushRight() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.push(direction: .right))
    }
    
    // Standard Push
    class func pushLeft() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.push(direction: .left))
    }
    
    class func coverDown() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.cover(direction: .down))
    }
    
    class func coverUp() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.cover(direction: .up))
    }
    
    class func unCoverDown() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.uncover(direction: .down))
    }
    
    class func unCoverUp() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.uncover(direction: .up))
    }
    
    class func popLeft() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.pull(direction: .left))
    }
    
    class func popUp() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.pull(direction: .up))
    }
    
    class func popDown() {
        Hero.shared.setDefaultAnimationForNextTransition(HeroDefaultAnimationType.pull(direction: .down))
    }
    
    class func enableHero(_ vc: UIViewController) {
        vc.isHeroEnabled = true
    }
}
