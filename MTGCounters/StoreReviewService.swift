//
//  StoreReviewService.swift
//  MTGCounters
//
//  Created by Conor King on 6/9/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import Foundation
import StoreKit

class StoreReviewHelper {
    
    let defaults = UserDefaults.standard
    let promptCounterKey = "App_Opened_Count"
    
    func incrementAppOpenedCount() { // called from appdelegate didfinishLaunchingWithOptions:
        guard var appOpenCount = defaults.value(forKey: promptCounterKey) as? Int else {
            defaults.set(1, forKey: promptCounterKey)
            return
        }
        appOpenCount += 1
        defaults.set(appOpenCount, forKey: promptCounterKey)
    }
    
    func checkAndAskForReview() { // call this whenever appropriate
        // this will not be shown everytime. Apple has some internal logic on how to show this.
        guard let appOpenCount = defaults.value(forKey: promptCounterKey) as? Int else {
            defaults.set(1, forKey: promptCounterKey)
            return
        }
        
        switch appOpenCount {
        case 10,50:
            self.requestReview()
        case _ where appOpenCount%100 == 0 :
            self.requestReview()
        default:
            print("App run count is : \(appOpenCount)")
            break;
        }
        
    }
    
    fileprivate func requestReview() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            // Fallback on earlier versions
            // Try any other 3rd party or manual method here.
        }
    }
    
}
