//
//  RecentTokensTableViewController.swift
//  MTGCounters
//
//  Created by William King on 5/23/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class RecentTokensTableViewController: MDMTableViewController {
    
    var recentTokens = Array<CardData>()
    var tokenCategory: String?
    var players = Array<PlayerData>()
    var playerNumber = 1
    var deckMode: Bool = false
    var deckName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.recentTokens = RecentsService().getRecentTokens()
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.UISetup()
    }

    override var prefersStatusBarHidden: Bool {
        if self.playerNumber == 2 {
            return true
        } else {
            return false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recentTokens.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentTokensCell", for: indexPath) as! RecentTokenTableViewCell

        cell.tokenStatsLabel.text = self.recentTokens[indexPath.row].stats
        cell.tokenNameLabel.text = self.recentTokens[indexPath.row].category
        cell.tokenDescriptionLabel.text = self.recentTokens[indexPath.row].description
        cell.configureManaIcons(colors: self.recentTokens[indexPath.row].color)
        cell.contentView.backgroundColor = self.primaryColor
        cell.backgroundColor = self.primaryColor
        cell.tokenStatsLabel.textColor = self.secondaryColor
        cell.tokenNameLabel.textColor = self.secondaryColor
        cell.tokenDescriptionLabel.textColor = self.secondaryColor
        cell.separator.backgroundColor = self.color3

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Change the text color of the color label to signify it is highlighted
        let cell = tableView.cellForRow(at: indexPath) as! RecentTokenTableViewCell
        cell.tokenNameLabel.textColor = UIColor.lightGray
        cell.tokenDescriptionLabel.textColor = UIColor.lightGray
        cell.tokenStatsLabel.textColor = UIColor.lightGray
        cell.contentView.backgroundColor = ColorManager().getHighlightColor(for: GameData.sharedInstance.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        
        if deckMode {
            // Deck Mode: Submit token and segue back to the deck list
            DecksServices().submitTo(deck: self.deckName, card: recentTokens[indexPath.row], errorView: self, completion: { 
                var viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                viewControllers.remove(at: viewControllers.count - 2)
                self.navigationController?.setViewControllers(viewControllers, animated: false)
                HeroWrapper.unCoverUp()
                self.navigationController?.popViewController(animated: true)
            })
        } else {
            
            if self.players.count == 2 {
                // Segue to TwoPlayerBattleFieldVC, declare a new card on that view, then submit the card into the recents list
                let battlefieldVC = storyboard?.instantiateViewController(withIdentifier: "TwoPlayerBattlefieldVC") as! TwoPlayerBattlefieldViewController
                battlefieldVC.newCard = recentTokens[indexPath.row]
                battlefieldVC.newCardPlayerNumber = self.playerNumber
                RecentsService().submitRecent(token: recentTokens[indexPath.row])
                battlefieldVC.players = self.players
                if self.playerNumber == 2 && UIApplication.shared.statusBarOrientation.isPortrait {
                    let snap = self.view.snapshotView(afterScreenUpdates: true)
                    self.view.addSubview(snap!)
                    snap?.transform = CGAffineTransform(rotationAngle: .pi)
                    HeroWrapper.unCoverDown()
                } else {
                    HeroWrapper.unCoverUp()
                }
                navigationController?.setViewControllers([battlefieldVC], animated: true)
            } else {
                // Segue to OnePlayerBattleFieldVC, declare a new card on that view, then submit the card into the recents list
                let battlefieldVC = storyboard?.instantiateViewController(withIdentifier: "OnePlayerBattlefieldVC") as! OnePlayerBattlefieldViewController
                battlefieldVC.newCard = self.recentTokens[indexPath.row]
                RecentsService().submitRecent(token: self.recentTokens[indexPath.row])
                battlefieldVC.player1 = self.players[0]
                HeroWrapper.unCoverUp()
                navigationController?.setViewControllers([battlefieldVC], animated: true)
            }

        }

    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
        
    }

}


// MARK: STYLE SETUP
extension RecentTokensTableViewController {
    
    func UISetup() {
        self.title = self.tokenCategory
        
        // Acquire colors
        let colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
        
        // Set Colors
        self.formatNavigationBar()
        self.tableView.backgroundColor = primaryColor
        
        if self.colorScheme == .colorless || self.colorScheme == .green || self.colorScheme == .white {
            self.tableView.indicatorStyle = .black
        } else {
            self.tableView.indicatorStyle = .white
        }
    }
}
