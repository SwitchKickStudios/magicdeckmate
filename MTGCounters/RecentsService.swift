//
//  RecentsService.swift
//  MTGCounters
//
//  Created by William King on 5/23/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

class RecentsService {
    
    
    // Adds and limits recent tokens
    func submitRecent(token: CardData) {
        var recentTokensArray = UserDefaults.standard.array(forKey: "recentTokens") as? [[String:Any]] ?? [[String:Any]]()
        
        let newRecentToken: [String:Any] = [
            "description":token.description!,
            "name":token.name!,
            "category":token.category!,
            "color":token.color,
            "stats":token.stats
        ]
        
        var index = 0 // keeps track of the current index as we loop over the array
        
        // loop over the array to search for duplicates
        for token in recentTokensArray {
            if newRecentToken["name"] as! String == token["name"] as! String {
                recentTokensArray.remove(at: index)
                index -= 1 // decrement by 1 to adjust for removed item
            }
            index += 1
        }
        recentTokensArray.insert(newRecentToken, at: 0)
        if (recentTokensArray.count) > 20 {
            recentTokensArray.remove(at: 20)
        }
        UserDefaults.standard.set(recentTokensArray, forKey: "recentTokens")
    }

    // Adds and limits recent counters
    func submitRecent(counter: CardData) {
        var recentCountersArray = UserDefaults.standard.array(forKey: "recentCounters") as? [[String:Any]] ?? [[String:Any]]()
        
        let newRecentCounter: [String:Any] = [
            "description":counter.description!,
            "name":counter.name!,
            "category":counter.category!,
            "color":counter.color,
            "stats":counter.stats
        ]
        
        var index = 0 // keeps track of the current index as we loop over the array
        
        // loop over the array to search for duplicates
        for counter in recentCountersArray {
            if newRecentCounter["name"] as! String == counter["name"] as! String {
                recentCountersArray.remove(at: index)
                index -= 1 // decrement by 1 to adjust for removed item
            }
            index += 1
        }
        recentCountersArray.insert(newRecentCounter, at: 0)
        if (recentCountersArray.count) > 20 {
            recentCountersArray.remove(at: 20)
        }
        UserDefaults.standard.set(recentCountersArray, forKey: "recentCounters")
    }
    
    
    
    // Retrieves an array of recent tokens
    // Loops over the raw array in UserDefaults create CardData for each item
    func getRecentTokens() -> [CardData] {
        
        let rawTokensArray: [[String:Any]] = UserDefaults.standard.array(forKey: "recentTokens") as? [[String : Any]] ?? [[String:Any]]()
        var recentTokensArray = [CardData]()
        
        for cardInfo in rawTokensArray {
            let recentCard = CardData(description: cardInfo["description"] as! String
                                        , name: cardInfo["name"] as! String
                                        , type: .token
                                        , category: cardInfo["category"] as! String
                                        , color: cardInfo["color"] as! [String]
                                        , stats: cardInfo["stats"] as! String)
            recentTokensArray.append(recentCard)
        }
        
        return recentTokensArray
    }
    
    // Retrieves an array of recent counters
    // Loops over the raw array in UserDefaults create CardData for each item
    func getRecentCounters() -> [CardData] {
        
        let rawCountersArray: [[String:Any]] = UserDefaults.standard.array(forKey: "recentCounters") as? [[String : Any]] ?? [[String:Any]]()
        var recentCountersArray = [CardData]()
        
        for cardInfo in rawCountersArray {
            let recentCard = CardData(description: cardInfo["description"] as! String
                , name: cardInfo["name"] as! String
                , type: .counter
                , category: cardInfo["category"] as! String
                , color: cardInfo["color"] as! [String]
                , stats: cardInfo["stats"] as! String)
            recentCountersArray.append(recentCard)
        }
        
        return recentCountersArray
    }
    
    
}
