//
//  TokenCardViewController.swift
//  MTGCounters
//
//  Created by William King on 5/1/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol TokenDelegate {
    func updateToken(id: Int, orientation: SlotState, quantity: Int)
    func getPlayerData() -> PlayerData
}

class TokenCardViewController: GenericCard {

    var labelColor: UIColor?
    
    @IBOutlet weak var tokenQuantityLabel: UILabel!
    @IBOutlet weak var tokenImageView: UIImageView!
    @IBOutlet weak var tokenSlotGuideView: UIView!
    
    @IBOutlet weak var tokenQuantityLabelYPosition: NSLayoutConstraint!
    
    var delegate: TokenDelegate? = nil
    var battlefieldDelegate: BattlefieldCollectionDelegate? = nil
    
    let swipeUp = UISwipeGestureRecognizer()
    let swipeDown = UISwipeGestureRecognizer()
    let swipeLeft = UISwipeGestureRecognizer()
    let swipeRight = UISwipeGestureRecognizer()
    let longPress = UILongPressGestureRecognizer()
    let longPressForAnimation = UILongPressGestureRecognizer()
    
    var orientation: SlotState = .untapped
    var quantity = 0
    var yPositionCoefficient: CGFloat = -0.15
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupControls()
        setupCardInterface()
        self.adjustQuantityLabelFontSize() // Set font size of the quantity label

    }
    
    // This is fired whenever a change in layout is detecte (I.E. orientation change or size change)
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.adjustQuantityLabelFontSize() // Set font size of the quantity label
    }

    // Wait for the view to finish changing orientation
    // Then adjust the layout constraints
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.view.layoutIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupControls() {
        tokenImageView.isUserInteractionEnabled = true
        // Swipe Up
        swipeUp.addTarget(self, action: #selector(swipeUp(_:)))
        swipeUp.direction = .up
        self.tokenImageView.addGestureRecognizer(swipeUp)
        // Swipe Down
        swipeDown.addTarget(self, action: #selector(swipeDown(_:)))
        swipeDown.direction = .down
        self.tokenImageView.addGestureRecognizer(swipeDown)
        // Swipe Left
        swipeLeft.addTarget(self, action: #selector(swipeLeft(_:)))
        swipeLeft.direction = .left
        self.tokenImageView.addGestureRecognizer(swipeLeft)
        // Swipe Right
        swipeRight.addTarget(self, action: #selector(swipeRight(_:)))
        swipeRight.direction = .right
        self.tokenImageView.addGestureRecognizer(swipeRight)
        // Long Press
        longPress.addTarget(self, action: #selector(longPress(_:)))
        longPress.minimumPressDuration = self.delayBeforeRemoval
        self.tokenImageView.addGestureRecognizer(longPress)
        longPress.delegate = self
        // Long Press For Build-Up-To-Removal Animation
        longPressForAnimation.addTarget(self, action: #selector(longPressForAnimation(_:)))
        longPressForAnimation.minimumPressDuration = self.delayBeforeRomovalAnimationStart
        self.tokenImageView.addGestureRecognizer(longPressForAnimation)
        longPressForAnimation.delegate = self
    }
    
    // LONG PRESS
    @objc func longPress(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            delegate?.updateToken(id: self.id!, orientation: .empty, quantity: self.quantity)
            self.deathIconImageView.alpha = 1.0
            self.deathIconBackgroundImageView.alpha = 1.0
            UIView.animate(withDuration: 0.5, animations: {
                self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.view.alpha = 0.0
                self.deathIconImageView.alpha = 1.0
                self.deathIconBackgroundImageView.alpha = 1.0
            },completion: {(finished: Bool) in
                if finished == true {
                    self.battlefieldDelegate?.adjustBattlefield()
                    self.view.removeFromSuperview()
                }
            })
        }
    }
    
    // UP SWIPE
    @objc func swipeUp(_ sender: UISwipeGestureRecognizer) {
        switch orientation {
        case .tappedLeft:
            return
        case .tappedRight:
            return
        case .untapped:
            increaseQuantity()
        default:
            return
        }
    }
    
    // DOWN SWIPE
    @objc func swipeDown(_ sender: UISwipeGestureRecognizer) {
        switch orientation {
        case .tappedLeft:
            unTap()
        case .tappedRight:
            unTap()
        case .untapped:
            decreaseQuantity()
        default:
            return
        }
    }
    
    // LEFT SWIPE
    @objc func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        switch orientation {
        case .tappedLeft:
            decreaseQuantity()
        case .tappedRight:
            increaseQuantity()
        case .untapped:
            tapLeft()
        default:
            return
        }
    }
    
    // RIGHT SWIPE
    @objc func swipeRight(_ sender: UISwipeGestureRecognizer) {
        switch orientation {
        case .tappedLeft:
            increaseQuantity()
        case .tappedRight:
            decreaseQuantity()
        case .untapped:
            tapRight()
        default:
            return
        }
    }
    
    // Initial Creation of the Token
    func setupCardInterface() {
        if let createdCard = card {
            tokenImageView.image = UIImage(named: createdCard.name!)
            tokenQuantityLabel.textColor = labelColor
            deathIconImageView.alpha = 0.0
            deathIconBackgroundImageView.alpha = 0.0
            quantity = card?.quantity ?? 0
            tokenQuantityLabel.text = String(quantity)
            switch self.orientation {
            case .tappedLeft:
                setTappedLeft()
            case .tappedRight:
                setTappedRight()
            case .untapped:
                setUntapped()
            default:
                return
            }
        }
    }
    
    // Method to readjust the quantity label - animated
    func readjustQuantityLabel() {
        if self.orientation == .tappedLeft || self.orientation == .tappedRight {
            UIView.animate(withDuration: 0.1, animations: ({
                if UIDevice.current.orientation.isLandscape {
                    self.view.layoutIfNeeded()
                    self.tokenQuantityLabelYPosition.constant = self.tokenSlotGuideView.layer.frame.size.height * self.yPositionCoefficient
                    self.view.layoutIfNeeded()
                } else {
                    self.view.layoutIfNeeded()
                    self.tokenQuantityLabelYPosition.constant = self.tokenImageView.layer.frame.size.height * self.yPositionCoefficient
                    self.view.layoutIfNeeded()
                }
            }))
        }
    }
    // Adjust the font size of the quantity label
    // Depending on number of cards in play and orientation
    func adjustQuantityLabelFontSize() {
        let orientation = UIApplication.shared.statusBarOrientation
        let device = UIDevice.current
        let player = delegate?.getPlayerData()
        let game = GameData.sharedInstance
        var fontSize: CGFloat = 40
    
        // Font Sizes for tutorial tokens
        if self.isExample == true {
            if game.numberOfPlayers == 2 {
                if device.userInterfaceIdiom == .phone {
                    fontSize = 30
                } else {
                    fontSize = 64
                }
            } else {
                if device.userInterfaceIdiom == .phone && orientation.isPortrait {
                    fontSize = 45
                } else if device.userInterfaceIdiom == .phone {
                    fontSize = 30
                } else {
                    fontSize = 72
                }
            }
        
        // Font Sizes for battlefield tokens
        } else {
            switch game.numberOfPlayers {
            case 2:
                // iPhone Landscape
                if orientation.isLandscape && device.userInterfaceIdiom == .phone {
                    if player!.getCardCount() >= game.growAtCards2[2]! {
                        fontSize = 24
                    } else if player!.getCardCount() >= game.growAtCards1[2]! {
                        fontSize = 35
                    } else {
                        fontSize = 45
                    }
                    // iPhone Portrait
                } else if device.userInterfaceIdiom == .phone {
                    if player!.getCardCount() >= game.growAtCards2[2]! {
                        fontSize = 22
                    } else if player!.getCardCount() >= game.growAtCards1[2]! {
                        fontSize = 36
                    } else {
                        fontSize = 36
                    }
                    // iPad Landscape
                } else if orientation.isLandscape && device.userInterfaceIdiom == .pad {
                    if player!.getCardCount() >= game.growAtCards2[2]! {
                        fontSize = 44
                    } else if player!.getCardCount() >= game.growAtCards1[2]! {
                        fontSize = 55
                    } else {
                        fontSize = 64
                    }
                    // iPad Portrait
                } else {
                    if player!.getCardCount() >= game.growAtCards2[2]! {
                        fontSize = 40
                    } else if player!.getCardCount() >= game.growAtCards1[2]! {
                        fontSize = 64
                    } else {
                        fontSize = 72
                    }
                }
            default:
                // iPhone Landscape
                if orientation.isLandscape && device.userInterfaceIdiom == .phone {
                    if player!.getCardCount() >= game.growAtCards2[1]! {
                        fontSize = 30
                    } else if player!.getCardCount() >= game.growAtCards1[1]! {
                        fontSize = 35
                    } else {
                        fontSize = 40
                    }
                    // iPhone Portrait
                } else if device.userInterfaceIdiom == .phone {
                    if player!.getCardCount() >= game.growAtCards2[1]! {
                        fontSize = 30
                    } else if player!.getCardCount() >= game.growAtCards1[1]! {
                        fontSize = 40
                    } else {
                        fontSize = 45
                    }
                    // iPad Landscape
                } else if orientation.isLandscape && device.userInterfaceIdiom == .pad {
                    if player!.getCardCount() >= game.growAtCards2[1]! {
                        fontSize = 35
                    } else if player!.getCardCount() >= game.growAtCards1[1]! {
                        fontSize = 55
                    } else {
                        fontSize = 55
                    }
                    // iPad Portrait
                } else {
                    if player!.getCardCount() >= game.growAtCards2[1]! {
                        fontSize = 45
                    } else if player!.getCardCount() >= game.growAtCards1[1]! {
                        fontSize = 64
                    } else {
                        fontSize = 72
                    }
                }
            }
        }
        self.view.layoutIfNeeded()
        self.tokenQuantityLabel.font = self.tokenQuantityLabel.font.withSize(fontSize) // Set font size of the quantity label
        self.view.layoutIfNeeded()
    }
    
    
    // Functions to increase or decrease token quantity and update label
    func increaseQuantity() {
        quantity += 1
        tokenQuantityLabel.text = String(quantity)
        delegate?.updateToken(id: self.id!, orientation: self.orientation, quantity: self.quantity)
    }
    func decreaseQuantity() {
        quantity -= 1
        tokenQuantityLabel.text = String(quantity)
        delegate?.updateToken(id: self.id!, orientation: self.orientation, quantity: self.quantity)
    }
    
    // Functions to rotate the token image and save its orientation
    func unTap() {
        UIView.animate(withDuration: 0.5, animations: ({
            self.tokenImageView.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            self.lowerQuantityLabel()
        }))
        orientation = .untapped
        delegate?.updateToken(id: self.id!, orientation: self.orientation, quantity: self.quantity)
    }
    func tapLeft() {
        UIView.animate(withDuration: 0.5, animations: ({
            self.raiseQuantityLabel()
            self.tokenImageView.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
        }))
        orientation = .tappedLeft
        delegate?.updateToken(id: self.id!, orientation: self.orientation, quantity: self.quantity)
    }
    func tapRight() {
        UIView.animate(withDuration: 0.5, animations: ({
            self.raiseQuantityLabel()
            self.tokenImageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
        }))
        orientation = .tappedRight
        delegate?.updateToken(id: self.id!, orientation: self.orientation, quantity: self.quantity)
    }
    
    
    // Functions to animate the tokenQuantityLabel up and down
    func raiseQuantityLabel() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
            self.tokenQuantityLabelYPosition.constant = self.tokenSlotGuideView.layer.frame.size.height * self.yPositionCoefficient
            self.view.layoutIfNeeded()
        })
    }
    func lowerQuantityLabel() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
            self.tokenQuantityLabelYPosition.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    
    // Functions to set the physical orientation of the token
    // Used when the token loads to set to the previously saved qualities
    func setUntapped() {
        self.setQuantityLabelLower()
        self.tokenImageView.transform = CGAffineTransform(rotationAngle: CGFloat(0))
    }
    func setTappedLeft() {
        setQuantityLabelRaise()
        self.tokenImageView.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
    }
    func setTappedRight() {
        setQuantityLabelRaise()
        self.tokenImageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
    }
    func setQuantityLabelRaise() {
        self.view.layoutIfNeeded()
        self.tokenQuantityLabelYPosition.constant = (self.tokenSlotGuideView.layer.frame.size.height * self.yPositionCoefficient) + (self.tokenSlotGuideView.layer.frame.size.height * 0.1)
        self.view.layoutIfNeeded()
    }
    func setQuantityLabelLower() {
        self.view.layoutIfNeeded()
        self.tokenQuantityLabelYPosition.constant = 0
        self.view.layoutIfNeeded()
    }


    
}







