//
//  DataFormatServices.swift
//  MTGCounters
//
//  Created by William King on 4/30/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation

class DataFormatService {
    
    // - TOKENS -
    // Retrieve tokens data from the tokens plist
    // Parse data, create card data, then populate token dictionary
    func getTokenDictionary() -> [String:[CardData]] {
        var tokenDictionary = Dictionary<String,[CardData]>()
        let tokensData = UserDefaults.standard.dictionary(forKey: "TokensDictionary")!
        
        // Loop through eachkey in the dictionary
        for category in tokensData {
            
            // Create an array of the values within the key
            var cardsInCategory = [Dictionary<String, Any>]()
            if let categoryValues = category.value as? [Dictionary<String, Any>] {
                cardsInCategory = categoryValues
            }
            
            // Initialize CardData array, then loop over the array of descriptions
            // Use each description to create CardData and stuff it into CardData Array
            var tokensInCategory = [CardData]()
            var cardIndex = 0
            for card in cardsInCategory {
                cardIndex += 1
                let cardName = String(category.key.replacingOccurrences(of: " ", with: "") + "Token \(String(describing: cardIndex))")
                let newCardData: CardData = CardData(description: card["description"] as! String
                                                                , name: cardName
                                                                , type: .token
                                                                , category: category.key
                                                                , color: card["color"] as! [String]
                                                                , stats: card["stats"] as! String)
                tokensInCategory.append(newCardData)
        
            }
            // Put the CardData Array into the key of the new dictionary
            tokenDictionary[category.key] = tokensInCategory
            
        }
        return tokenDictionary
    }
    
    // - COUNTERS -
    // Retrive counters data from the counters plist
    // Parse data, create card data, then populate counter dictionary
    func getCounterDictionary() -> [String:[CardData]] {
        var counterDictionary = Dictionary<String,[CardData]>()
        let countersData = UserDefaults.standard.dictionary(forKey: "CountersDictionary")!
        
        // Loop through each key in the dictionary
        for category in countersData {
            
            // Create an array of the values within the key
            var categoryValuesArray = [String]()
            if let descriptionsInCategory = category.value as? [String] {
                categoryValuesArray = descriptionsInCategory
            }
            
            // Initialize CardData array, then loop over the array of descriptions
            // Use each description to create CardData and stuff it into CardData Array
            var countersInCategory = [CardData]()
            for description in categoryValuesArray {
                
                let cardName = String(description.replacingOccurrences(of: "/", with: "").replacingOccurrences(of: " ", with: "") + "Counter")
                let newCardData: CardData = CardData(description: description, name: cardName, type: .counter, category: category.key, color: ["colorless"], stats: "counter")
                countersInCategory.append(newCardData)
            }
            // Put CardData Array into the key of the new dictionary
            counterDictionary[category.key] = countersInCategory
        }
        return counterDictionary
    }
}












