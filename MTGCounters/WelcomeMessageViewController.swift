//
//  WelcomeMessageViewController.swift
//  MTGCounters
//
//  Created by Conor King on 6/11/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class WelcomeMessageViewController: MDMViewController, CloseModalDelegate {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var tutorialMode = false
    
    @IBOutlet weak var welcomeMessageLabel: UILabel!
    @IBOutlet weak var descriptionMessageLabel: UILabel!
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var topMessageBufferView: UIView!
    @IBOutlet weak var bottomMessageBufferView: UIView!
    @IBOutlet weak var background: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.UISetup()
    }
    
    // NextModalDelegate function
    func closeButton() {
        self.delegate?.openView(selection: "tokenControls", animate: false)
    }

}

// ------------------------------------------
// STYLING
// ------------------------------------------
extension WelcomeMessageViewController {
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Hello"
        customTopBar.view.frame = self.topBufferView.bounds
        customTopBar.shouldShowTipButton = !tutorialMode
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        
        customNavBar.showCloseButton = true
        customNavBar.showBackButton = false
        customNavBar.showNextButton = false
        
        customNavBar.closeButtonText = "Next"
        customNavBar.closeDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.showTopBorder = true
        
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
        

    }
    
    func UISetup() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color4 = colorData.color4
        
        self.background.backgroundColor = primaryColor
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        self.topMessageBufferView.backgroundColor = UIColor.clear
        self.bottomMessageBufferView.backgroundColor = UIColor.clear
        self.welcomeMessageLabel.textColor = secondaryColor
        self.descriptionMessageLabel.textColor = secondaryColor
    }
}







