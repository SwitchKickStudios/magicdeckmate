//
//  ColorData.swift
//  MTGCounters
//
//  Created by William King on 5/16/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import Foundation
import UIKit

class ColorData {
    
    var backgroundLarge: String?
    var menuIcon: UIImage?
    var primaryColor: UIColor?
    var secondaryColor: UIColor?
    var color3: UIColor?
    var color4: UIColor?
    var color5: UIColor?
    
    init(backgroundLarge: String,
         menuIcon: UIImage,
         primaryColor: UIColor,
         secondaryColor: UIColor,
         color3: UIColor,
         color4: UIColor,
         color5: UIColor) {
        
        self.backgroundLarge = backgroundLarge
        self.menuIcon = menuIcon
        self.primaryColor = primaryColor
        self.secondaryColor = secondaryColor
        self.color3 = color3
        self.color4 = color4
        self.color5 = color5
    }
    
    
}
