//
//  GenericCard.swift
//  MTGCounters
//
//  Created by Conor King on 3/8/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class GenericCard: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var deathIconImageView: UIImageView!
    @IBOutlet weak var deathIconBackgroundImageView: UIImageView!
    
    var card: CardData?
    var id: Int?
    var isExample = false
    
    let delayBeforeRemoval: Double = 1.5
    let delayBeforeRomovalAnimationStart: Double = 0.2
    
    // UIGestureRecognizerDelegate delegate function
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer is UILongPressGestureRecognizer {
            return true
        } else {
            return false
        }
    }
    
    @objc func longPressForAnimation(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            UIView.animate(withDuration: self.delayBeforeRemoval - self.delayBeforeRomovalAnimationStart, delay: 0.0, options: .curveEaseIn, animations: {
                self.deathIconImageView.alpha = 1.0
                self.deathIconBackgroundImageView.alpha = 1.0
            })
        } else if sender.state == .ended {
            self.deathIconImageView.alpha = 0.0
            self.deathIconBackgroundImageView.alpha = 0.0
        }
    }
}
