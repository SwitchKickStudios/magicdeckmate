//
//  StyleManager.swift
//  MTGCounters
//
//  Created by Conor King on 3/25/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import Foundation
import UIKit

class StyleManager {
    
    // Parameters
    let menuCornerRadius: CGFloat = 13
    let menuBorderWidth: CGFloat = 1.5
    let shadowRadius: CGFloat = 3.5
    let shadowOpacity: Float = 0.65
    let shadowDropLength: Double = 3.5
    let shadowRadiusSmall: CGFloat = 1.5
    let shadowDropLengthSmall: Double = 2
    let buttonCornerRadius: CGFloat = 10
    
    var primaryColor: UIColor?
    var secondaryColor: UIColor?
    var color3: UIColor?
    var colorData: ColorData?
    var colorScheme: ColorSchemes?
    
    init(colorData: ColorData, scheme: ColorSchemes) {
        self.colorData = colorData
        self.colorScheme = scheme
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
    }
    
    // Format a button
    func format(button: UIButton, type: ButtonTypes, iconImageView: UIImageView?) {
        switch type {
            
        case .standardButton:
            button.layer.borderWidth = self.menuBorderWidth
            button.layer.borderColor = self.color3?.cgColor
            button.layer.cornerRadius = self.buttonCornerRadius
            button.backgroundColor = self.primaryColor
            button.setTitleColor(self.secondaryColor, for: .normal)
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLength)
            button.layer.shadowRadius = self.shadowRadius
            button.layer.shadowOpacity = self.shadowOpacity
            
        case .smallButton:
            button.layer.borderWidth = self.menuBorderWidth
            button.layer.borderColor = self.color3?.cgColor
            button.layer.cornerRadius = self.buttonCornerRadius
            button.backgroundColor = self.primaryColor
            button.setTitleColor(self.secondaryColor, for: .normal)
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLengthSmall)
            button.layer.shadowRadius = self.shadowRadiusSmall
            button.layer.shadowOpacity = self.shadowOpacity
            
        case .circlarMenuButton:
            let buttons = Buttons()
            button.setBackgroundImage(buttons.circleButton(diameter: button.layer.bounds.width, color: primaryColor!), for: .normal)
            button.setBackgroundImage(buttons.circleButton(diameter: button.layer.bounds.width, color: ColorManager().getHighlightColor(for: self.colorScheme, color: self.primaryColor!)), for: .highlighted)
            button.backgroundColor = self.primaryColor
            button.layer.cornerRadius = button.layer.bounds.width/2
            button.layer.borderColor = self.color3?.cgColor
            button.layer.borderWidth = self.menuBorderWidth
            button.setTitleColor(self.secondaryColor, for: .normal)
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLength)
            button.layer.shadowRadius = self.shadowRadius
            button.layer.shadowOpacity = self.shadowOpacity
            if iconImageView != nil {
                iconImageView!.image = self.colorData?.menuIcon
            }
            
        case .circularMenuButtonSmallShadow:
            let buttons = Buttons()
            button.setBackgroundImage(buttons.circleButton(diameter: button.layer.bounds.width, color: primaryColor!), for: .normal)
            button.setBackgroundImage(buttons.circleButton(diameter: button.layer.bounds.width, color: ColorManager().getHighlightColor(for: self.colorScheme, color: self.primaryColor!)), for: .highlighted)
            button.backgroundColor = self.primaryColor
            button.layer.cornerRadius = button.layer.bounds.width/2
            button.layer.borderColor = self.color3?.cgColor
            button.layer.borderWidth = self.menuBorderWidth
            button.setTitleColor(self.secondaryColor, for: .normal)
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLengthSmall)
            button.layer.shadowRadius = self.shadowRadiusSmall
            button.layer.shadowOpacity = self.shadowOpacity
            if iconImageView != nil {
                iconImageView!.image = self.colorData?.menuIcon
            }
            
        case .menuItem:
            button.backgroundColor = primaryColor
            button.setTitleColor(secondaryColor, for: .normal)
            
        case .editButton:
            button.layer.borderWidth = self.menuBorderWidth
            button.layer.borderColor = self.color3?.cgColor
            button.layer.cornerRadius = self.buttonCornerRadius
            button.backgroundColor = primaryColor
            button.setTitleColor(self.secondaryColor, for: .normal)
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLengthSmall)
            button.layer.shadowRadius = self.shadowRadiusSmall
            button.layer.shadowOpacity = self.shadowOpacity
            
        case .editButton2:
            button.backgroundColor = UIColor.white
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLengthSmall)
            button.layer.shadowRadius = self.shadowRadiusSmall
            button.layer.shadowOpacity = self.shadowOpacity
            
        case .deleteButton:
            button.layer.borderWidth = self.menuBorderWidth
            button.layer.borderColor = self.color3?.cgColor
            button.layer.cornerRadius = buttonCornerRadius
            button.backgroundColor = primaryColor
            button.setTitleColor(self.secondaryColor, for: .normal)
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: shadowDropLengthSmall)
            button.layer.shadowRadius = shadowRadiusSmall
            button.layer.shadowOpacity = shadowOpacity
        
        case .deleteButton2:
            button.backgroundColor = UIColor.red
            button.layer.shadowColor = UIColor.black.cgColor
            button.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLengthSmall)
            button.layer.shadowRadius = self.shadowRadiusSmall
            button.layer.shadowOpacity = self.shadowOpacity
            
        }
        
    }
    
    // Format a View
    func format(view: UIView) {
        view.layer.borderWidth = self.menuBorderWidth
        view.layer.borderColor = self.color3?.cgColor
        view.layer.cornerRadius = self.buttonCornerRadius
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLength)
        view.layer.shadowRadius = self.shadowRadius
        view.layer.shadowOpacity = self.shadowOpacity
        view.backgroundColor = self.primaryColor
    }
    
    // Format an imageView
    func format(imageView: UIImageView) {
        imageView.image = UIImage(named: self.colorData!.backgroundLarge!)
        imageView.layer.shadowColor = UIColor.black.cgColor
        imageView.layer.shadowOffset = CGSize(width: 0.0, height: self.shadowDropLength)
        imageView.layer.shadowRadius = self.shadowRadius
        imageView.layer.shadowOpacity = self.shadowOpacity
    }
    
    // Format a border
    func format(borderView: UIView, widthConstraint: NSLayoutConstraint) {
        widthConstraint.constant = self.menuBorderWidth
        borderView.backgroundColor = self.color3
    }
    
}
