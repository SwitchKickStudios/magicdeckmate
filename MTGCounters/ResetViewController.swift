//
//  ResetViewController.swift
//  MTGCounters
//
//  Created by Conor King on 11/17/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol ResetDelegate {
    func resetBattlefield(playerNumber: Int, type: ResetType)
}

class ResetViewController: MDMViewController , CloseModalDelegate, BackModalDelegate {

    var delegate: MenuNavigationDelegate? = nil
    var resetDelegate: ResetDelegate? = nil
    var playerNumber = 1
    
    @IBOutlet weak var resetLifeButton: UIButton!
    @IBOutlet weak var resetCardValuesButton: UIButton!
    @IBOutlet weak var removeAllCardsButton: UIButton!
    @IBOutlet weak var resetAllButton: UIButton!
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var resetLifeBorder: UIView!
    @IBOutlet weak var resetCardValuesBorder: UIView!
    @IBOutlet weak var removeAllCardsBorder: UIView!
    @IBOutlet weak var resetAllBorder: UIView!
    
    let alertTitles: [ResetType:String] = [
        .resetLife:"Reset Life?",
        .setCardsToZero:"Reset Card Values?",
        .resetCards:"Clear Cards?",
        .resetAll:"Reset the Battlfield?",
    ]
    
    let alertMessages: [ResetType:String] = [
        .resetLife:"This will set your life back to the starting life total.",
        .setCardsToZero:"This will allow your cards to remain on the battlefield but return their respective values to zero.",
        .resetCards:"This will remove all cards from the battlefield.",
        .resetAll:"This will clear the battlefield as well as reset the life count.",
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.sizingSetup()
        self.UISetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetLifeButton(_ sender: Any) {
        processReset(type: .resetLife)
    }
    @IBAction func resetCardValuesButton(_ sender: Any) {
        processReset(type: .setCardsToZero)
    }
    @IBAction func removeAllCardsButton(_ sender: Any) {
        processReset(type: .resetCards)
    }
    @IBAction func resetAllButton(_ sender: Any) {
        processReset(type: .resetAll)
    }
    
    func processReset(type: ResetType) {
        // Create an alert before calling the delegate method do perform the proper reset
        let alert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertViewController
        alert.player = self.playerNumber
        alert.setAlertText(title: self.alertTitles[type]!, message: self.alertMessages[type]!, b1: "No", b2: "Yes")
        
        alert.button1Action = { (alertVC) in
            self.unHighlightButtons()
        }
        alert.button2Action = { (alertVC) in
            self.resetDelegate?.resetBattlefield(playerNumber: self.playerNumber, type: type)
            self.unHighlightButtons()
        }
        alert.show()
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        self.delegate?.closeMenu()
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "main", animate: false)
    }
    
}

// MARK: STYLE SETUP
extension ResetViewController {
    
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Battlefield Resets"
        customTopBar.view.frame = self.topBufferView.bounds
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        customNavBar.showBackButton = true
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func sizingSetup() {
        self.resetLifeButton.set(fontSize: getCenterMenuFontSize())
        self.resetCardValuesButton.set(fontSize: getCenterMenuFontSize())
        self.removeAllCardsButton.set(fontSize: getCenterMenuFontSize())
        self.resetAllButton.set(fontSize: getCenterMenuFontSize())
    }
    
    func UISetup() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: self.colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color3 = colorData.color3
        self.color4 = colorData.color4
        
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        
        // Separators
        self.resetLifeBorder.backgroundColor = self.color3
        self.resetCardValuesBorder.backgroundColor = self.color3
        self.removeAllCardsBorder.backgroundColor = self.color3
        self.resetAllBorder.backgroundColor = self.color3

        // Buttons
        styleManager.format(button: self.resetLifeButton, type: .menuItem, iconImageView: nil)
        self.resetLifeButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
        styleManager.format(button: self.resetCardValuesButton, type: .menuItem, iconImageView: nil)
        self.resetCardValuesButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
        styleManager.format(button: self.removeAllCardsButton, type: .menuItem, iconImageView: nil)
        self.removeAllCardsButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
        styleManager.format(button: self.resetAllButton, type: .menuItem, iconImageView: nil)
        self.resetAllButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)

    }

    // Change color back
    func unHighlightButtons() {
        self.resetLifeButton.backgroundColor? = self.primaryColor!
        self.resetCardValuesButton.backgroundColor? = self.primaryColor!
        self.removeAllCardsButton.backgroundColor? = self.primaryColor!
        self.resetAllButton.backgroundColor? = self.primaryColor!
    }
    
    // Returns a font size based on device class
    func getCenterMenuFontSize() -> CGFloat {
        let device = UIDevice.current
        let game = GameData.sharedInstance
        if device.model == "iPad" {
            if game.numberOfPlayers == 2 {
                return 23
            } else {
                return 25
            }
        } else {
            if game.numberOfPlayers == 2 {
                return 18
            } else {
                return 20
            }
        }
    }

}




