//
//  RecentTokenTableViewCell.swift
//  MTGCounters
//
//  Created by William King on 5/23/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class RecentTokenTableViewCell: UITableViewCell {

    @IBOutlet weak var colorlessManaIcon: UIImageView!
    @IBOutlet weak var blackManaIcon: UIImageView!
    @IBOutlet weak var blueManaIcon: UIImageView!
    @IBOutlet weak var greenManaIcon: UIImageView!
    @IBOutlet weak var redManaIcon: UIImageView!
    @IBOutlet weak var whiteManaIcon: UIImageView!
    @IBOutlet weak var tokenDescriptionLabel: UILabel!
    @IBOutlet weak var tokenNameLabel: UILabel!
    @IBOutlet weak var tokenStatsLabel: UILabel!
    @IBOutlet weak var separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureManaIcons(colors: [String]) {
        // Create an array of possible colors
        var possibleColors = [
            "colorless",
            "black",
            "blue",
            "green",
            "red",
            "white"
        ]
        // Filter the colors from token information from the array of possible colors
        for color in colors {
            possibleColors = possibleColors.filter { $0 != color }
        }
        
        // Turn off the icons that remain in the possible colors
        self.colorlessManaIcon.isHidden = possibleColors.contains("colorless")
        self.blackManaIcon.isHidden = possibleColors.contains("black")
        self.blueManaIcon.isHidden = possibleColors.contains("blue")
        self.greenManaIcon.isHidden = possibleColors.contains("green")
        self.redManaIcon.isHidden = possibleColors.contains("red")
        self.whiteManaIcon.isHidden = possibleColors.contains("white")
        
    }

}
