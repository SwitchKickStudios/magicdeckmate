//
//  ColorSelectViewController.swift
//  MTGCounters
//
//  Created by Conor King on 11/17/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol ColorSelectDelegate {
    func resetColorScheme()
}

class ColorSelectViewController: MDMViewController, CloseModalDelegate, BackModalDelegate {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var colorDelegate: ColorSelectDelegate? = nil
    var game = GameData.sharedInstance
    var colorManager = ColorManager()
    
    @IBOutlet weak var neutralButton: UIButton!
    @IBOutlet weak var blackButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var whiteButton: UIButton!
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var neutralBorder: UIView!
    @IBOutlet weak var blackBorder: UIView!
    @IBOutlet weak var blueBorder: UIView!
    @IBOutlet weak var greenBorder: UIView!
    @IBOutlet weak var redBorder: UIView!
    @IBOutlet weak var whiteBorder: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.customNavigationSetup()
        self.sizingSetup()
        self.UISetup()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func neutralButton(_ sender: Any) {
        resetUI(color: .colorless)
    }
    @IBAction func blackButton(_ sender: Any) {
        resetUI(color: .black)
    }
    @IBAction func blueButton(_ sender: Any) {
        resetUI(color: .blue)
    }
    @IBAction func greenButton(_ sender: Any) {
        resetUI(color: .green)
    }
    @IBAction func redButton(_ sender: Any) {
        resetUI(color: .red)
    }
    @IBAction func whiteButton(_ sender: Any) {
        resetUI(color: .white)
    }
    
    func resetUI(color: ColorSchemes) {
        game.colorSchemes[self.playerNumber-1] = color
        self.colorDelegate?.resetColorScheme()
        if bottomBufferView.subviews.last != nil {
            bottomBufferView.subviews.last?.removeFromSuperview()
        }
        self.UISetup()
        self.customNavigationSetup()
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        self.delegate?.closeMenu()
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "settings", animate: false)
    }
    
    // Used to remove the token
    func removeSubviews(from view: UIView) {
        for subUIView in view.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
    }
    
}

// MARK: STYLE SETUP
extension ColorSelectViewController {

    func customNavigationSetup() {
        self.removeSubviews(from: self.topBufferView)
        self.removeSubviews(from: self.bottomBufferView)
        
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Color"
        customTopBar.view.frame = self.topBufferView.bounds
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        customNavBar.showBackButton = true
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func sizingSetup() {
        self.neutralButton.set(fontSize: getMenuFontSize())
        self.blackButton.set(fontSize: getMenuFontSize())
        self.blueButton.set(fontSize: getMenuFontSize())
        self.greenButton.set(fontSize: getMenuFontSize())
        self.redButton.set(fontSize: getMenuFontSize())
        self.whiteButton.set(fontSize: getMenuFontSize())
    }
    
    func UISetup() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color3 = colorData.color3
        self.color4 = colorData.color4
        
        self.view.layoutIfNeeded()
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        self.view.layer.borderColor = self.color3?.cgColor
        self.view.backgroundColor = self.primaryColor

        // Separators
        self.neutralBorder.backgroundColor = self.color3
        self.blackBorder.backgroundColor = self.color3
        self.blueBorder.backgroundColor = self.color3
        self.greenBorder.backgroundColor = self.color3
        self.redBorder.backgroundColor = self.color3
        self.whiteBorder.backgroundColor = self.color3
        
        // Buttons
        styleManager.format(button: self.neutralButton, type: .menuItem, iconImageView: nil)
        styleManager.format(button: self.blackButton, type: .menuItem, iconImageView: nil)
        styleManager.format(button: self.blueButton, type: .menuItem, iconImageView: nil)
        styleManager.format(button: self.greenButton, type: .menuItem, iconImageView: nil)
        styleManager.format(button: self.redButton, type: .menuItem, iconImageView: nil)
        styleManager.format(button: self.whiteButton, type: .menuItem, iconImageView: nil)

        self.neutralButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpInside)
        self.neutralButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpOutside)
        self.neutralButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)

        self.blackButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpInside)
        self.blackButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpOutside)
        self.blackButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)

        self.blueButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpInside)
        self.blueButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpOutside)
        self.blueButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)

        self.greenButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpInside)
        self.greenButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpOutside)
        self.greenButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)

        self.redButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpInside)
        self.redButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpOutside)
        self.redButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)

        self.whiteButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpInside)
        self.whiteButton.addTarget(self, action: #selector(self.unHighlightColorButton(sender:)), for: .touchUpOutside)
        self.whiteButton.addTarget(self, action: #selector(self.highlightButtonColor(sender:)), for: .touchDown)
        
        self.setSelectedButton()
    }
    
    // Pre-selected Color
    func setSelectedButton() {
        switch GameData.sharedInstance.colorSchemes[self.playerNumber-1] {
        case .black:
            self.blackButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        case .blue:
            self.blueButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        case .green:
            self.greenButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        case .red:
            self.redButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        case .white:
            self.whiteButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        default:
            self.neutralButton.backgroundColor = self.colorManager.getSelectionColor(for: self.game.colorSchemes[self.playerNumber-1], color: self.primaryColor!)
        }
    }
    
    // Change color back
    @objc func unHighlightColorButton(sender: UIButton) {
        self.blackButton.backgroundColor = self.primaryColor!
        self.blueButton.backgroundColor = self.primaryColor!
        self.greenButton.backgroundColor = self.primaryColor!
        self.redButton.backgroundColor = self.primaryColor!
        self.whiteButton.backgroundColor = self.primaryColor!
        self.neutralButton.backgroundColor = self.primaryColor!
        self.setSelectedButton()
    }
    
    // Returns a font size based on device class
    func getMenuFontSize() -> CGFloat {
        let device = UIDevice.current
        let game = GameData.sharedInstance
        if device.model == "iPad" {
            if game.numberOfPlayers == 2 {
                return 23
            } else {
                return 25
            }
        } else {
            if game.numberOfPlayers == 2 {
                return 18
            } else {
                return 20
            }
        }
    }
}







