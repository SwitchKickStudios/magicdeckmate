//
//  CountersTutorialExampleViewController.swift
//  MTGCounters
//
//  Created by Conor King on 5/13/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class CountersTutorialExampleViewController: MDMViewController, CloseModalDelegate, BackModalDelegate, NextModalDelegate {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var tutorialMode = false // Indicates whether we are displaying for inital opening of the app or not.
    
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var contentBackground: UIView!
    @IBOutlet weak var counterTopBufferView: UIView!
    @IBOutlet weak var counterBottomBufferView: UIView!
    @IBOutlet weak var counterGuideView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var bufferSizeConstraint: NSLayoutConstraint!
    
    var countersDictionary = [String:[CardData]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.countersDictionary = DataFormatService().getCounterDictionary()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.UISetup()
        self.setupCounter()
    }
    
    // This is fired whenever a change in layout is detecte (I.E. orientation change or size change)
    // However, if the token has been killed, we do not want this to trigger an immediate reset
    override func viewWillLayoutSubviews() {
        let currentlyPopulated = counterGuideView.subviews.count
        if currentlyPopulated > 0 {
            super.viewWillLayoutSubviews()
            self.removeSubviews()
            self.setupCounter()
            self.view.layoutIfNeeded()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resetButton(_ sender: Any) {
        self.removeSubviews()
        self.view.layoutIfNeeded()
        self.setupCounter()
        self.view.layoutIfNeeded()
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        if self.tutorialMode {
            self.delegate?.openView(selection: "menuNavigationControls", animate: false)
        } else {
            self.delegate?.closeMenu()
        }    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "counterControls", animate: false)
    }
    
    // NextModalDelegate function
    func nextButton() {
        self.delegate?.openView(selection: "menuNavigationControls", animate: false)
    }
    
    // Set up Example Couter
    func setupCounter() {
        let counterVC = storyboard?.instantiateViewController(withIdentifier: "CounterCardVC") as! CounterCardViewController
        var exampleCounter: CardData? = nil
        
        switch self.colorScheme {
        case .black:
            exampleCounter = self.countersDictionary["Mana Counters"]?[0]
        case .blue:
            exampleCounter = self.countersDictionary["Mana Counters"]?[1]
        case .green:
            exampleCounter = self.countersDictionary["Mana Counters"]?[3]
        case .colorless:
            exampleCounter = self.countersDictionary["Mana Counters"]?[2]
        case .red:
            exampleCounter = self.countersDictionary["Mana Counters"]?[4]
        case .white:
            exampleCounter = self.countersDictionary["Mana Counters"]?[5]
        }
        
        counterVC.card = exampleCounter
        counterVC.id = 1
        counterVC.isExample = true

        self.addChildViewController(counterVC)
        counterVC.view.frame = self.counterGuideView.bounds
        self.counterGuideView.addSubview(counterVC.view)
        counterVC.didMove(toParentViewController: self)
    }
    
    // Used to remove the token
    func removeSubviews() {
        for subUIView in counterGuideView.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
    }
    
    // Set up Buffer Views
    func setupBufferViewHeights() {
        let orientation = UIApplication.shared.statusBarOrientation
        let device = UIDevice.current
        if GameData.sharedInstance.numberOfPlayers == 2 || (orientation.isLandscape && device.userInterfaceIdiom == .phone) {
            self.bufferSizeConstraint.isActive = false
        }
    }

}

// MARK: STYLE SETUP
extension CountersTutorialExampleViewController {
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Counter Controls"
        customTopBar.view.frame = self.topBufferView.bounds
        customTopBar.shouldShowTipButton = !tutorialMode
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        
        if self.tutorialMode {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = false
            customNavBar.showCloseButton = true
            customNavBar.closeButtonText = "Next"
        } else {
            customNavBar.showBackButton = true
            customNavBar.showNextButton = true
            customNavBar.showCloseButton = true
        }
        
        customNavBar.showTopBorder = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.nextDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func UISetup() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color4 = colorData.color4
        
        if self.colorScheme == .blue || self.colorScheme == .black || self.colorScheme == .red {
            self.resetButton.setImage(#imageLiteral(resourceName: "ResetIconWhite"), for: .normal)
        } else {
            self.resetButton.setImage(#imageLiteral(resourceName: "ResetIconBlack"), for: .normal)
        }
        
        self.counterTopBufferView.backgroundColor = UIColor.clear
        self.counterBottomBufferView.backgroundColor = UIColor.clear
        self.counterGuideView.backgroundColor = UIColor.clear
        
        self.background.backgroundColor = primaryColor
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        
    }
    
}
