//
//  CounterCardViewController.swift
//  MTGCounters
//
//  Created by William King on 5/2/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol CounterDelegate {
    func updateCounter(id: Int, orientation: SlotState, counterValue: Int)
    func getPlayerData() -> PlayerData
}

class CounterCardViewController: GenericCard {

    @IBOutlet weak var counterImageView: UIImageView!
    @IBOutlet weak var counterQuantityLabel: UILabel!
    
    var delegate: CounterDelegate? = nil
    var battlefieldDelegate: BattlefieldCollectionDelegate? = nil
    
    let swipeUp = UISwipeGestureRecognizer()
    let swipeDown = UISwipeGestureRecognizer()
    let swipeLeft = UISwipeGestureRecognizer()
    let swipeRight = UISwipeGestureRecognizer()
    let longPress = UILongPressGestureRecognizer()
    let longPressForAnimation = UILongPressGestureRecognizer()
    
    var counterValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupControls()
        setupCardInterface()
        self.adjustQuantityLabelFontSize() // Set font size of the quantity label
    }
    
    // This is fired whenever a change in layout is detecte (I.E. orientation change or size change)
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.adjustQuantityLabelFontSize() // Set font size of the quantity label
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupControls() {
        counterImageView.isUserInteractionEnabled = true
        // Swipe Up
        swipeUp.addTarget(self, action: #selector(swipeUp(_:)))
        swipeUp.direction = .up
        self.counterImageView.addGestureRecognizer(swipeUp)
        // Swipe Down
        swipeDown.addTarget(self, action: #selector(swipeDown(_:)))
        swipeDown.direction = .down
        self.counterImageView.addGestureRecognizer(swipeDown)
        // Swipe Left
        swipeLeft.addTarget(self, action: #selector(swipeLeft(_:)))
        swipeLeft.direction = .left
        self.counterImageView.addGestureRecognizer(swipeLeft)
        // Swipe Right
        swipeRight.addTarget(self, action: #selector(swipeRight(_:)))
        swipeRight.direction = .right
        self.counterImageView.addGestureRecognizer(swipeRight)
        // Long Press
        longPress.addTarget(self, action: #selector(longPress(_:)))
        longPress.minimumPressDuration = self.delayBeforeRemoval
        self.counterImageView.addGestureRecognizer(longPress)
        longPress.delegate = self
        // Long Press For Build-Up-To-Removal Animation
        longPressForAnimation.addTarget(self, action: #selector(longPressForAnimation(_:)))
        longPressForAnimation.minimumPressDuration = self.delayBeforeRomovalAnimationStart
        self.counterImageView.addGestureRecognizer(longPressForAnimation)
        longPressForAnimation.delegate = self
    }
    
    // LONG PRESS
    @objc func longPress(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            delegate?.updateCounter(id: self.id!, orientation: .empty, counterValue: self.counterValue)
            self.deathIconImageView.alpha = 1.0
            self.deathIconBackgroundImageView.alpha = 1.0
            UIView.animate(withDuration: 0.5, animations: {
                self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.view.alpha = 0.0
                self.deathIconImageView.alpha = 1.0
                self.deathIconBackgroundImageView.alpha = 1.0
            }, completion: {(finished: Bool) in
                if finished == true {
                    self.battlefieldDelegate?.adjustBattlefield()
                    self.view.removeFromSuperview()
                }
            })
        }
    }
    
    // UP SWIPE
    @objc func swipeUp(_ sender: UISwipeGestureRecognizer) {
        increaseCounterValue()
    }
    
    // DOWN SWIPE
    @objc func swipeDown(_ sender: UISwipeGestureRecognizer) {
        decreaseCounterValue()
    }
    
    // LEFT SWIPE
    @objc func swipeLeft(_ sender: UISwipeGestureRecognizer) {
    }
    
    // RIGHT SWIPE
    @objc func swipeRight(_ sender: UISwipeGestureRecognizer) {
    }
    
    // Initial Creation of the Counter
    func setupCardInterface() {
        if let createdCard = card {
            counterImageView.image = UIImage(named: createdCard.name!)
            deathIconImageView.alpha = 0.0
            deathIconBackgroundImageView.alpha = 0.0
            counterValue = card?.quantity ?? 0
            counterQuantityLabel.text = String(counterValue)
        }
    }
    
    
    // Functions to increase or decrease the counter value and update the label accordingly
    func increaseCounterValue() {
        counterValue += 1
        counterQuantityLabel.text = String(counterValue)
        delegate?.updateCounter(id: self.id!, orientation: .untapped, counterValue: counterValue)
    }
    func decreaseCounterValue() {
        counterValue -= 1
        counterQuantityLabel.text = String(counterValue)
        delegate?.updateCounter(id: self.id!, orientation: .untapped, counterValue: counterValue)
    }
    
    // Adjust the font size of the quantity label
    // Depending on number of cards in play and orientation
    func adjustQuantityLabelFontSize() {
        let orientation = UIApplication.shared.statusBarOrientation
        let device = UIDevice.current
        let player = delegate?.getPlayerData()
        let game = GameData.sharedInstance
        var fontSize: CGFloat = 72
        
        // Font Sizes for tutorial counters
        if self.isExample == true {
            if game.numberOfPlayers == 2 {
                if device.userInterfaceIdiom == .phone {
                    fontSize = 30
                } else {
                    fontSize = 64
                }
            } else {
                if device.userInterfaceIdiom == .phone && orientation.isPortrait {
                    fontSize = 60
                } else if device.userInterfaceIdiom == .phone {
                    fontSize = 30
                } else {
                    fontSize = 80
                }
            }
            
        // Font Sizes for battlefield counters
        } else {
            switch game.numberOfPlayers {
            case 2:
                // iPhone Landscape
                if orientation.isLandscape && device.userInterfaceIdiom == .phone {
                    if player!.getCardCount() >= game.growAtCards2[2]! {
                        fontSize = 42
                    } else if player!.getCardCount() >= game.growAtCards1[2]! {
                        fontSize = 60
                    } else {
                        fontSize = 72
                    }
                    // iPhone Portrait
                } else if device.userInterfaceIdiom == .phone {
                    if player!.getCardCount() >= game.growAtCards2[2]! {
                        fontSize = 36
                    } else if player!.getCardCount() >= game.growAtCards1[2]! {
                        fontSize = 67
                    } else {
                        fontSize = 72
                    }
                    // iPad Landscape
                } else if orientation.isLandscape && device.userInterfaceIdiom == .pad {
                    if player!.getCardCount() >= game.growAtCards2[2]! {
                        fontSize = 90
                    } else if player!.getCardCount() >= game.growAtCards1[2]! {
                        fontSize = 90
                    } else {
                        fontSize = 116
                    }
                    // iPad Portrait
                } else {
                    if player!.getCardCount() >= game.growAtCards2[2]! {
                        fontSize = 74
                    } else if player!.getCardCount() >= game.growAtCards1[2]! {
                        fontSize = 110
                    } else {
                        fontSize = 116
                    }
                }
            default:
                // iPhone Landscape
                if orientation.isLandscape && device.userInterfaceIdiom == .phone {
                    if player!.getCardCount() >= game.growAtCards2[1]! {
                        fontSize = 36
                    } else if player!.getCardCount() >= game.growAtCards1[1]! {
                        fontSize = 60
                    } else {
                        fontSize = 76
                    }
                    // iPhone Portrait
                } else if device.userInterfaceIdiom == .phone {
                    if player!.getCardCount() >= game.growAtCards2[1]! {
                        fontSize = 69
                    } else if player!.getCardCount() >= game.growAtCards1[1]! {
                        fontSize = 69
                    } else {
                        fontSize = 76
                    }
                    // iPad Landscape
                } else if orientation.isLandscape && device.userInterfaceIdiom == .pad {
                    if player!.getCardCount() >= game.growAtCards2[1]! {
                        fontSize = 76
                    } else if player!.getCardCount() >= game.growAtCards1[1]! {
                        fontSize = 90
                    } else {
                        fontSize = 100
                    }
                    // iPad Portrait
                } else {
                    if player!.getCardCount() >= game.growAtCards2[1]! {
                        fontSize = 100
                    } else if player!.getCardCount() >= game.growAtCards1[1]! {
                        fontSize = 110
                    } else {
                        fontSize = 116
                    }
                }
            }
        }
        self.view.layoutIfNeeded()
        self.counterQuantityLabel.font = self.counterQuantityLabel.font.withSize(fontSize) // Set font size of the quantity label
        self.view.layoutIfNeeded()
    }

}








