//
//  DeckListViewController.swift
//  MTGCounters
//
//  Created by Conor King on 8/5/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class DeckListViewController: MDMViewController, UITableViewDataSource, UITableViewDelegate {
    
    let game = GameData.sharedInstance
    let colorManager = ColorManager()
    
    @IBOutlet weak var deckListTableView: UITableView!
    @IBOutlet weak var deckEditToolbarView: UIView!
    @IBOutlet weak var deckEditTitle: UILabel!
    @IBOutlet weak var deckEditRenameButton: UIButton!
    @IBOutlet weak var deckEditAddTokenButton: UIButton!
    @IBOutlet weak var deckEditAddCounterButton: UIButton!
    @IBOutlet weak var deckEditDeleteButton: UIButton!
    @IBOutlet weak var deckEditBorder: UIView!
    @IBOutlet weak var deckEditBorderWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var addToBattlefieldToolbar: UIView!
    @IBOutlet weak var addToBattlefieldAllButton: UIButton!
    @IBOutlet weak var addToBattlefiedBorder: UIView!
    @IBOutlet weak var addToBattlefieldBorderWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    var players = Array<PlayerData>()
    var playerNumber = 1
    var deleteMode: Bool = false
    var inAnimationTransition: Bool = false // Used for detecting if the resfresh is based on the delete button
    var colorData: ColorData?
    
    // Inputs
    var deckCards = [CardData]()
    var deckName: String?
    var editMode: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        deckListTableView.delegate = self
        deckListTableView.dataSource = self
        deckListTableView.rowHeight = UITableViewAutomaticDimension

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.inAnimationTransition = false
        self.deleteMode = false
        self.deckCards = DecksServices().getCardsFor(deck: deckName!)
        self.refreshTable()
        self.UISetup()
    }
    
    override var prefersStatusBarHidden: Bool {
        if self.playerNumber == 2 {
            return true
        } else {
            return false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Edit Mode Buttons
    @IBAction func deckEditRenameButton(_ sender: Any) {
        self.displayRenameAlert(initialName: self.deckEditTitle.text ?? "")
    }
    
    // Tokens
    @IBAction func deckEditAddTokenButton(_ sender: Any) {
        let tokensVC = storyboard?.instantiateViewController(withIdentifier: "TokenTypeTableVC") as! TokenTypeSelectionTableViewController
        tokensVC.tokensDictionary = DataFormatService().getTokenDictionary()
        tokensVC.deckMode = true
        tokensVC.deckName = self.deckName!
        tokensVC.players = self.players
        HeroWrapper.coverDown()
        self.navigationController?.pushViewController(tokensVC, animated: true)
    }
    // Counters
    @IBAction func deckEditAddCounterButton(_ sender: Any) {
        let countersVC = storyboard?.instantiateViewController(withIdentifier: "CounterCategoryTableVC") as! CounterCategoryTableViewController
        countersVC.countersDictionary = DataFormatService().getCounterDictionary()
        countersVC.deckMode = true
        countersVC.deckName = self.deckName!
        countersVC.players = self.players
        HeroWrapper.coverDown()
        navigationController?.pushViewController(countersVC, animated: true)
    }

    // Add To Battlefield Buttons
    @IBAction func addToBattlefieldAllButton(_ sender: Any) {
        
        // Loop over battlefield states to count current amount of cards in player's card array
        var currentCardCount = 0
        for state in players[self.playerNumber-1].battledfieldState {
            if state != .empty {
                currentCardCount += 1
            }
        }
        
        if self.players.count == 2 {
            // 2 PLAYER
            if (currentCardCount + deckCards.count) > self.game.maxCards2[self.players.count]! {
                // After displaying the confirmation to overwrite the battlefield
                displayCardOverWriteAlert( completion: { reset in
                    if reset {
                        // Reset current cards, loop to add decks cards to the player's cards,
                        // loop to correct battlefield states, then segue to battlefield
                        let battlefieldVC = self.storyboard?.instantiateViewController(withIdentifier: "TwoPlayerBattlefieldVC") as! TwoPlayerBattlefieldViewController
                        battlefieldVC.players = self.players
                        battlefieldVC.players[self.playerNumber-1].removeAllCards()
                        var cardIndex = 1
                        for card in self.deckCards {
                            if cardIndex <= self.game.maxCards2[self.players.count]! {
                                if battlefieldVC.players[self.playerNumber-1].findEmptySlot(for: card) == false {
                                    self.displayNoRoomAlert(playerNumber: self.playerNumber)
                                }
                            }
                            cardIndex += 1
                        }
                        for count in 1...(self.deckCards.count) {
                            if count <= self.game.maxCards2[self.players.count]! {
                                battlefieldVC.players[self.playerNumber-1].battledfieldState[count-1] = .untapped
                            }
                        }
                        if self.playerNumber == 2 {
                            let snap = self.view.snapshotView(afterScreenUpdates: true)
                            self.view.addSubview(snap!)
                            snap?.transform = CGAffineTransform(rotationAngle: .pi)
                            HeroWrapper.unCoverDown()
                        } else {
                            HeroWrapper.unCoverUp()
                        }
                        self.navigationController?.setViewControllers([battlefieldVC], animated: true)
                    }
                })
            } else {
                // Segue to TwoPlayerBattleFieldVC, appending deck to list of player cards
                let battlefieldVC = storyboard?.instantiateViewController(withIdentifier: "TwoPlayerBattlefieldVC") as! TwoPlayerBattlefieldViewController
                for card in deckCards {
                    if players[self.playerNumber-1].findEmptySlot(for: card) == false {
                        self.displayNoRoomAlert(playerNumber: self.playerNumber)
                    }
                }
                battlefieldVC.players = self.players
                if self.playerNumber == 2 {
                    let snap = self.view.snapshotView(afterScreenUpdates: true)
                    self.view.addSubview(snap!)
                    snap?.transform = CGAffineTransform(rotationAngle: .pi)
                    HeroWrapper.unCoverDown()
                } else {
                    HeroWrapper.unCoverUp()
                }
                navigationController?.setViewControllers([battlefieldVC], animated: true)
            }

        } else {
            // 1 PLAYER
            if (currentCardCount + deckCards.count) > self.game.maxCards2[self.players.count]! {
                // After displaying the confirmation to overwrite the battlefield
                displayCardOverWriteAlert( completion: { reset in
                    if reset {
                        // Reset current cards, loop to add decks cards to the player's cards,
                        // loop to current battlefield states, then segue to battlefield
                        let battlefieldVC = self.storyboard?.instantiateViewController(withIdentifier: "OnePlayerBattlefieldVC") as! OnePlayerBattlefieldViewController
                        battlefieldVC.player1.removeAllCards()
                        for card in self.deckCards {
                            if battlefieldVC.player1.findEmptySlot(for: card) == false {
                                self.displayNoRoomAlert(playerNumber: 1)
                            }
                        }
                        for count in 0...(self.deckCards.count - 1) {
                            battlefieldVC.player1.battledfieldState[count] = .untapped
                        }
                        HeroWrapper.unCoverUp()
                        self.navigationController?.setViewControllers([battlefieldVC], animated: true)
                    }
                })
            } else {
                // Segue to OnePlayerBattleFieldVC, appending deck to list of player cards
                let battlefieldVC = storyboard?.instantiateViewController(withIdentifier: "OnePlayerBattlefieldVC") as! OnePlayerBattlefieldViewController
                for card in deckCards {
                    if players[0].findEmptySlot(for: card) == false {
                        self.displayNoRoomAlert(playerNumber: 1)
                    }
                }
                battlefieldVC.player1 = players[0]
                HeroWrapper.unCoverUp()
                navigationController?.setViewControllers([battlefieldVC], animated: true)
            }
        }
        
    }
    
    // Delete
    @IBAction func deckEditDeleteButton(_ sender: Any) {
        if self.deleteMode {
            self.deleteMode = false
            self.toggleDeleteButton(button: self.deckEditDeleteButton, on: false)
        } else {
            self.deleteMode = true
            self.toggleDeleteButton(button: self.deckEditDeleteButton, on: true)
        }
        self.inAnimationTransition = true
        self.refreshTable()
    }
    
    // Display dialog to ask user if they want to overwrite the battlefield with this deck's cards
    func displayCardOverWriteAlert(completion: @escaping (Bool) -> ()) {
        
        let alert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertViewController
        alert.player = self.playerNumber
        
        var shouldShowRestrictionMessage = false
        
        // If 2 Player Mode, if the player's battlefield is currently empty, flag the player to receive a restriction message instead
        if self.players.count == 2 {
            shouldShowRestrictionMessage = true
            for state in self.players[self.playerNumber-1].battledfieldState {
                if state != .empty {
                    shouldShowRestrictionMessage = false
                }
            }
        }
        
        // If player's battlefield is currently empty, we let the player know that that only the first two cards in the deck will be places on the battlefield
        if shouldShowRestrictionMessage == true {
            alert.setAlertText(title: "Limited space on the battlefield.", message: "Only the first six cards from this deck will be played.", b1: "Cancel", b2: "Okay")
        } else if self.players.count == 2 {
            alert.setAlertText(title: "Not enough space on the battlefield.", message: "Remove all cards from the battlefield and play the first six in this deck?", b1: "Cancel", b2: "Okay")
        } else {
            alert.setAlertText(title: "Not enough space on the battlefield.", message: "Remove all cards from the battlefield and play all cards in this deck?", b1: "Cancel", b2: "Okay")
        }
        
        alert.button1Action = { (alertVC) in
            completion(false)
        }
        alert.button2Action = { (alertVC) in
            completion(true)
        }
        alert.show()
        
    }
    
    // Action for the rename button
    // Display dialog to ask user to rename the deck
    func displayRenameAlert(initialName: String) {
        let alert = UIAlertController(title: "Rename", message: "Enter a new name for this deck:", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = initialName
        }
        
        let OKAction = UIAlertAction(title: "Create", style: .default) { (action:UIAlertAction!) in
            let nameField = alert.textFields![0].text!
            var savedDecks = UserDefaults.standard.dictionary(forKey: "savedDecks") ?? [String:Any]()
            if savedDecks[nameField] != nil && nameField != initialName {
                self.displayNamingError(name: initialName)
            } else {
                let changedDeck = savedDecks[initialName]
                savedDecks.removeValue(forKey: initialName)
                savedDecks[nameField] = changedDeck
                UserDefaults.standard.set(savedDecks, forKey: "savedDecks")
                self.deckName = nameField
                self.deckEditTitle.text = nameField
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        }
        alert.addAction(OKAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // Display dialog to alert user that the deck name already exists
    func displayNamingError(name: String) {
        let alert = UIAlertController(title: "Please choose another name.", message: "A deck with that name already exists", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            self.displayRenameAlert(initialName: name)
        }
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // Display dialog to alert user too many cards on the battlefield.
    func displayNoRoomAlert(playerNumber: Int) {
        //Create alert when no slots available for cards
        let alert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertVC") as! CustomAlertViewController
        alert.player = playerNumber
        alert.setAlertText(title: "", message: "No more room on the battlefield.", b1: "Okay", b2: "")
        
        alert.button1Action = { (alertVC) in
        }
        alert.button2ShouldHide = true
        alert.show()
        
    }
    
    // Show the alert dialog when deleting a card
    func displayDeleteAlert(cell: DeckListTableViewCell, index: Int) {
        let cardName: String = cell.nameLabel.text!
        let alert = UIAlertController(title: "Delete \(cardName)?", message: "This will remove it from \(self.deckEditTitle.text ?? "this deck")", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            self.deleteCardAt(index: index, deck: self.deckName!)
        }
        let noAction = UIAlertAction(title: "No", style: .cancel)  { (action:UIAlertAction!) in
        }
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // Delete a card from the deck
    func deleteCardAt(index: Int, deck: String) {
        var savedDecks = UserDefaults.standard.dictionary(forKey: "savedDecks") ?? [String:Any]()
        self.deckCards.remove(at: index)
        var remainingCardsArray = [[String:Any]]()
        let deckService = DecksServices()
        for card in self.deckCards {
            let cardAsDictionary = deckService.convertToDictionary(cardData: card)
            remainingCardsArray.append(cardAsDictionary)
        }
        savedDecks[self.deckName!] = remainingCardsArray
        UserDefaults.standard.set(savedDecks, forKey: "savedDecks")
        self.refreshTable()
    }
    
    // Method to refresh the list of decks
    func refreshTable() {
        DispatchQueue.main.async  {
            self.deckListTableView.reloadData()
        }
    }
    
    
}


// MARK: - Table view data source
extension DeckListViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deckCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let deckItemCell = tableView.dequeueReusableCell(withIdentifier: "DeckListTableViewCell", for: indexPath) as! DeckListTableViewCell
        if self.deckCards.count > 0 {
            // Cell Content
            let card = deckCards[indexPath.row]
            
            if card.type == .token {
                deckItemCell.nameLabel.text = card.category
                deckItemCell.statsLabel.text = card.stats
                deckItemCell.descriptionLabel.text = card.description
                deckItemCell.configureManaIcons(colors: card.color)
            } else {
                deckItemCell.nameLabel.text = card.description! + " Counter"
                deckItemCell.statsLabel.text = ""
                deckItemCell.descriptionLabel.text = ""
                deckItemCell.configureManaIcons(colors: [""])
            }
            
            self.configureCellButtons(cell: deckItemCell, index: indexPath.row)
            
            // Cell Styling
            deckItemCell.contentView.backgroundColor = self.primaryColor
            deckItemCell.nameLabel.textColor = self.secondaryColor
            deckItemCell.descriptionLabel.textColor = self.secondaryColor
            deckItemCell.statsLabel.textColor = self.secondaryColor
            deckItemCell.separator.backgroundColor = self.color3
            
            if let lastVisibleIndexPath = deckListTableView.indexPathsForVisibleRows?.last {
                if indexPath == lastVisibleIndexPath {
                    self.inAnimationTransition = false
                }
            }
            
        } else {
            deckItemCell.isHidden = true
            deckItemCell.isUserInteractionEnabled = false
        }
        return deckItemCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.editMode == false {
            // Change the text color of the color label to signify it is highlighted
            let cell = tableView.cellForRow(at: indexPath) as! DeckListTableViewCell
            cell.nameLabel.textColor = UIColor.lightGray
            cell.descriptionLabel.textColor = UIColor.lightGray
            cell.statsLabel.textColor = UIColor.lightGray
            cell.contentView.backgroundColor = self.colorManager.getHighlightColor(for: GameData.sharedInstance.colorSchemes[self.playerNumber-1], color: primaryColor!)
            
            if self.players.count == 2 {
                // Segue to TwoPlayerBattleFieldVC, declare a new card on that view, then submit the card into the recents list
                let battlefieldVC = storyboard?.instantiateViewController(withIdentifier: "TwoPlayerBattlefieldVC") as! TwoPlayerBattlefieldViewController
                battlefieldVC.newCard = deckCards[indexPath.row]
                battlefieldVC.newCardPlayerNumber = self.playerNumber
                battlefieldVC.players = self.players
                if self.playerNumber == 2 {
                    let snap = self.view.snapshotView(afterScreenUpdates: true)
                    self.view.addSubview(snap!)
                    snap?.transform = CGAffineTransform(rotationAngle: .pi)
                    HeroWrapper.unCoverDown()
                } else {
                    HeroWrapper.unCoverUp()
                }
                navigationController?.setViewControllers([battlefieldVC], animated: true)
            } else {
                // Segue to OnePlayerBattleFieldVC, declaring a new card on that view
                let battlefieldVC = storyboard?.instantiateViewController(withIdentifier: "OnePlayerBattlefieldVC") as! OnePlayerBattlefieldViewController
                battlefieldVC.newCard = deckCards[indexPath.row]
                battlefieldVC.player1 = players[0]
                HeroWrapper.unCoverUp()
                navigationController?.setViewControllers([battlefieldVC], animated: true)
                
            }

        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    
    // Determine if cell buttons are needed and set them
    func configureCellButtons(cell: DeckListTableViewCell, index: Int) {
        let styleManager = StyleManager(colorData: self.colorData!, scheme: self.colorScheme)
        
        if self.deleteMode {
            cell.deleteButton.isUserInteractionEnabled = true
            
            styleManager.format(button: cell.deleteButton, type: .deleteButton2, iconImageView: nil)
            self.formatButtonOnAction(button: cell.deleteButton, type: .deleteButton2)
            
            // If loading cell because of the delete button, then animate the cell's delete button constraint
            // If not, display the delete button immediately
            if self.inAnimationTransition {
                cell.deleteButtonTrailingConstraint.constant = -75
                cell.contentView.layoutIfNeeded()
                cell.deleteButtonTrailingConstraint.constant = 0
                UIView.animate(withDuration: 0.5, animations: {
                    cell.contentView.layoutIfNeeded()
                })
            } else {
                cell.deleteButtonTrailingConstraint.constant = 0
                cell.contentView.layoutIfNeeded()
            }
            cell.deleteCellAction = { (cell) in
                self.displayDeleteAlert(cell: cell, index: index)
            }
        } else {
            
            // If loading cell because of the delete button, then animate the cell's delete button constraint
            // If not, display the delete button immediately
            if self.inAnimationTransition {
                cell.deleteButtonTrailingConstraint.constant = 0
                cell.contentView.layoutIfNeeded()
                cell.deleteButtonTrailingConstraint.constant = -75
                UIView.animate(withDuration: 0.5, animations: {
                    cell.contentView.layoutIfNeeded()
                })
            } else {
                cell.deleteButtonTrailingConstraint.constant = -75
                cell.contentView.layoutIfNeeded()
            }
            cell.deleteButton.isUserInteractionEnabled = false
        }
    }
    
}


// MARK: STYLE SETUP
extension DeckListViewController {
    
    func UISetup() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        self.colorData = self.colorManager.getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: self.colorData!, scheme: self.colorScheme)
        self.secondaryColor = colorData?.secondaryColor
        self.primaryColor = styleManager.primaryColor
        self.color3 = colorData?.color3
        self.color4 = colorData?.color4
        
        // Set Colors
        self.view.backgroundColor = primaryColor
        self.navigationController?.navigationBar.barTintColor = primaryColor
        self.navigationController?.navigationBar.tintColor = secondaryColor
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor : secondaryColor! ]
        self.deckListTableView.backgroundColor = primaryColor
        
        if self.editMode == true {
            
            self.deckEditToolbarView.alpha = 1.0
            self.deckEditToolbarView.isUserInteractionEnabled = true
            self.deckEditToolbarView.backgroundColor = self.color4
            
            self.addToBattlefieldToolbar.alpha = 0.0
            self.addToBattlefieldToolbar.isUserInteractionEnabled = false
            
            self.title = "Edit Deck"
            self.deckEditTitle.text = self.deckName
            self.deckEditTitle.textColor = self.secondaryColor
            styleManager.format(borderView: self.deckEditBorder, widthConstraint: self.deckEditBorderWidthConstraint)
            
            self.view.layoutIfNeeded()
            self.tableViewBottomConstraint.constant = self.deckEditToolbarView.frame.height
            self.view.layoutIfNeeded()
            
            // Buttons
            styleManager.format(button: self.deckEditRenameButton, type: .smallButton, iconImageView: nil)
            self.formatButtonOnAction(button: self.deckEditRenameButton, type: .smallButton)
            styleManager.format(button: self.deckEditAddTokenButton, type: .smallButton, iconImageView: nil)
            self.formatButtonOnAction(button: self.deckEditAddTokenButton, type: .smallButton)
            styleManager.format(button: self.deckEditAddCounterButton, type: .smallButton, iconImageView: nil)
            self.formatButtonOnAction(button: self.deckEditAddCounterButton, type: .smallButton)
            styleManager.format(button: self.deckEditDeleteButton, type: .deleteButton, iconImageView: nil)
            self.formatButtonOnAction(button: self.deckEditDeleteButton, type: .deleteButton)
            
        } else {
            
            self.deckEditToolbarView.alpha = 0.0
            self.deckEditToolbarView.isUserInteractionEnabled = false
            
            self.addToBattlefieldToolbar.alpha = 1.0
            self.addToBattlefieldToolbar.isUserInteractionEnabled = true
            self.addToBattlefieldToolbar.backgroundColor = self.color4
            
            self.title = "Play"
            styleManager.format(borderView: self.addToBattlefiedBorder, widthConstraint: self.addToBattlefieldBorderWidthConstraint)
            
            self.view.layoutIfNeeded()
            self.tableViewBottomConstraint.constant = self.addToBattlefieldToolbar.frame.height
            self.view.layoutIfNeeded()

            // Buttons
            styleManager.format(button: self.addToBattlefieldAllButton, type: .smallButton, iconImageView: nil)
            self.formatButtonOnAction(button: self.addToBattlefieldAllButton, type: .smallButton)

        }
        

    }
    
    // Returns a font size based on device class
    func getButtonFontSize() -> CGFloat {
        if let currentTraits = self.navigationController?.traitCollection {
            if currentTraits.isIpad {
                return 20
            } else if currentTraits.isIphoneLandscape {
                return 18
            } else if currentTraits.isIphonePortrait {
                return 18
            }
        }
        return 20
    }
}










