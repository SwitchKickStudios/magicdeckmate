//
//  BattlefieldCollectionViewController.swift
//  MTGCounters
//
//  Created by Conor King on 2/12/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

protocol BattlefieldCollectionDelegate {
    func adjustBattlefield()
}

protocol BattlefieldUpdatePlayerDelegate {
    func updatePlayer(data: PlayerData)
}

class BattlefieldCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, BattlefieldCollectionDelegate {
    
    var playerNumber = 1
    var player = PlayerData(maxPlayers: 1)
    var tokenDelegate: TokenDelegate? = nil
    var counterDelegate: CounterDelegate? = nil
    var updateDelegate: BattlefieldUpdatePlayerDelegate? = nil
    let game = GameData.sharedInstance
    let colorManager = ColorManager()
    
    var displayCardCount = 0 // Used to determine what size the cells need to be
    var realCardCount = 0 // Used to determine if we need to reorganize cells
    var secondaryColor: UIColor?
    var primaryColor: UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.countCards()
        self.setupColors()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.collectionViewLayout.invalidateLayout()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberOfPotentialCards: Int = game.maxCards2[self.game.numberOfPlayers]!
        return numberOfPotentialCards
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BattlefieldCollectionCell", for: indexPath) as! BattlefieldCollectionViewCell
        
        if self.player.battledfieldState[indexPath.row] != .empty {
            let card = self.player.cards[indexPath.row]
            if card.type == .token {
                let tokenVC = storyboard?.instantiateViewController(withIdentifier: "TokenCardVC") as! TokenCardViewController
                tokenVC.card = self.player.cards[indexPath.row]
                tokenVC.labelColor = self.secondaryColor
                tokenVC.orientation = self.player.battledfieldState[indexPath.row]
                tokenVC.id = self.player.cardIDs[indexPath.row]
                tokenVC.delegate = self.tokenDelegate
                tokenVC.battlefieldDelegate = self
                self.addChildViewController(tokenVC)
                tokenVC.view.frame = cell.contentView.bounds
                cell.contentView.addSubview(tokenVC.view)
                tokenVC.didMove(toParentViewController: self)
            } else {
                let counterVC = storyboard?.instantiateViewController(withIdentifier: "CounterCardVC") as! CounterCardViewController
                counterVC.card = card
                counterVC.id = self.player.cardIDs[indexPath.row]
                counterVC.delegate = self.counterDelegate
                counterVC.battlefieldDelegate = self
                self.addChildViewController(counterVC)
                counterVC.view.frame = cell.contentView.bounds
                cell.contentView.addSubview(counterVC.view)
                counterVC.didMove(toParentViewController: self)
            }
        }
    
        return cell
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.determineCellSize(collectionView: collectionView)

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        return sectionInset
    }
    
//    ---------------------------------
//    CUSTOM FUNCTIONS
//    ----------------
    
    // Determine the number of cards NOT empty
    func countCards() {
        self.displayCardCount = 0
        self.realCardCount = 0
        for state in self.player.battledfieldState {
            if state != .empty {
                self.realCardCount += 1
            }
        }
        self.displayCardCount = self.realCardCount
        if (self.displayCardCount >= game.growAtCards1[self.game.numberOfPlayers]! && self.displayCardCount < game.growAtCards2[self.game.numberOfPlayers]!) {
            self.displayCardCount = game.maxCards1[self.game.numberOfPlayers]!
        } else if self.displayCardCount >= game.growAtCards2[self.game.numberOfPlayers]! {
            self.displayCardCount = game.maxCards2[self.game.numberOfPlayers]!
        }
    }
    
    // Return Size for Cells based on device type, orientation, and number of cards on the battlefield
    func determineCellSize(collectionView: UICollectionView) -> CGSize {
        let device = UIDevice.current
        let orientation = UIApplication.shared.statusBarOrientation
        var cellSize = CGSize(width: (collectionView.bounds.width/2), height: (collectionView.bounds.height/2))
        
        if game.numberOfPlayers == 2 {
            cellSize = CGSize(width: (collectionView.bounds.width/2), height: (collectionView.bounds.height))
            switch self.displayCardCount {
            case 3:
                cellSize = CGSize(width: (collectionView.bounds.width/3), height: (collectionView.bounds.height))
            case 6:
                cellSize = CGSize(width: (collectionView.bounds.width/3), height: (collectionView.bounds.height)/2)
            default:
                cellSize = CGSize(width: (collectionView.bounds.width/2), height: (collectionView.bounds.height))
            }
            return cellSize
        } else {
            if orientation.isLandscape && device.userInterfaceIdiom == .phone {
                switch self.displayCardCount {
                case 6:
                    cellSize = CGSize(width: (collectionView.bounds.width/6), height: (collectionView.bounds.height))
                case 9:
                    cellSize = CGSize(width: (collectionView.bounds.width/9), height: (collectionView.bounds.height))
                default:
                    cellSize = CGSize(width: (collectionView.bounds.width/4), height: (collectionView.bounds.height))
                }
            } else {
                switch self.displayCardCount {
                case 6:
                    cellSize = CGSize(width: (collectionView.bounds.width/3), height: (collectionView.bounds.height/2))
                case 9:
                    cellSize = CGSize(width: (collectionView.bounds.width/3), height: (collectionView.bounds.height/3))
                default:
                    cellSize = CGSize(width: (collectionView.bounds.width/2), height: (collectionView.bounds.height/2))
                }
            }
            return cellSize
        }
    }
    
    // Animate and change the size of cells
    // Used when removing tokens or counters
    // Reorganize cells if we want to display 4 cells
    func adjustBattlefield() {
        self.countCards()
        let cardCountToTransition1 = game.shrinkAtCards1[self.game.numberOfPlayers]
        let cardCountToTransition2 = game.shrinkAtCards2[self.game.numberOfPlayers]
        
        if  (self.displayCardCount == cardCountToTransition1 && self.realCardCount == cardCountToTransition1) ||
            (self.displayCardCount == cardCountToTransition2 && self.realCardCount == cardCountToTransition2) {
            
            // Stuff the empty slot numbers into an array
            var emptySlotArray: [Int] = []
            for index in 0...collectionView!.visibleCells.count-1 {
                if self.player.battledfieldState[index] == .empty {
                    emptySlotArray.append(index)
                }
            }
            
            // Sort cards and update parent viewcontrollers
            self.player.sortCardsForBattlefield()
            self.updateDelegate?.updatePlayer(data: self.player)
            
            // Loop over the array of empty slots and remove them from the collectionview,
            // replacing removed cells with cells at the top of the stack
            self.collectionView?.performBatchUpdates({
                for index in emptySlotArray {
                    self.collectionView?.deleteItems(at: [IndexPath(row: index, section: 0)])
                    self.collectionView?.insertItems(at: [IndexPath(row: ((self.collectionView?.numberOfItems(inSection: 0))!-1), section: 0)])
                    print("Removed at \(index)")
                }
            }, completion: { (finished:Bool) in
                if finished == true {
                    self.refreshCollectionViewWithCellSizing()
                }
            })
        } else {
            self.refreshCollectionViewWithCellSizing()
        }

    }
    
    func refreshCollectionViewWithCellSizing() {
        if let newLayout = self.collectionViewLayout as? UICollectionViewFlowLayout {
            newLayout.itemSize = self.determineCellSize(collectionView: self.collectionView!)
            self.collectionView?.setCollectionViewLayout(newLayout, animated: true)
        }
    }
    
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}

// MARK: STYLING
extension BattlefieldCollectionViewController {
    
    func setupColors() {
        // Acquire colors
        let colorScheme = self.game.colorSchemes[self.playerNumber-1]
        let colorData = colorManager.getAssetsForColor(scheme: colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
    }
    
    
}




