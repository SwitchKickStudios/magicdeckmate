//
//  CountersCategoryTableViewController.swift
//  MTGCounters
//
//  Created by William King on 5/1/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class CounterCategoryTableViewController: MDMTableViewController {

    let colorManager = ColorManager()
    
    var countersDictionary = Dictionary<String,[CardData]>()
    var counterCategories = Array<String>()
    var players = Array<PlayerData>()
    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    var deckMode: Bool = false
    var deckName: String = ""

    let viewTitle = "Counters"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatCategories()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.orientNavigationController(orientation: OrientationService().convertToDeviceOrientationFrom(interfaceOrientation: UIApplication.shared.statusBarOrientation))
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshTable()
        self.UISetup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController) {
            if playerNumber == 2 && UIApplication.shared.statusBarOrientation.isPortrait {
                // Make a snapshot of the tableview, place it over the view, then flip it
                // When the navigation controller is turned right side up, the snapshot will be upside down
                let snap = self.view.snapshotView(afterScreenUpdates: false)
                self.view.addSubview(snap!)
                snap?.transform = CGAffineTransform(rotationAngle: .pi)
                HeroWrapper.unCoverDown()
                self.delegate?.openView(selection: "main", animate: false)
            } else {
                HeroWrapper.unCoverUp()
                self.delegate?.openView(selection: "main", animate: false)
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.orientNavigationController(orientation: UIDevice.current.orientation)
    }
    
    override var prefersStatusBarHidden: Bool {
        if self.playerNumber == 2 {
            return true
        } else {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        self.navigationController?.navigationBar.barStyle = self.colorManager.getNavigationBarStyleFor(scheme: self.colorScheme)
        return self.colorManager.getStatusBarColorFor(scheme: self.colorScheme)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func orientNavigationController(orientation: UIDeviceOrientation) {
        if self.playerNumber == 2 && orientation.isPortrait {
            self.navigationController?.view.transform = CGAffineTransform(rotationAngle: .pi)
        } else {
            self.navigationController?.view.transform = CGAffineTransform(rotationAngle: 0)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.counterCategories.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CounterCategoryCell", for: indexPath) as! CounterCategoryTableViewCell
        
        cell.counterCategoryLabel.text = counterCategories[indexPath.row]
        cell.contentView.backgroundColor = self.primaryColor
        cell.backgroundColor = self.primaryColor
        cell.counterCategoryLabel.textColor = self.secondaryColor
        cell.separator.backgroundColor = self.color3
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Change the text color of the color label to signify it is highlighted
        let cell = tableView.cellForRow(at: indexPath) as! CounterCategoryTableViewCell
        cell.counterCategoryLabel.textColor = UIColor.lightGray
        cell.contentView.backgroundColor = ColorManager().getHighlightColor(for: GameData.sharedInstance.colorSchemes[self.playerNumber-1], color: primaryColor!)
        
        // Decide whether to segue to Recent Counters Table VC or Counter Variation Table VC
        let selectedCategory = self.counterCategories[indexPath.row]
        if selectedCategory == "Recent Counters" {
            
            let recentCountersVC = storyboard?.instantiateViewController(withIdentifier: "RecentCountersTableVC") as! RecentCountersTableViewController
            recentCountersVC.counterCategory = selectedCategory
            recentCountersVC.players = self.players
            recentCountersVC.playerNumber = self.playerNumber
            recentCountersVC.deckMode = self.deckMode
            recentCountersVC.deckName = self.deckName
            navigationController?.pushViewController(recentCountersVC, animated: true)
            
        } else {
            
            let countersVC = storyboard?.instantiateViewController(withIdentifier: "CounterVariationTableVC") as! CounterVariationTableViewController
            countersVC.countersDictionary = self.countersDictionary
            // Define and pass the variations within each counter category as an array
            if let counterVariations = self.countersDictionary[counterCategories[indexPath.row]] {
                countersVC.counterVariations = counterVariations
            }
            countersVC.counterCategory = self.counterCategories[indexPath.row]
            countersVC.players = self.players
            countersVC.playerNumber = self.playerNumber
            countersVC.deckMode = self.deckMode
            countersVC.deckName = self.deckName
            navigationController?.pushViewController(countersVC, animated: true)
            
        }

    }
    
    func formatCategories() {
        self.counterCategories = self.countersDictionary.keys.sorted()
        self.counterCategories = self.counterCategories.filter { $0 != "Mana Counters" }
        self.counterCategories.insert("Mana Counters", at: 0)
        self.counterCategories.insert("Recent Counters", at: 0)
    }
    
    func refreshTable() {
        DispatchQueue.main.async  {
            self.tableView.reloadData()
            return
        }
    }

}

// MARK: STYLE SETUP
extension CounterCategoryTableViewController {
    
    func UISetup() {
        self.title = self.viewTitle
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = self.colorManager.getAssetsForColor(scheme: colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
        
        // Set Colors
        self.formatNavigationBar()
        self.tableView.backgroundColor = primaryColor
        
        if self.colorScheme == .colorless || self.colorScheme == .green || self.colorScheme == .white {
            self.tableView.indicatorStyle = .black
        } else {
            self.tableView.indicatorStyle = .white
        }
    }
}
