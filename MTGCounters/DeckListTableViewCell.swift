//
//  DeckListTableViewCell.swift
//  MTGCounters
//
//  Created by Conor King on 8/5/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class DeckListTableViewCell: UITableViewCell {

    @IBOutlet weak var colorlessManaIcon: UIImageView!
    @IBOutlet weak var blackManaIcon: UIImageView!
    @IBOutlet weak var blueManaIcon: UIImageView!
    @IBOutlet weak var greenManaIcon: UIImageView!
    @IBOutlet weak var redManaIcon: UIImageView!
    @IBOutlet weak var whiteManaIcon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var separator: UIView!
    
    @IBOutlet weak var infoStackTopPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteButtonTrailingConstraint: NSLayoutConstraint!
    
    var deleteCellAction: ((DeckListTableViewCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        deleteCellAction?(self)
    }
    
    func configureManaIcons(colors: [String]) {
        // Create an array of possible colors
        var possibleColors = [
            "colorless",
            "black",
            "blue",
            "green",
            "red",
            "white"
        ]
        // Filter the colors from token information from the array of possible colors
        for color in colors {
            possibleColors = possibleColors.filter { $0 != color }
        }
        
        // Turn off the icons that remain in the possible colors
        self.colorlessManaIcon.isHidden = possibleColors.contains("colorless")
        self.blackManaIcon.isHidden = possibleColors.contains("black")
        self.blueManaIcon.isHidden = possibleColors.contains("blue")
        self.greenManaIcon.isHidden = possibleColors.contains("green")
        self.redManaIcon.isHidden = possibleColors.contains("red")
        self.whiteManaIcon.isHidden = possibleColors.contains("white")
        
    }

}
