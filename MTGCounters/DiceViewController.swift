//
//  DiceViewController.swift
//  MTGCounters
//
//  Created by Conor King on 11/18/17.
//  Copyright © 2017 Switch Kick Studios. All rights reserved.
//

import UIKit

class DiceViewController: MDMViewController, CloseModalDelegate, BackModalDelegate {

    var playerNumber = 1
    var delegate: MenuNavigationDelegate? = nil
    
    @IBOutlet weak var coinResultLabel: UILabel!
    @IBOutlet weak var diceResultLabel: UILabel!
    @IBOutlet weak var diceResultLabel2: UILabel!
    @IBOutlet weak var topBufferView: UIView!
    @IBOutlet weak var bottomBufferView: UIView!
    @IBOutlet weak var buttonsBarBackground: UIView!
    @IBOutlet weak var buttonsBarTopBorder: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var coinButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sixTwiceButton: UIButton!
    @IBOutlet weak var eightButton: UIButton!
    @IBOutlet weak var twentyButton: UIButton!
    
    @IBOutlet weak var buttonsBarTopBorderWidthConstraint: NSLayoutConstraint!
    
    var currentDiceType: DiceType?
    let coinOutcomes = ["Heads", "Tails"]
    var timer = Timer()
    var timer2 = Timer()
    var repCounter = 0
    var repCounter2 = 0
    var maxReps = 0
    var maxReps2 = 0
    var previousNum: Int?
    var previousNum2: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customNavigationSetup()
        self.UISetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func coinButton(_ sender: Any) {
        self.roll(type: .coin)
    }
    @IBAction func sixButton(_ sender: Any) {
        self.roll(type: .six)
    }
    @IBAction func sixTwiceButton(_ sender: Any) {
        self.roll(type: .sixTwice)
    }
    @IBAction func eightButton(_ sender: Any) {
        self.roll(type: .eight)
    }
    @IBAction func twentyButton(_ sender: Any) {
        self.roll(type: .twenty)
    }
    
    // Rolls the die with given sides, repeatedly randomizing the proper label
    func roll(type: DiceType) {
        self.currentDiceType = type
        self.repCounter = 0
        self.repCounter2 = 0
        self.previousNum = 0
        self.previousNum2 = 0
        self.maxReps = 0
        self.maxReps2 = 0
        self.lockButtons()
        self.setupLabels(type: type)
        self.maxReps = Int(arc4random_uniform(UInt32(30))) // Randomize the amount of times the dice will roll
        self.maxReps = self.maxReps + 20 // Minimum amount of times the dice will roll
        timer.invalidate()
        timer2.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.randomize), userInfo: nil, repeats: true)
        if type == .sixTwice {
            self.maxReps2 = Int(arc4random_uniform(UInt32(30)))
            self.maxReps2 = self.maxReps2 + 20
            timer2 = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(self.randomize2), userInfo: nil, repeats: true)
        }
    }
    
    // Get a random value based on given value
    // Set proper label to that value, then count the reps
    @objc func randomize() {
        let diceType = self.currentDiceType!
        
        // random number
        let randomNum = getRandomUnique(type: diceType, diceNum: 1)
        let outcomeInstance = randomNum + 1
        
        // determine which label to change, then change it
        switch diceType {
        case .coin:
            coinResultLabel.text = "\(coinOutcomes[randomNum])"
        default:
            diceResultLabel.text = "\(outcomeInstance)"
        }
        
        // count the repetitions, then invalidate timer when ready
        repCounter += 1
        if repCounter >= self.maxReps {
            timer.invalidate()
            if self.maxReps > self.maxReps2 {
                self.enableButtons()
            }
        }
    }
    
    // Get a random value based on given value
    // Set proper label to that value, then count the reps
    @objc func randomize2() {
        let diceType = self.currentDiceType!
        
        // random number
        let randomNum = getRandomUnique(type: diceType, diceNum: 2)
        let outcomeInstance = randomNum + 1
        
        diceResultLabel2.text = "\(outcomeInstance)"
        
        // count the repetitions, then invalidate timer when ready
        repCounter2 += 1
        if repCounter2 >= self.maxReps2 {
            timer2.invalidate()
            if self.maxReps2 > self.maxReps {
                self.enableButtons()
            }
        }
    }
    
    // Return a random number based on given value
    // Excludes previously returned number to mimic dice roll or coin flip
    func getRandomUnique(type: DiceType, diceNum: Int) -> Int {
        var value = 2
        switch type {
        case .coin:
            value = 2
        case .six, .sixTwice:
            value = 6
        case .eight:
            value = 8
        case .twenty:
            value = 20
        }
        
        var randomNum = Int(arc4random_uniform(UInt32(value)))
        if diceNum == 2 {
            if randomNum == self.previousNum2 {
                randomNum = getRandomUnique(type: type, diceNum: diceNum)
            }
            self.previousNum2 = randomNum
        } else {
            if randomNum == self.previousNum {
                randomNum = getRandomUnique(type: type, diceNum: diceNum)
            }
            self.previousNum = randomNum
        }
        return randomNum
    }
    
    // Shows and hides labels depending on the dice type
    func setupLabels(type: DiceType) {
        switch type {
        case .coin:
            coinResultLabel.isHidden = false
            diceResultLabel.isHidden = true
            diceResultLabel2.isHidden = true
        case .sixTwice:
            coinResultLabel.isHidden = true
            diceResultLabel.isHidden = false
            diceResultLabel2.isHidden = false
        default:
            coinResultLabel.isHidden = true
            diceResultLabel.isHidden = false
            diceResultLabel2.isHidden = true
        }
    }
    
    // Prevent additional selections when the die is being rolled
    func lockButtons() {
        coinButton.isUserInteractionEnabled = false
        sixButton.isUserInteractionEnabled = false
        sixTwiceButton.isUserInteractionEnabled = false
        eightButton.isUserInteractionEnabled = false
        twentyButton.isUserInteractionEnabled = false
        let grayedOutColor = ColorManager().getHighlightColor(for: GameData.sharedInstance.colorSchemes[self.playerNumber-1], color: primaryColor!)
        coinButton.backgroundColor? = grayedOutColor
        sixButton.backgroundColor? = grayedOutColor
        sixTwiceButton.backgroundColor? = grayedOutColor
        eightButton.backgroundColor? = grayedOutColor
        twentyButton.backgroundColor? = grayedOutColor
    }
    
    // Allow selections when the die is finished being rolled
    func enableButtons() {
        coinButton.isUserInteractionEnabled = true
        sixButton.isUserInteractionEnabled = true
        sixTwiceButton.isUserInteractionEnabled = true
        eightButton.isUserInteractionEnabled = true
        twentyButton.isUserInteractionEnabled = true
        coinButton.backgroundColor? = primaryColor!
        sixButton.backgroundColor? = primaryColor!
        sixTwiceButton.backgroundColor? = primaryColor!
        eightButton.backgroundColor? = primaryColor!
        twentyButton.backgroundColor? = primaryColor!
    }
    
    // CloseModalDelegate function - close this view in a button
    func closeButton() {
        self.delegate?.closeMenu()
    }
    
    // BackModalDelegate function
    func backButton() {
        self.delegate?.openView(selection: "main", animate: false)
    }
    
}

// MARK: STYLE SETUP
extension DiceViewController {
    func customNavigationSetup() {
        let customTopBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavigationTopVC") as! ModalNavigationTopViewController
        customTopBar.playerNumber = self.playerNumber
        customTopBar.delegate = self.delegate
        customTopBar.titleText = "Dice"
        customTopBar.view.frame = self.topBufferView.bounds
        self.addChildViewController(customTopBar)
        self.topBufferView.addSubview(customTopBar.view)
        customTopBar.didMove(toParentViewController: self) //OR  vc.willMove(toParentViewController: self)
        
        let customNavBar = storyboard?.instantiateViewController(withIdentifier: "ModalNavVC") as! ModalNavigationViewController
        customNavBar.showBackButton = true
        customNavBar.closeDelegate = self
        customNavBar.backDelegate = self
        customNavBar.playerNumber = self.playerNumber
        customNavBar.view.frame = self.bottomBufferView.bounds
        self.addChildViewController(customNavBar)
        bottomBufferView.addSubview(customNavBar.view)
        customNavBar.didMove(toParentViewController: self)
    }
    
    func UISetup() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        coinResultLabel.isHidden = true
        diceResultLabel.isHidden = true
        diceResultLabel2.isHidden = true
        
        // Acquire colors
        self.colorScheme = GameData.sharedInstance.colorSchemes[self.playerNumber-1]
        let colorData = ColorManager().getAssetsForColor(scheme: colorScheme)
        let styleManager = StyleManager(colorData: colorData, scheme: self.colorScheme)
        self.secondaryColor = colorData.secondaryColor
        self.primaryColor = colorData.primaryColor
        self.color3 = colorData.color3
        self.color4 = colorData.color4
        
        self.topBufferView.backgroundColor = self.color4
        self.bottomBufferView.backgroundColor = self.color4
        self.buttonsBarBackground.backgroundColor = self.color4
        self.backgroundView.backgroundColor = primaryColor
        self.coinResultLabel.textColor = secondaryColor
        self.diceResultLabel.textColor = secondaryColor
        self.diceResultLabel2.textColor = secondaryColor
        styleManager.format(borderView: self.buttonsBarTopBorder, widthConstraint: self.buttonsBarTopBorderWidthConstraint)

        // Buttons
        styleManager.format(button: self.coinButton, type: .smallButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.coinButton, type: .smallButton)
        styleManager.format(button: self.sixButton, type: .smallButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.sixButton, type: .smallButton)
        styleManager.format(button: self.sixTwiceButton, type: .smallButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.sixTwiceButton, type: .smallButton)
        styleManager.format(button: self.eightButton, type: .smallButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.eightButton, type: .smallButton)
        styleManager.format(button: self.twentyButton, type: .smallButton, iconImageView: nil)
        self.formatButtonOnAction(button: self.twentyButton, type: .smallButton)

    }
    
}









