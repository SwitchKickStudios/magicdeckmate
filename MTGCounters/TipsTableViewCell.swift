//
//  TipsTableViewCell.swift
//  MTGCounters
//
//  Created by Conor King on 6/28/18.
//  Copyright © 2018 Switch Kick Studios. All rights reserved.
//

import UIKit

class TipsTableViewCell: UITableViewCell {

    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var topBorder: UIView!
    @IBOutlet weak var separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
